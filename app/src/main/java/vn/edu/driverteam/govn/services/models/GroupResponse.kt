package vn.edu.driverteam.govn.services.models

import vn.edu.driverteam.govn.models.Group

class GroupResponse {
    var success: Boolean? = null
    var group: Group? = null
    var groups: MutableList<Group>? = null
    var invitations: MutableList<Group>? = null
}