package vn.edu.driverteam.govn.activity

import `in`.championswimmer.sfg.lib.SimpleFingerGestures
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.res.Configuration
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.*
import android.provider.Settings
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.activity.common.*
import vn.edu.driverteam.govn.adapters.CustomInfoWindowAdapter
import vn.edu.driverteam.govn.controllers.AppController
import vn.edu.driverteam.govn.models.Geometry
import vn.edu.driverteam.govn.models.Report
import vn.edu.driverteam.govn.models.User
import vn.edu.driverteam.govn.models.direction.*
import vn.edu.driverteam.govn.models.navigation.StepAdapter
import vn.edu.driverteam.govn.models.nearbyplaces.NearbyPlacesInterface
import vn.edu.driverteam.govn.models.nearbyplaces.NearbyPlacesResponse
import vn.edu.driverteam.govn.services.*
import vn.edu.driverteam.govn.utils.AudioPlayer
import vn.edu.driverteam.govn.utils.FileUtils
import vn.edu.driverteam.govn.utils.Permission
import vn.edu.driverteam.govn.utils.SharePrefs.Companion.mContext
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.libraries.places.api.model.Place
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.Task
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.maps.android.PolyUtil
import com.sdsmdg.tastytoast.TastyToast
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.*
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.UnsupportedEncodingException
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

// oudated dependancy
//import android.app.ProgressDialog
//import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
//import com.google.android.gms.location.places.ui.PlaceSelectionListener

import android.widget.ProgressBar
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import vn.edu.driverteam.govn.services.models.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import vn.edu.driverteam.govn.models.TypeBroadcastChat
import vn.edu.driverteam.govn.models.broadcast.BroadcastAdapter
import java.io.File
import java.io.FileInputStream

class MainActivity :
        AppCompatActivity(),
        OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnInfoWindowCloseListener,
        View.OnClickListener,
        DirectionFinder.DirectionListener,
        GoogleMap.OnPolylineClickListener,
        OnStartDragListener {
    // Static variables
    companion object {
        // PERMISSION_REQUEST_CODE
        private const val MY_LOCATION_PERMISSION_REQUEST_CODE = 1
        // Log
        private const val TAG = "MainActivity"
        // Distance to determine whether a report is on Route or not
        private const val REPORT_ON_ROUTE_DISTANCE_DIRECTION = 5.0 // meter
        private const val REPORT_ON_ROUTE_DISTANCE_NAVIGATION = 100.0 // meter
        // Request code for activity result
        private const val PICK_PLACE_HISTORY_REQUEST = 4
        private const val REQUEST_CHECK_SETTINGS = 2

        private const val QUICK_NAV = 1
        private const val WAY_POINT = 2
    }

    // Permission variables
    private lateinit var mLocationPermission: Permission

    // Google Map variables
    private lateinit var mMap: GoogleMap

    // Location variables
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private lateinit var mActionBarDrawerToggle: ActionBarDrawerToggle

    // Marker options for set up marker
    private var markerOptions = MarkerOptions()

    // Popup windows
    private var mPopupWindowReport: PopupWindow? = null
    private var mPopupWindowUser: PopupWindow? = null
    private var mPopupWindowHello: PopupWindow? = null
    private var mPopupWindowStrongLight: PopupWindow? = null
    private var mPopupWindowWatcher: PopupWindow? = null
    private var mPopupWindowSlowDown: PopupWindow? = null
    private var mPopupWindowTurnAround: PopupWindow? = null
    private var mPopupWindowOther: PopupWindow? = null
    private var mPopupWindowThank: PopupWindow? = null
    private var mPopupWindowDelete: PopupWindow? = null
    private var mPopupWindowBroadcast: PopupWindow? = null

    // List of user of other cars
    private lateinit var listUser: List<User>

    // List of user of other cars
    private lateinit var listReport: List<Report>

    // Socket
    private lateinit var socket: Socket

    // List of user markers
    private var listUserMarker: MutableList<Marker> = ArrayList()
    private var listParticipantMarker: MutableList<Marker> = ArrayList()

    // List of report markers
    private var listReportMarker: MutableList<Marker> = ArrayList()
    private var listReportMarkerCurrentRoute: MutableList<Marker> = ArrayList()
    private var listReportCurrentRoute: MutableList<Report> = ArrayList()

    // Handler của thread
    private lateinit var handler: Handler

    // Runnable của auto
    private lateinit var runnable: Runnable

    // Marker id để check marker nào đang được ấn
    private var curMarkerReport: Marker? = null

    // Marker id để check marker nào đang được ấn
    private var curMarkerUser: Marker? = null

    // Gesture Detector
    private lateinit var mDetector: GestureDetector

    // Place Info
    private var mPopupWindowPlaceInfo: PopupWindow? = null
    private var mPopupWindowRouteInfo: PopupWindow? = null
    private var mPopupWindowNavigationInfo: PopupWindow? = null
    private lateinit var viewNavigationPopup: View
    private var isPlaceInfoWindowUp = false
    private var currentSelectedPlace: Place? = null

    // Direction
    private lateinit var polylinePaths: MutableList<Polyline>
    private lateinit var currentPolyline: Polyline
    private lateinit var viewRoutePopup: View
    private var isRouteInfoWindowUp: Boolean = false
    private var isNavigationInfoWindowUp = false
    private var currentStepsLayout: androidx.recyclerview.widget.RecyclerView? = null
    private var currentDirectionRoute: ArrayList<SimplePlace> = ArrayList()

    private lateinit var viewDirectionPopup: View
    private lateinit var viewEditDirectionPopup: View
    private var mPopupWindowDirectionInfo: PopupWindow? = null
    private var mPopupWindowEditDirection: PopupWindow? = null
    private var isDirectionInfoWindowUp: Boolean = false
    private var isEditDirectionWindowUp: Boolean = false

    // setting hiện tại của socket
    private var currentSocketSetting: String? = null

    // AudioPlayer
    private var mAudioPlayer = AudioPlayer()

    // String step cũ để so sánh
    private lateinit var oldStep: Step

    // Settings
    private var isTouchSoundsEnabled: Boolean = false
    private var isTouchVibrateEnabled: Boolean = false

    private lateinit var viewAddPlacePopup: View
    private var mPopupWindowAddPlace: PopupWindow? = null
    private var isAddPlaceWindowUp = false

    private var currentReportMarkerPopupOnNavigation: Marker? = null

    private var haveNotReadSecondTime = true
    private var countOutOfRoute = 0

    // Animation
    private lateinit var qnabOpen: Animation
    private lateinit var qnabClose: Animation
    private var isOpen: Boolean = false

    //---------------------------------CAUTIOUS-----------------------------------

    lateinit var placesClient:PlacesClient
    private var placeFields = listOf(Place.Field.ID,
        Place.Field.NAME,
        Place.Field.ADDRESS,
        Place.Field.LAT_LNG)
    //----------------------------------------------------------------------------

    //-------------------------------------Outdated---------------------------------
//    private lateinit var progressDialog: ProgressDialog
    private var loadingAlert: AlertDialog? = null

    // ==================================================================================================================================== //
    // ======== VỀ DIRECTION ============================================================================================================== //
    // ==================================================================================================================================== //
    @SuppressLint("InflateParams")
    private fun showPlaceInfoPopup(place: Place) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val viewPlacePopup = inflater.inflate(R.layout.place_info_layout, null)
        mPopupWindowPlaceInfo = PopupWindow(viewPlacePopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
        mPopupWindowPlaceInfo!!.showAtLocation(parent, Gravity.BOTTOM, 0, 0)
        isPlaceInfoWindowUp = true

        val tvPlaceName = viewPlacePopup.findViewById<TextView>(R.id.tvPlaceName_place_info)
        val tvPlaceAddress = viewPlacePopup.findViewById<TextView>(R.id.tvPlaceAddress_place_info)
        val btnStartDirection = viewPlacePopup.findViewById<Button>(R.id.btnStartDirection_place_info)
        val btnSelectedPlace = viewPlacePopup.findViewById<LinearLayout>(R.id.btnSelectedPlace_place_info)

        tvPlaceName.text = place.name
        tvPlaceAddress.text = place.address

        btnStartDirection.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            // Chạy audio
            if (AppController.soundMode == 1) {
                when (AppController.voiceType) {
                    1 -> {
                        mAudioPlayer.play(this, R.raw.chi_dan_duong_di)
                    }
                    2 -> {
                        mAudioPlayer.play(this, R.raw.chi_dan_duong_di_2)
                    }
                }
            }
            currentDirectionRoute.clear()
            currentDirectionRoute.add(SimplePlace("Vị trí của bạn", LatLng(lastLocation.latitude, lastLocation.longitude)))
            currentDirectionRoute.add(SimplePlace(place.name.toString(), LatLng(place.latLng!!.latitude, place.latLng!!.longitude)))
            onBtnStartDirectionClick(currentDirectionRoute)
        }

        btnSelectedPlace.setOnClickListener {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.latLng, 17f))
        }
    }


    private fun onBtnStartDirectionClick(places: ArrayList<SimplePlace>) {
        if (places.size < 2)
            return
        if (isRouteInfoWindowUp)
            dismissPopupWindowRouteInfo()
        val origin = places[0].location!!.latitude.toString() + "," + places[0].location!!.longitude.toString()

        val destination = places[places.size - 1].location!!.latitude.toString() + "," + places[places.size - 1].location!!.longitude.toString()

        val waypoints = ArrayList<SimplePlace>()

        for (i in 1 until places.size - 1) {
            waypoints.add(places[i])
        }

        try {
            DirectionFinder(this, origin, destination, waypoints).execute()
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
    }

    private fun removeCurrentDirectionPolyline() {
        if (::polylinePaths.isInitialized) {
            for (polyline in polylinePaths) {
                polyline.remove()
            }
            polylinePaths.clear()
        }

        for (marker in waypointsOnRouteMarkers) {
            marker.remove()
        }
        waypointsOnRouteMarkers.clear()
    }

    override fun onDirectionFinderStart() {
        removeCurrentDirectionPolyline()

        showLoadingDialog()
    }

    override fun onDirectionFinderSuccess(routes: List<Route>) {
        // outdate progressDialog
        hideLoadingDialog()
        // progressDialog.dismiss()

        dismissPopupWindowPlaceInfo()
        // show Direction Info Popup
        showDirectionInfoPopup()

        listReportMarkerPassedCurrentRoute.clear()
        polylinePaths = ArrayList()

        if (routes.size == 1 && routes[0].legs!!.size > 1) {
            var firstLeg = true
            for (leg in routes[0].legs!!) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(leg.startLocation, 16f))

                val polylineOptions = PolylineOptions().geodesic(true).width(10f).color(Color.GRAY)

                val polyline = drawPolyline(routes[0], polylineOptions, leg)

                if (firstLeg) {
                    firstLeg = false
                    currentPolyline = polyline
                    currentRoute = routes[0]
                    currentPolyline.zIndex = 1F
                    currentPolyline.color = Color.BLUE
                }
            }
            markWaypointsOnRoute(currentDirectionRoute)
            showRouteInfoPopup(routes[0])
        } else {
            var firstRoute = true
            for (route in routes) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16f))

                val polylineOptions = PolylineOptions().geodesic(true).width(10f).color(Color.GRAY)

                val polyline = drawPolyline(route, polylineOptions)

                if (firstRoute) {
                    firstRoute = false
                    currentPolyline = polyline
                    currentRoute = route
                    currentPolyline.zIndex = 1F
                    currentPolyline.color = Color.BLUE
                    showRouteInfoPopup(route)
                }
            }
            markWaypointsOnRoute(currentDirectionRoute)
        }

    }

    private fun showDirectionInfoPopup() {
        if (isDirectionInfoWindowUp)
            return
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        viewDirectionPopup = inflater.inflate(R.layout.direction_layout, null)
        mPopupWindowDirectionInfo = PopupWindow(viewDirectionPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
        mPopupWindowDirectionInfo!!.showAtLocation(parent, Gravity.TOP, 0, 0)
        isDirectionInfoWindowUp = true

        val tvOrigin = viewDirectionPopup.findViewById<TextView>(R.id.tvOrigin_direction_layout)
        val tvWayPoints = viewDirectionPopup.findViewById<TextView>(R.id.tvWaypoints_direction_layout)
        val tvDestination = viewDirectionPopup.findViewById<TextView>(R.id.tvDestination_direction_layout)
        val btnEdit = viewDirectionPopup.findViewById<ImageView>(R.id.btnEdit_direction_layout)
        val btnBack = viewDirectionPopup.findViewById<ImageView>(R.id.btnBack_direction_layout)

        if (currentDirectionRoute.size > 1) {
            tvOrigin.text = "Từ: " + currentDirectionRoute[0].name
            if (currentDirectionRoute.size < 3) {
                tvWayPoints.visibility = View.GONE
            } else {
                tvWayPoints.visibility = View.VISIBLE
                tvWayPoints.text = "Qua: " + (currentDirectionRoute.size - 2).toString() + " điểm dừng"
            }

            tvDestination.text = "Đến: " + currentDirectionRoute[currentDirectionRoute.size - 1].name
        }

        btnEdit.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            // Chạy audio
            if (AppController.soundMode == 1) {
                when (AppController.voiceType) {
                    1 -> {
                        mAudioPlayer.play(this, R.raw.chinh_sua_lo_trinh)
                    }
                    2 -> {
                        mAudioPlayer.play(this, R.raw.chinh_sua_lo_trinh_2)
                    }
                }
            }
            dismissPopupWindowDirectionInfo()
            showEditDirectionPopup()
        }

        btnBack.setOnClickListener {
            if (::polylinePaths.isInitialized && polylinePaths.isNotEmpty() && isRouteInfoWindowUp && isDirectionInfoWindowUp) {
                removeCurrentDirectionPolyline()
                dismissPopupWindowRouteInfo()
                dismissPopupWindowDirectionInfo()
            }
        }
    }

    private fun showEditDirectionPopup() {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        viewEditDirectionPopup = inflater.inflate(R.layout.edit_direction_layout, null)
        mPopupWindowEditDirection = PopupWindow(viewEditDirectionPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
        mPopupWindowEditDirection!!.showAtLocation(parent, Gravity.TOP, 0, 0)
        isEditDirectionWindowUp = true

        val btnBack = viewEditDirectionPopup.findViewById<ImageView>(R.id.btnBack_edit_direction_layout)
        val btnDone = viewEditDirectionPopup.findViewById<TextView>(R.id.btnDone_edit_direction_layout)
        val btnAdd = viewEditDirectionPopup.findViewById<TextView>(R.id.btnAdd_edit_direction_layout)

        initDirectionRecyclerView(currentDirectionRoute, viewEditDirectionPopup, btnAdd)

        btnBack.setOnClickListener {
            onFinishEditDirection()
        }

        btnDone.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            // Chạy audio
            if (AppController.soundMode == 1) {
                when (AppController.voiceType) {
                    1 -> {
                        mAudioPlayer.play(this, R.raw.lo_trinh_da_cap_nhat)
                    }
                    2 -> {
                        mAudioPlayer.play(this, R.raw.lo_trinh_da_cap_nhat_2)
                    }
                }
            }
            onFinishEditDirection()
        }
    }

    private fun onFinishEditDirection() {
        dismissPopupWindowEditDirection()
        if (nearbyPlacesResultMarkers.size > 0) {
            for (i in 0 until nearbyPlacesResultMarkers.size) {
                nearbyPlacesResultMarkers[i].remove()
            }
            nearbyPlacesResultMarkers.clear()
        }
        onBtnStartDirectionClick(currentDirectionRoute)
    }

    private fun dismissAddPlacePopup() {
        mPopupWindowAddPlace?.dismiss()
        isAddPlaceWindowUp = false
    }

    private fun showAddPlacePopup(myDataSet: ArrayList<SimplePlace>, adapter: PlaceAdapter) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (!::viewAddPlacePopup.isInitialized) {
            viewAddPlacePopup = inflater.inflate(R.layout.place_picker_layout_new, null)
        }
        mPopupWindowAddPlace = PopupWindow(viewAddPlacePopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
        mPopupWindowAddPlace!!.showAtLocation(parent, Gravity.TOP, 0, 0)
        isAddPlaceWindowUp = true

        val autoCompleteFragment = supportFragmentManager.findFragmentById(R.id.place_autocomplete_place_picker_layout_new) as AutocompleteSupportFragment
        autoCompleteFragment.setPlaceFields(placeFields)
        autoCompleteFragment.setText(null)
        autoCompleteFragment.setOnPlaceSelectedListener(object:com.google.android.libraries.places.widget.listener.PlaceSelectionListener{
            override fun onPlaceSelected(p0: Place) {
                Log.d("Maps", "Place selected: " + p0.name)
                myDataSet.add(SimplePlace(p0.name.toString(), LatLng(p0.latLng!!.latitude, p0.latLng!!.longitude)))
                adapter.notifyDataSetChanged()
                dismissAddPlacePopup()
            }

            override fun onError(p0: Status) {
                Toast.makeText(this@MainActivity, ""+p0.statusMessage, Toast.LENGTH_SHORT).show()
            }
        })


        val btnNearbyGasStations = viewAddPlacePopup.findViewById<LinearLayout>(R.id.btnNearByGasStations_place_picker_layout_new)
        val btnNearbyParkings = viewAddPlacePopup.findViewById<LinearLayout>(R.id.btnNearByParkings_place_picker_layout_new)
        val btnNearbyCoffeeShops = viewAddPlacePopup.findViewById<LinearLayout>(R.id.btnNearByCoffeeShops_place_picker_layout_new)
        val btnNearbyRestaurants = viewAddPlacePopup.findViewById<LinearLayout>(R.id.btnNearByRestaurants_place_picker_layout_new)

        btnNearbyGasStations.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            // Chạy audio
            if (AppController.soundMode == 1) {
                when (AppController.voiceType) {
                    1 -> {
                        mAudioPlayer.play(this, R.raw.tram_xang_gan_day)
                    }
                    2 -> {
                        mAudioPlayer.play(this, R.raw.tram_xang_gan_day_2)
                    }
                }
            }
            getNearbyPlaces("gas_station", lastLocation, 1500, WAY_POINT)
            dismissAddPlacePopup()
        }
        btnNearbyParkings.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            // Chạy audio
            if (AppController.soundMode == 1) {
                when (AppController.voiceType) {
                    1 -> {
                        mAudioPlayer.play(this, R.raw.noi_dau_xe_gan_day)
                    }
                    2 -> {
                        mAudioPlayer.play(this, R.raw.noi_dau_xe_gan_day_2)
                    }
                }
            }
            getNearbyPlaces("parking", lastLocation, 1500, WAY_POINT)
            dismissAddPlacePopup()
        }
        btnNearbyCoffeeShops.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            // Chạy audio
            if (AppController.soundMode == 1) {
                when (AppController.voiceType) {
                    1 -> {
                        mAudioPlayer.play(this, R.raw.coffee_gan_day)
                    }
                    2 -> {
                        mAudioPlayer.play(this, R.raw.coffee_gan_day_2)
                    }
                }
            }
            getNearbyPlaces("cafe", lastLocation, 1500, WAY_POINT)
            dismissAddPlacePopup()
        }
        btnNearbyRestaurants.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            // Chạy audio
            if (AppController.soundMode == 1) {
                when (AppController.voiceType) {
                    1 -> {
                        mAudioPlayer.play(this, R.raw.nha_hang_gan_day)
                    }
                    2 -> {
                        mAudioPlayer.play(this, R.raw.nha_hang_gan_day_2)
                    }
                }
            }
            getNearbyPlaces("restaurant", lastLocation, 1500, WAY_POINT)
            dismissAddPlacePopup()
        }
    }

    private var waypointsOnRouteMarkers = ArrayList<Marker>()

    private fun markWaypointsOnRoute(places: ArrayList<SimplePlace>) {
        waypointsOnRouteMarkers.clear()
        for (i in 1 until places.size) {
            waypointsOnRouteMarkers.add(mMap.addMarker(MarkerOptions().position(LatLng(places[i].location!!.latitude, places[i].location!!.longitude))))
        }
    }

    private fun drawPolyline(route: Route, options: PolylineOptions): Polyline {
        for (leg in route.legs!!) {
            for(step in leg.steps!!){
                for(point in step.points!!){
                    options.add(point)
                }
            }
        }

        val polyline = mMap.addPolyline(options)
        polyline.isClickable = true
        polyline.tag = route
        polylinePaths.add(polyline)

        return polyline
    }

    private fun drawPolyline(route: Route, options: PolylineOptions, leg: Leg): Polyline {
        for (step in leg.steps!!) {
            for (point in step.points!!) {
                options.add(point)
            }
        }
        val polyline = mMap.addPolyline(options)
        polyline.isClickable = true
        polyline.tag = route
        polylinePaths.add(polyline)

        return polyline
    }

    override fun onPolylineClick(p0: Polyline) {
        if (p0 == currentPolyline || isNavigationInfoWindowUp)
            return
        p0.color = Color.BLUE
        currentPolyline.color = Color.GRAY
        p0.zIndex = 1F
        currentPolyline.zIndex = 0F
        currentPolyline = p0
        val currentRoute = currentPolyline.tag as Route
        updateUIRouteInfoPopup(currentRoute)
    }

    @SuppressLint("InflateParams")
    private fun showRouteInfoPopup(route: Route) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        viewRoutePopup = inflater.inflate(R.layout.steps_layout, null)
        mPopupWindowRouteInfo = PopupWindow(viewRoutePopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
        mPopupWindowRouteInfo!!.showAtLocation(parent, Gravity.BOTTOM, 0, 0)

        isRouteInfoWindowUp = true

        val tvRouteDuration = viewRoutePopup.findViewById<TextView>(R.id.tvDuration_route_info)
        val tvRouteDistance = viewRoutePopup.findViewById<TextView>(R.id.tvDistance_route_info)
        val tvReportCount = viewRoutePopup.findViewById<TextView>(R.id.tvNumReport_route_info)
        val btnStartNavigation = viewRoutePopup.findViewById<Button>(R.id.btnStartNavigation_route_info)
        val btnSteps = viewRoutePopup.findViewById<LinearLayout>(R.id.btnSteps_route_info)
        val tvBackToMap = viewRoutePopup.findViewById<TextView>(R.id.tvSteps_detail_route_info_layout)
        val dividerAboveRecyclerView = viewRoutePopup.findViewById<LinearLayout>(R.id.recycler_view_divider_steps_layout)
        val dividerAboveReportDetail = viewRoutePopup.findViewById<LinearLayout>(R.id.report_detail_divider_route_info_layout)

        if (currentDirectionRoute.size == 2) {
            tvRouteDuration.text = route.duration!!.text
            tvRouteDistance.text = route.distance!!.text
        } else {
            val seconds = route.duration!!.value.toLong()
            Log.d("TimeConvert", "text = " + route.duration!!.text)
            Log.d("TimeConvert", "second = " + seconds.toString())
            val numHour = TimeUnit.SECONDS.toHours(seconds).toInt()
            Log.d("TimeConvert", "numHour = $numHour")
            val numMinute = (TimeUnit.SECONDS.toMinutes(seconds) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(seconds))).toInt()
            Log.d("TimeConvert", "numMinute = $numMinute")
            var convertedDuration = ""
            val convertedDistance = route.distance!!.value / 1000
            if (numHour > 0) {
                convertedDuration += numHour.toString() + " giờ "
            }
            convertedDuration += numMinute.toString() + " phút"

            tvRouteDuration.text = convertedDuration
            tvRouteDistance.text = convertedDistance.toString() + " km"
        }

        btnStartNavigation.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            // Chạy audio
            if (AppController.soundMode == 1) {
                when (AppController.voiceType) {
                    1 -> {
                        mAudioPlayer.play(this, R.raw.bat_dau_di)
                    }
                    2 -> {
                        mAudioPlayer.play(this, R.raw.bat_dau_di_2)
                    }
                }
            }
            onBtnStartNavigationClick(route)
        }

        btnSteps.setOnClickListener {
            val recyclerView = viewRoutePopup.findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycler_view_steps_layout)
            if (recyclerView.visibility == View.GONE) {
                recyclerView.visibility = View.VISIBLE
                currentStepsLayout = recyclerView
                initStepRecyclerView(route, viewRoutePopup)

                val layoutReport = viewRoutePopup.findViewById<LinearLayout>(R.id.layoutReport_detail)
                layoutReport.visibility = View.GONE
                tvBackToMap.text = "QUAY LẠI BẢN ĐỒ"
                dividerAboveRecyclerView.visibility = View.VISIBLE
            } else {
                currentStepsLayout!!.visibility = View.GONE
                currentStepsLayout = null
                if (listReportMarkerCurrentRoute.size > 0) {
                    val layoutReport = viewRoutePopup.findViewById<LinearLayout>(R.id.layoutReport_detail)
                    layoutReport.visibility = View.VISIBLE
                }
                tvBackToMap.text = "CHI TIẾT CÁC BƯỚC"
                dividerAboveRecyclerView.visibility = View.GONE
            }
        }

        val currentRoute = currentPolyline.tag as Route
        listReportMarkerCurrentRoute = ArrayList()
        listReportCurrentRoute = ArrayList()
        updateListReportMarkerCurrentRoute()

        listReportMarkerCurrentRoute.sortedWith(compareBy { (it.tag as Report).distance })

        for (i in 0 until listReportMarkerCurrentRoute.size) {
            listReportCurrentRoute.add(listReportMarkerCurrentRoute[i].tag as Report)
        }

        val layoutReport = viewRoutePopup.findViewById<LinearLayout>(R.id.layoutReport_detail)

        Log.v("ReportCount", "NumReport = " + listReportMarkerCurrentRoute.size.toString())

        if (listReportMarkerCurrentRoute.size > 0) {
            dividerAboveReportDetail.visibility = View.VISIBLE
            tvReportCount.visibility = View.VISIBLE
            tvReportCount.text = listReportMarkerCurrentRoute.size.toString() + " báo hiệu"

            val btnPreviousReport = viewRoutePopup.findViewById<ImageView>(R.id.btnPrevious_report_detail)
            val btnNextReport = viewRoutePopup.findViewById<ImageView>(R.id.btnNext_report_detail)
            val btnCurrentReport = viewRoutePopup.findViewById<LinearLayout>(R.id.btnCurrent_report_detail)
            val progressBar = viewRoutePopup.findViewById<ProgressBar>(R.id.progressBar_report_detail)
            progressBar.max = listReportMarkerCurrentRoute.size
            if (listReportMarkerCurrentRoute.size == 1) {
                progressBar.visibility = View.GONE
                btnNextReport.visibility = View.INVISIBLE
                btnPreviousReport.visibility = View.INVISIBLE
            }
            progressBar.progress = 1

            currentReportIndex = 0

            updateUIReportDetail(listReportCurrentRoute[currentReportIndex], viewRoutePopup)

            btnCurrentReport.setOnClickListener {
                mMap.animateCamera(CameraUpdateFactory.newLatLng(listReportMarkerCurrentRoute[currentReportIndex].position))
            }

            btnPreviousReport.setOnClickListener {
                if (currentReportIndex > 0) {
                    currentReportIndex--
                    progressBar.progress--
                    Log.v("ReportCount", "currentReportIndex = " + currentReportIndex.toString())

                    updateUIReportDetail(listReportCurrentRoute[currentReportIndex], viewRoutePopup)
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(listReportMarkerCurrentRoute[currentReportIndex].position))
                }
            }
            btnNextReport.setOnClickListener {
                if (currentReportIndex + 1 < listReportMarkerCurrentRoute.size) {
                    currentReportIndex++
                    progressBar.progress++
                    Log.v("ReportCount", "currentReportIndex = " + currentReportIndex.toString())

                    updateUIReportDetail(listReportCurrentRoute[currentReportIndex], viewRoutePopup)
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(listReportMarkerCurrentRoute[currentReportIndex].position))
                }
            }
        } else {
            layoutReport.visibility = View.GONE
            dividerAboveReportDetail.visibility = View.GONE
        }
    }

    private var currentReportIndex: Int = 0

    private fun updateUIRouteInfoPopup(route: Route) {
        if (!isRouteInfoWindowUp || !::viewRoutePopup.isInitialized)
            return
        val tvRouteDuration = viewRoutePopup.findViewById<TextView>(R.id.tvDuration_route_info)
        val tvRouteDistance = viewRoutePopup.findViewById<TextView>(R.id.tvDistance_route_info)
        val tvReportCount = viewRoutePopup.findViewById<TextView>(R.id.tvNumReport_route_info)
        val btnStartNavigation = viewRoutePopup.findViewById<Button>(R.id.btnStartNavigation_route_info)
        val btnSteps = viewRoutePopup.findViewById<LinearLayout>(R.id.btnSteps_route_info)
        val tvBackToMap = viewRoutePopup.findViewById<TextView>(R.id.tvSteps_detail_route_info_layout)
        val dividerAboveRecyclerView = viewRoutePopup.findViewById<LinearLayout>(R.id.recycler_view_divider_steps_layout)
        val dividerAboveReportDetail = viewRoutePopup.findViewById<LinearLayout>(R.id.report_detail_divider_route_info_layout)

        tvRouteDuration.text = route.duration!!.text
        tvRouteDistance.text = route.distance!!.text

        btnStartNavigation.setOnClickListener {
            onBtnStartNavigationClick(route)
        }

        btnSteps.setOnClickListener {
            val recyclerView = viewRoutePopup.findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycler_view_steps_layout)
            if (recyclerView.visibility == View.GONE) {
                recyclerView.visibility = View.VISIBLE
                currentStepsLayout = recyclerView
                initStepRecyclerView(route, viewRoutePopup)

                val layoutReport = viewRoutePopup.findViewById<LinearLayout>(R.id.layoutReport_detail)
                layoutReport.visibility = View.GONE
                tvBackToMap.text = "QUAY LẠI BẢN ĐỒ"
                dividerAboveRecyclerView.visibility = View.VISIBLE
            } else {
                currentStepsLayout!!.visibility = View.GONE
                currentStepsLayout = null
                if (listReportMarkerCurrentRoute.size > 0) {
                    val layoutReport = viewRoutePopup.findViewById<LinearLayout>(R.id.layoutReport_detail)
                    layoutReport.visibility = View.VISIBLE
                }
                tvBackToMap.text = "CHI TIẾT CÁC BƯỚC"
                dividerAboveRecyclerView.visibility = View.GONE
            }
        }

        // Count report on route
        val currentRoute = currentPolyline.tag as Route
        listReportMarkerCurrentRoute = ArrayList()
        listReportCurrentRoute = ArrayList()

        for (i in 0 until listReportMarker.size) {
            if (PolyUtil.isLocationOnPath(LatLng(listReportMarker[i].position.latitude, listReportMarker[i].position.longitude), currentRoute.points, true, REPORT_ON_ROUTE_DISTANCE_DIRECTION)) {
                listReportMarkerCurrentRoute.add(listReportMarker[i])
            }
        }

        listReportMarkerCurrentRoute.sortedWith(compareBy { (it.tag as Report).distance })

        for (i in 0 until listReportMarkerCurrentRoute.size) {
            listReportCurrentRoute.add(listReportMarkerCurrentRoute[i].tag as Report)
        }

        val layoutReport = viewRoutePopup.findViewById<LinearLayout>(R.id.layoutReport_detail)

        Log.v("ReportCount", "NumReport = " + listReportMarkerCurrentRoute.size.toString())

        if (listReportMarkerCurrentRoute.size > 0) {
            dividerAboveReportDetail.visibility = View.VISIBLE
            layoutReport.visibility = View.VISIBLE
            tvReportCount.visibility = View.VISIBLE
            tvReportCount.text = listReportMarkerCurrentRoute.size.toString() + " báo hiệu"

            val btnPreviousReport = viewRoutePopup.findViewById<ImageView>(R.id.btnPrevious_report_detail)
            val btnNextReport = viewRoutePopup.findViewById<ImageView>(R.id.btnNext_report_detail)
            val btnCurrentReport = viewRoutePopup.findViewById<LinearLayout>(R.id.btnCurrent_report_detail)
            val progressBar = viewRoutePopup.findViewById<ProgressBar>(R.id.progressBar_report_detail)
            progressBar.max = listReportMarkerCurrentRoute.size
            if (listReportMarkerCurrentRoute.size == 1) {
                progressBar.visibility = View.GONE
                btnNextReport.visibility = View.INVISIBLE
                btnPreviousReport.visibility = View.INVISIBLE
            } else {
                progressBar.visibility = View.VISIBLE
                btnNextReport.visibility = View.VISIBLE
                btnPreviousReport.visibility = View.VISIBLE
            }
            progressBar.progress = 1

            var currentReportIndex = 0

            updateUIReportDetail(listReportCurrentRoute[currentReportIndex], viewRoutePopup)

            btnCurrentReport.setOnClickListener {
                mMap.animateCamera(CameraUpdateFactory.newLatLng(listReportMarkerCurrentRoute[currentReportIndex].position))
            }

            btnPreviousReport.setOnClickListener {
                if (currentReportIndex > 0) {
                    currentReportIndex--
                    progressBar.progress--
                    Log.v("ReportCount", "currentReportIndex = " + currentReportIndex.toString())

                    updateUIReportDetail(listReportCurrentRoute[currentReportIndex], viewRoutePopup)
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(listReportMarkerCurrentRoute[currentReportIndex].position))
                }
            }
            btnNextReport.setOnClickListener {
                if (currentReportIndex + 1 < listReportMarkerCurrentRoute.size) {
                    currentReportIndex++
                    progressBar.progress++
                    Log.v("ReportCount", "currentReportIndex = " + currentReportIndex.toString())

                    updateUIReportDetail(listReportCurrentRoute[currentReportIndex], viewRoutePopup)
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(listReportMarkerCurrentRoute[currentReportIndex].position))
                }
            }
        } else {
            layoutReport.visibility = View.GONE
            dividerAboveReportDetail.visibility = View.GONE
        }
    }

    private fun updateUIReportDetail(report: Report, view: View) {
        val imReportIcon = view.findViewById<ImageView>(R.id.imIcon_report_detail)
        val tvReportType = view.findViewById<TextView>(R.id.tvType_report_detail)
        val tvReportDistance = view.findViewById<TextView>(R.id.tvDistance_report_detail)
        val tvReportAddress = view.findViewById<TextView>(R.id.tvAddress_report_detail)

        // Làm tròn số double
        val decimalFormat = DecimalFormat("#")
        decimalFormat.roundingMode = RoundingMode.CEILING

        val location = Location("tempLocation")
        location.latitude = report.geometry!!.coordinates!![1]
        location.longitude = report.geometry!!.coordinates!![0]

        val tmpDistance = lastLocation.distanceTo(location).toInt()
        tvReportDistance.text = "Cách " + tmpDistance.toString() + " m"
        Log.d("UpdateListReport", tvReportDistance.text as String)

        // Lấy địa chỉ sử dụng Geocoder
        try {
            val geocoder = Geocoder(this, Locale.getDefault())
            val yourAddresses: List<Address>
            yourAddresses = geocoder.getFromLocation(report.geometry!!.coordinates!![1], report.geometry!!.coordinates!![0], 1)

            if (yourAddresses.isNotEmpty()) {
                val address = yourAddresses.get(0).thoroughfare + ", " + yourAddresses.get(0).locality + ", " + yourAddresses.get(0).subAdminArea
                tvReportAddress.text = address
            }

        } catch (ex: Exception) {
        }

        when (report.type) {
            "traffic" -> {
                imReportIcon.background = getDrawable(R.drawable.bg_btn_report_traffic)
            }
            "crash" -> {
                imReportIcon.background = getDrawable(R.drawable.bg_btn_report_crash)
            }
            "hazard" -> {
                imReportIcon.background = getDrawable(R.drawable.bg_btn_report_hazard)
            }
            "help" -> {
                imReportIcon.background = getDrawable(R.drawable.bg_btn_report_assistance)
            }
        }
    }

    @SuppressLint("InflateParams", "MissingPermission")
    private fun onBtnStartNavigationClick(route: Route) {
        dismissPopupWindowDirectionInfo()
        dismissPopupWindowNavigationInfo()

        currentReportMarkerPopupOnNavigation = null

        val routeInfoLayout = viewRoutePopup.findViewById<LinearLayout>(R.id.llRouteInfo_route_info_layout)
        val dividerAboveReportDetail = viewRoutePopup.findViewById<LinearLayout>(R.id.report_detail_divider_route_info_layout)
        val layoutReport = viewRoutePopup.findViewById<LinearLayout>(R.id.layoutReport_detail)
        routeInfoLayout.visibility = View.GONE
        dividerAboveReportDetail.visibility = View.INVISIBLE
        if (layoutReport.visibility == View.VISIBLE) {
            val progressBar = viewRoutePopup.findViewById<ProgressBar>(R.id.progressBar_report_detail)
            val btnPreviousReport = viewRoutePopup.findViewById<ImageView>(R.id.btnPrevious_report_detail)
            val btnNextReport = viewRoutePopup.findViewById<ImageView>(R.id.btnNext_report_detail)
            btnPreviousReport.visibility = View.INVISIBLE
            btnNextReport.visibility = View.INVISIBLE
            progressBar.visibility = View.INVISIBLE

            updateListReportMarkerCurrentRoute()
            currentReportIndex = 0
            progressBar.progress = 1
            listReportMarkerCurrentRoute.sortedWith(compareBy { (it.tag as Report).distance })
            if (listReportMarkerCurrentRoute.size > 0) {
                Log.d("UpdateListReport", "List size > 0")
                if (!::nextReportMarkerPopupOnNavigation.isInitialized) {
                    nextReportMarkerPopupOnNavigation = listReportMarkerCurrentRoute[0]
                    nextReportPopupOnNavigation = listReportMarkerCurrentRoute[0].tag as Report
                    Log.d("UpdateListReport", "nextReportMarkerPopupOnNavigation = listReportMarkerCurrentRoute[0]")
                }
                updateUIReportDetail(nextReportPopupOnNavigation, viewRoutePopup)
            } else {
                Log.d("UpdateListReport", "List size = 0")
            }
        } else {

        }


        fusedLocationClient.lastLocation.addOnSuccessListener {
            if (it != null) {
                lastLocation = it
            }
            if (::lastLocation.isInitialized) {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(lastLocation.latitude, lastLocation.longitude)))
                // Phải cách trong code vì nếu để cùng loại animate, và gần nhau thì 1 trong 2 cái ko kịp thực hiện làm ko thể cập nhật vị trí theo thời gian
                // moveCamera cho điểm, animateCamera cho CameraPosition
                val camPos = CameraPosition.builder()
                        .target(mMap.cameraPosition.target)
                        .zoom(20f)
                        .tilt(80f)
                        .bearing(lastLocation.bearing)
                        .build()
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPos))
                mMap.uiSettings.setAllGesturesEnabled(false)
            }
        }

        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        viewNavigationPopup = inflater.inflate(R.layout.navigation_layout, null)
        mPopupWindowNavigationInfo = PopupWindow(viewNavigationPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
        mPopupWindowNavigationInfo!!.showAtLocation(parent, Gravity.TOP, 0, 0)
        isNavigationInfoWindowUp = true

        val imInstruction = viewNavigationPopup.findViewById<ImageView>(R.id.imInstruction_navigation_layout)
        val tvInstruction = viewNavigationPopup.findViewById<TextView>(R.id.tvInstruction_navigation_layout)
        val tvDistance = viewNavigationPopup.findViewById<TextView>(R.id.tvDistance_navigation_layout)

        // imInstruction set source
        val currentStep = getNavigationInstruction(route)
        tvInstruction.text = currentStep.instruction
        tvDistance.text = currentStep.distance!!.text
        when (currentStep.maneuver) {
            "ferry" -> {
                imInstruction.setImageResource(R.drawable.ferry)
            }
            "ferry-train" -> {
                imInstruction.setImageResource(R.drawable.ferry_train)
            }
            "fork-left" -> {
                imInstruction.setImageResource(R.drawable.fork_left)
            }
            "fork-right" -> {
                imInstruction.setImageResource(R.drawable.fork_right)
            }
            "keep-left" -> {
                imInstruction.setImageResource(R.drawable.keep_left)
            }
            "keep-right" -> {
                imInstruction.setImageResource(R.drawable.keep_right)
            }
            "merge" -> {
                imInstruction.setImageResource(R.drawable.merge)
            }
            "ramp-left" -> {
                imInstruction.setImageResource(R.drawable.ramp_left)
            }
            "ramp-right" -> {
                imInstruction.setImageResource(R.drawable.ramp_right)
            }
            "roundabout-left" -> {
                imInstruction.setImageResource(R.drawable.roundabout_left)
            }
            "roundabout-right" -> {
                imInstruction.setImageResource(R.drawable.roundabout_right)
            }
            "straight" -> {
                imInstruction.setImageResource(R.drawable.straight)
            }
            "turn-left" -> {
                imInstruction.setImageResource(R.drawable.turn_left)
            }
            "turn-right" -> {
                imInstruction.setImageResource(R.drawable.turn_right)
            }
            "turn-sharp-left" -> {
                imInstruction.setImageResource(R.drawable.turn_sharp_left)
            }
            "turn-sharp-right" -> {
                imInstruction.setImageResource(R.drawable.turn_sharp_right)
            }
            "turn-slight-left" -> {
                imInstruction.setImageResource(R.drawable.turn_slight_left)
            }
            "turn-slight-right" -> {
                imInstruction.setImageResource(R.drawable.turn_slight_right)
            }
            "uturn-left" -> {
                imInstruction.setImageResource(R.drawable.uturn_left)
            }
            "uturn-right" -> {
                imInstruction.setImageResource(R.drawable.uturn_right)
            }
        }
    }

    private fun getNavigationEndLocation(route: Route): LatLng? {
        for (iL in 0 until route.legs!!.size) {
            for (iS in 0 until route.legs!![iL].steps!!.size) {
                if (PolyUtil.isLocationOnPath(LatLng(lastLocation.latitude, lastLocation.longitude), route.legs!![iL].steps!![iS].points, true, REPORT_ON_ROUTE_DISTANCE_DIRECTION)) {
                    return route.legs!![iL].steps!![iS].endLocation
                }
            }
        }
        return route.legs!![route.legs!!.size - 1].steps!![route.legs!![route.legs!!.size - 1].steps!!.size - 1].endLocation
    }

    private fun isOutOfRoute(route: Route): Boolean {
        for (iL in 0 until route.legs!!.size) {
            for (iS in 0 until route.legs!![iL].steps!!.size) {
                if (PolyUtil.isLocationOnPath(LatLng(lastLocation.latitude, lastLocation.longitude), route.legs!![iL].steps!![iS].points, true, REPORT_ON_ROUTE_DISTANCE_DIRECTION)) {
                    return false
                }
            }
        }
        return true
    }

    @SuppressLint("MissingPermission")
    private fun updateUINavigation(route: Route) {
        if (!mPopupWindowNavigationInfo!!.isShowing || !::viewNavigationPopup.isInitialized || !::lastLocation.isInitialized) {
            Log.d("TestCam", "Called but fail")
            return
        }
        Log.d("TestCam", "Called success")
        val routeInfoLayout = viewRoutePopup.findViewById<LinearLayout>(R.id.llRouteInfo_route_info_layout)
        val dividerAboveReportDetail = viewRoutePopup.findViewById<LinearLayout>(R.id.report_detail_divider_route_info_layout)
        routeInfoLayout.visibility = View.GONE
        dividerAboveReportDetail.visibility = View.INVISIBLE

        if (isOutOfRoute(route)) {
            countOutOfRoute++
        } else {
            countOutOfRoute = 0
        }
//        Log.d("ReDirection", "countOutOfRoute = " + countOutOfRoute.toString())

//        if (listReportMarkerPassedCurrentRoute.size>=listReportMarkerCurrentRoute)
        val layoutReport = viewRoutePopup.findViewById<LinearLayout>(R.id.layoutReport_detail)
        var passedAllReport = true
        for (reportMarker in listReportMarkerCurrentRoute) {
            if (!listReportMarkerPassedCurrentRoute.contains(reportMarker)) {
                passedAllReport = false
                break
            }
        }
        if (passedAllReport)
            layoutReport.visibility = View.GONE

        if (layoutReport.visibility == View.VISIBLE) {
//            Log.d("UpdateListReport","layoutReport visible")
            val progressBar = viewRoutePopup.findViewById<ProgressBar>(R.id.progressBar_report_detail)
            val btnPreviousReport = viewRoutePopup.findViewById<ImageView>(R.id.btnPrevious_report_detail)
            val btnNextReport = viewRoutePopup.findViewById<ImageView>(R.id.btnNext_report_detail)
            btnPreviousReport.visibility = View.INVISIBLE
            btnNextReport.visibility = View.INVISIBLE
            progressBar.visibility = View.INVISIBLE
            if (::nextReportMarkerPopupOnNavigation.isInitialized) {
                updateUIReportDetail(nextReportPopupOnNavigation, viewRoutePopup)
            } else {
                updateListReportMarkerCurrentRoute()
                listReportMarkerCurrentRoute.sortedWith(compareBy { (it.tag as Report).distance })
                if (listReportMarkerCurrentRoute.size > 0) {
                    Log.d("UpdateListReport", "List size > 0")
                    if (!::nextReportMarkerPopupOnNavigation.isInitialized) {
                        nextReportMarkerPopupOnNavigation = listReportMarkerCurrentRoute[0]
                        nextReportPopupOnNavigation = listReportMarkerCurrentRoute[0].tag as Report
                        Log.d("UpdateListReport", "nextReportMarkerPopupOnNavigation = listReportMarkerCurrentRoute[0]")
                    }
                    updateUIReportDetail(nextReportPopupOnNavigation, viewRoutePopup)
                } else {
                    Log.d("UpdateListReport", "List size = 0")
                }
            }
        }

        if (countOutOfRoute >= 4) {
            val newRoute = ArrayList<SimplePlace>()
            newRoute.add(SimplePlace("Vị trí của bạn", LatLng(lastLocation.latitude, lastLocation.longitude)))
            for (i in 1 until currentDirectionRoute.size) {
                newRoute.add(currentDirectionRoute[i])
            }
            currentDirectionRoute.clear()
            currentDirectionRoute = newRoute
//            onBackPressed()
//            onBackPressed()
//            onBtnStartDirectionClick(currentDirectionRoute)

            val currentChosenRoute = currentPolyline.tag as Route?

            if (currentChosenRoute != null) {
                Log.d("ReDirection", "newRoute success")
                onBtnStartNavigationClick(currentChosenRoute)
            } else {
                Log.d("ReDirection", "currentChosenRoute = null")
                onBtnStartNavigationClick(route)
            }
            Log.d("ReDirection", "ReDirection")
            countOutOfRoute = 0
            return
        }

        Log.d("TestCam", "Success")
        fusedLocationClient.lastLocation.addOnSuccessListener {
            if (it != null)
                lastLocation = it
            if (::lastLocation.isInitialized) {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(lastLocation.latitude, lastLocation.longitude)))
                // Phải cách trong code vì nếu để cùng loại animate, và gần nhau thì 1 trong 2 cái ko kịp thực hiện làm ko thể cập nhật vị trí theo thời gian
                // moveCamera cho điểm, animateCamera cho CameraPosition
                val camPos = CameraPosition.builder()
                        .target(mMap.cameraPosition.target)
                        .zoom(20f)
                        .tilt(80f)
                        .bearing(lastLocation.bearing)
                        .build()
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPos))
            }
        }

        val imInstruction = viewNavigationPopup.findViewById<ImageView>(R.id.imInstruction_navigation_layout)
        val tvInstruction = viewNavigationPopup.findViewById<TextView>(R.id.tvInstruction_navigation_layout)
        val tvDistance = viewNavigationPopup.findViewById<TextView>(R.id.tvDistance_navigation_layout)

        val currentStep = getNavigationInstruction(route)

        if (!::oldStep.isInitialized) {
            // NÓI
            sayVoiceNavigation(currentStep)
            haveNotReadSecondTime = true
            oldStep = currentStep
        } else {
            if (currentStep != oldStep) {
                // NÓI
                sayVoiceNavigation(currentStep)
                haveNotReadSecondTime = true
                oldStep = currentStep
            } else {
                val endLocation = getNavigationEndLocation(route)
                val results = FloatArray(3)
                if (endLocation != null) {
                    Location.distanceBetween(lastLocation.latitude, lastLocation.longitude, endLocation.latitude, endLocation.longitude, results)
                }
                if (results[0] <= 100 && haveNotReadSecondTime) {
                    // NÓI
                    sayVoiceNavigation(currentStep)
                    haveNotReadSecondTime = false
                }
            }

        }

        tvInstruction.text = currentStep.instruction
        tvDistance.text = currentStep.distance!!.text
        when (currentStep.maneuver) {
            "ferry" -> {
                imInstruction.setImageResource(R.drawable.ferry)
            }
            "ferry-train" -> {
                imInstruction.setImageResource(R.drawable.ferry_train)
            }
            "fork-left" -> {
                imInstruction.setImageResource(R.drawable.fork_left)
            }
            "fork-right" -> {
                imInstruction.setImageResource(R.drawable.fork_right)
            }
            "keep-left" -> {
                imInstruction.setImageResource(R.drawable.keep_left)
            }
            "keep-right" -> {
                imInstruction.setImageResource(R.drawable.keep_right)
            }
            "merge" -> {
                imInstruction.setImageResource(R.drawable.merge)
            }
            "ramp-left" -> {
                imInstruction.setImageResource(R.drawable.ramp_left)
            }
            "ramp-right" -> {
                imInstruction.setImageResource(R.drawable.ramp_right)
            }
            "roundabout-left" -> {
                imInstruction.setImageResource(R.drawable.roundabout_left)
            }
            "roundabout-right" -> {
                imInstruction.setImageResource(R.drawable.roundabout_right)
            }
            "straight" -> {
                imInstruction.setImageResource(R.drawable.straight)
            }
            "turn-left" -> {
                imInstruction.setImageResource(R.drawable.turn_left)
            }
            "turn-right" -> {
                imInstruction.setImageResource(R.drawable.turn_right)
            }
            "turn-sharp-left" -> {
                imInstruction.setImageResource(R.drawable.turn_sharp_left)
            }
            "turn-sharp-right" -> {
                imInstruction.setImageResource(R.drawable.turn_sharp_right)
            }
            "turn-slight-left" -> {
                imInstruction.setImageResource(R.drawable.turn_slight_left)
            }
            "turn-slight-right" -> {
                imInstruction.setImageResource(R.drawable.turn_slight_right)
            }
            "uturn-left" -> {
                imInstruction.setImageResource(R.drawable.uturn_left)
            }
            "uturn-right" -> {
                imInstruction.setImageResource(R.drawable.uturn_right)
            }
        }
    }

    private fun sayVoiceNavigation(step: Step) {
        when (step.maneuver) {
            "ferry" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_qua_pha)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_qua_pha_2)
                        }
                    }
                }
            }
            "ferry-train" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_qua_tau_pha)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_qua_tau_pha_2)
                        }
                    }
                }
            }
            "fork-left" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_trai_tai_nga_ba)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_trai_tai_nga_ba_2)
                        }
                    }
                }
            }
            "fork-right" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_phai_tai_nga_ba)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_phai_tai_nga_ba_2)
                        }
                    }
                }
            }
            "keep-left" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_giu_chay_lan_trai)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_giu_chay_lan_trai_2)
                        }
                    }
                }
            }
            "keep-right" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_giu_chay_lan_phai)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_giu_chay_lan_phai_2)
                        }
                    }
                }
            }
            "merge" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_chay_vao_duong_hop)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_chay_vao_duong_hop_2)
                        }
                    }
                }
            }
            "ramp-left" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_trai_vao_duong_noi)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_trai_vao_duong_noi_2)
                        }
                    }
                }
            }
            "ramp-right" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_phai_vao_duong_noi)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_phai_vao_duong_noi_2)
                        }
                    }
                }
            }
            "roundabout-left" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_vao_vong_xoay_trai)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_vao_vong_xoay_trai_2)
                        }
                    }
                }
            }
            "roundabout-right" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_vao_vong_xoay_phai)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_vao_vong_xoay_phai_2)
                        }
                    }
                }
            }
            "straight" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_di_thang)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_di_thang_2)
                        }
                    }
                }
            }
            "turn-left" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_trai)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_trai_2)
                        }
                    }
                }
            }
            "turn-right" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_phai)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_phai_2)
                        }
                    }
                }
            }
            "turn-sharp-left" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_manh_trai)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_manh_trai_2)
                        }
                    }
                }
            }
            "turn-sharp-right" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_manh_phai)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_manh_phai_2)
                        }
                    }
                }
            }
            "turn-slight-left" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_nhe_trai)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_nhe_trai_2)
                        }
                    }
                }
            }
            "turn-slight-right" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_nhe_phai)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_queo_nhe_phai_2)
                        }
                    }
                }
            }
            "uturn-left" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_vong_chu_u_trai)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_vong_chu_u_trai_2)
                        }
                    }
                }
            }
            "uturn-right" -> {
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_vong_chu_u_phai)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tiep_theo_vong_chu_u_phai_2)
                        }
                    }
                }
            }
        }
    }

    private fun getNavigationInstruction(route: Route): Step {
        for (iL in 0 until route.legs!!.size) {
            for (iS in 0 until route.legs!![iL].steps!!.size) {

                val options = PolylineOptions()
                options.color(Color.RED)
                options.width(5f)
                options.zIndex(2F)

                for (element in route.legs!![iL].steps!![iS].points!!) {
                    options.add(element)
                }

                if (PolyUtil.isLocationOnPath(LatLng(lastLocation.latitude, lastLocation.longitude), route.legs!![iL].steps!![iS].points, true, REPORT_ON_ROUTE_DISTANCE_DIRECTION)) {
                    // The polyline is composed of great circle segments if geodesic is true, and of Rhumb segments otherwise
                    Log.v("Navigation", "OnPathOK")
                    return if (iS + 1 < route.legs!![iL].steps!!.size) {
                        route.legs!![iL].steps!![iS + 1]
                    } else {
                        if (iL + 1 < route.legs!!.size) {
                            route.legs!![iL + 1].steps!![0]
                        } else {
                            route.legs!![iL].steps!![iS]
                        }
                    }
                }
            }
        }
        Log.v("Navigation", "OnPathFALSE")
        return route.legs!![0].steps!![0]
    }

    private fun dismissPopupWindowRouteInfo() {
        mPopupWindowRouteInfo?.dismiss()
        isRouteInfoWindowUp = false
        if (mPopupWindowPlaceInfo != null && !polylinePaths.isNotEmpty()) {
            val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
            mPopupWindowPlaceInfo!!.showAtLocation(parent, Gravity.BOTTOM, 0, 0)
            isPlaceInfoWindowUp = true
        }
    }

    private fun dismissPopupWindowNavigationInfo() {
        mPopupWindowNavigationInfo?.dismiss()
        isNavigationInfoWindowUp = false
        if (mPopupWindowDirectionInfo != null) {
            val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
            mPopupWindowDirectionInfo!!.showAtLocation(parent, Gravity.TOP, 0, 0)
            isDirectionInfoWindowUp = true
            val routeInfoLayout = viewRoutePopup.findViewById<LinearLayout>(R.id.llRouteInfo_route_info_layout)
            routeInfoLayout.visibility = View.VISIBLE
            val layoutReport = viewRoutePopup.findViewById<LinearLayout>(R.id.layoutReport_detail)
            if (layoutReport.visibility == View.VISIBLE) {
                val progressBar = viewRoutePopup.findViewById<ProgressBar>(R.id.progressBar_report_detail)
                val btnPreviousReport = viewRoutePopup.findViewById<ImageView>(R.id.btnPrevious_report_detail)
                val btnNextReport = viewRoutePopup.findViewById<ImageView>(R.id.btnNext_report_detail)
                btnPreviousReport.visibility = View.VISIBLE
                btnNextReport.visibility = View.VISIBLE
                progressBar.visibility = View.VISIBLE
            }
        }
    }

    private fun dismissPopupWindowDirectionInfo() {
        mPopupWindowDirectionInfo?.dismiss()
        isDirectionInfoWindowUp = false
    }

    private fun dismissPopupWindowEditDirection() {
        mPopupWindowEditDirection?.dismiss()
        isEditDirectionWindowUp = false
    }

    // ========================================================================================================================================= //
    // ======== VỀ MAIN ======================================================================================================================== //
    // ========================================================================================================================================= //
    // outdate locationAPI
//    private lateinit var placeAutoComplete: PlaceAutocompleteFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        initPlaces()
        setUpPlacesAutoComplete()

        obtainMapFragment()

        initActionBarDrawerToggle()

        // Load user profile
        loadUserProfile()

        // Khởi tạo socket
        currentSocketSetting = AppController.settingSocket
        if (currentSocketSetting == "true") {
            initSocket()
        }

        qnabOpen = AnimationUtils.loadAnimation(this, R.anim.qnab_open)
        qnabClose = AnimationUtils.loadAnimation(this, R.anim.qnab_close)

        // onClickListener cho các nút
        imvMyLoc.setOnClickListener(this)
        imvReport.setOnClickListener(this)
        imvBroadcast.setOnClickListener(this)
        imvQuickNav.setOnClickListener(this)
        imvQuickNavPetrol.setOnClickListener(this)
        imvQuickNavParking.setOnClickListener(this)

        // Khởi tạo các nút trên menu drawer
        initMenuItemDrawer()

        mContext = this.applicationContext
    }

    private fun initMenuItemDrawer() {
        layout_group.setOnClickListener(this)
        layout_setting.setOnClickListener(this)
        btnLogOut.setOnClickListener(this)
        btn_update_user_avatar.setOnClickListener(this)
        switchFilterDriver.isOn = AppController.settingFilterCar == "true"
        switchFilterReport.isOn = AppController.settingFilterReport == "true"
        switchTrafficLayer.isOn = AppController.settingTrafficLayer == "true"
        switchTrafficLayer.setOnToggledListener { _, isOn->
            if(isOn) {
                AppController.settingTrafficLayer = "true"
                mMap.isTrafficEnabled = true
            } else {
                AppController.settingTrafficLayer = "false"
                mMap.isTrafficEnabled = false
            }
        }
        switchFilterReport.setOnToggledListener { _, isOn ->
            if (isOn) {
                // Chạy audio
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.hien_bao_hieu)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.hien_bao_hieu_2)
                        }
                    }
                }
                AppController.settingFilterReport = "true"
                if (::listReport.isInitialized) {
                    if (listReport.isNotEmpty()) {
                        drawValidReports()
                    }
                }
            } else {
                // Chạy audio
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.an_bao_hieu)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.an_bao_hieu_2)
                        }
                    }
                }
                AppController.settingFilterReport = "false"
                for (i in 0 until listReportMarker.size) {
                    listReportMarker[i].remove()
                }
            }
        }
        switchFilterDriver.setOnToggledListener { _, isOn ->
            if(isOn) {
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.hien_tai_xe)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.hien_tai_xe_2)
                        }
                    }
                }
                AppController.settingFilterCar = "true"
                if (::listUser.isInitialized) {
                    if (listUser.isNotEmpty()) {
                        drawValidUsers()
                    }
                }
            }
            else {
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.an_tai_xe)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.an_tai_xe_2)
                        }
                    }
                }
                AppController.settingFilterCar = "false"
                for (i in 0 until listUserMarker.size) {
                    listUserMarker[i].remove()
                }
            }
        }
    }

    private fun initSoundVibrate() {
        if (Settings.System.getInt(contentResolver, Settings.System.SOUND_EFFECTS_ENABLED, 1) == 0) {
            Settings.System.putInt(contentResolver, Settings.System.SOUND_EFFECTS_ENABLED, 1)
            isTouchSoundsEnabled = true
        }
        if (Settings.System.getInt(contentResolver, Settings.System.HAPTIC_FEEDBACK_ENABLED, 1) == 0) {
            Settings.System.putInt(contentResolver, Settings.System.HAPTIC_FEEDBACK_ENABLED, 1)
            isTouchVibrateEnabled = true
        }
    }

    private fun destroySoundVibrate() {
        if (isTouchSoundsEnabled) {
            Settings.System.putInt(contentResolver, Settings.System.SOUND_EFFECTS_ENABLED, 0)
        }
        if (isTouchVibrateEnabled) {
            Settings.System.putInt(contentResolver, Settings.System.HAPTIC_FEEDBACK_ENABLED, 0)
        }
    }

    override fun onPause() {
        super.onPause()
        // stopLocationUpdates
        if (::fusedLocationClient.isInitialized) {
            fusedLocationClient.removeLocationUpdates(locationCallback)
        }
    }

    public override fun onStop() {
        super.onStop()
        // Xoá runnable thread
        handler.removeCallbacks(runnable)

        // Destroy sound and vibrate
        destroySoundVibrate()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    public override fun onResume() {
        super.onResume()
        val lng = intent.getDoubleExtra("appointment_location_lng", -1.0)
        val lat = intent.getDoubleExtra("appointment_location_lat", -1.0)
        val name = intent.getStringExtra("appointment_location_name")
        val address = intent.getStringExtra("appointment_location_address")
        val appointmentId = intent.getStringExtra("appointmentId")
        if(appointmentId != null) {
            switchFilterDriver.isOn = false
            AppController.settingFilterCar = "false"
        }
        if (::fusedLocationClient.isInitialized) {
            startLocationUpdates()
        }

        // Bắt sự kiện socket report other
        if (AppController.base64ImageReportOther != "" || AppController.licensePlate != "") {
            for (i in listUser.indices) {
                if (listUser[i].email != AppController.userProfile!!.email) {
                    attemptReportOther(AppController.userProfile!!.email!!,
                    AppController.userProfile!!.name!!,
                    listUser[i].socketID.toString(),
                    AppController.typeReportOther,
                    AppController.base64ImageReportOther,
                    AppController.licensePlate)
                }
            }
            AppController.base64ImageReportOther = ""
            AppController.licensePlate = ""
        }

        // Tự động thực hiện
        handler = Handler()
        runnable = object : Runnable {
            override fun run() {
                // Sau đó lặp lại mỗi 15s
                if (::lastLocation.isInitialized) {
                    val listGeo: List<Double> = listOf(lastLocation.longitude, lastLocation.latitude)
                    val newGeo = Geometry("Point", listGeo)
                    val user = User("", "", "", "", "", "", newGeo, 0.0, 0.0, 0.0, 0.0, null, "", emptyList(), emptyList(), emptyList(), "")
                    onUpdateCurrentLocation(user)

                    if(AppController.userProfile != null) {
                        // Cập nhật người dùng xung quanh
                        onGetNearbyUsers()

                        // Cập nhật biển báo xung quanh
                        onGetNearbyReports()
                    }
                }
                handler.postDelayed(this, 5000)
            }
        }
        val quickNavigateAppointment = object : Runnable {
            override fun run() {
                if(lat != -1.0 && lng != -1.0) {
                    if(this@MainActivity::mMap.isInitialized && ::lastLocation.isInitialized) {
                        val markerOptions = MarkerOptions()
                        markerOptions.position(LatLng(lat,lng))
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                        markerOptions.title("appointment_place")

                        val place = Place.builder()
                        place.setName(name)
                        place.setAddress(address)
                        place.setLatLng(LatLng(lat,lng))
                        currentSelectedPlace = place.build()

//                        currentDirectionRoute.clear()
//                        currentDirectionRoute.add(SimplePlace("Vị trí của bạn", LatLng(lastLocation.latitude, lastLocation.longitude)))
//                        currentDirectionRoute.add(SimplePlace(currentSelectedPlace!!.name, currentSelectedPlace!!.latLng))
//                        val wayPoints = currentDirectionRoute

                        currentSelectedPlaceMarker = mMap.addMarker(markerOptions)
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat,lng), 17f))
//                        DirectionFinder(this@MainActivity, lastLocation.latitude.toString()+","+lastLocation.longitude.toString(), "$lat,$lng", wayPoints).execute()
                        intent.removeExtra("appointment_location_lng")
                        intent.removeExtra("appointment_location_lat")
                        intent.removeExtra("appointment_location_address")
                        intent.removeExtra("appointment_location_name")
                        handler.removeCallbacks(this)
                    }
                } else {
                    handler.removeCallbacks(this)
                }
            }
        }
        val groupTravelService = object : Runnable {
            override fun run() {
                if(appointmentId != null) {
                    onGetAllParticipantsLocation(appointmentId)
                    handler.postDelayed(this, 5000)
                }
            }
        }

        // Lần đầu chạy sau 1s
        handler.postDelayed(runnable, 2000)  //the time is in miliseconds
        handler.postDelayed(quickNavigateAppointment, 3000)
        handler.postDelayed(groupTravelService, 2000)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.System.canWrite(this)) {
            // Khởi tạo sound và vibrate
            initSoundVibrate()
        } else {
            // Migrate to Setting write permission screen
            val intent: Intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)
            intent.data = Uri.parse("package:" + this.packageName)
            startActivity(intent)
            TastyToast.makeText(this, "Cho phép chỉnh sửa cài đặt hệ thống", TastyToast.LENGTH_SHORT, TastyToast.DEFAULT)
        }
    }

    private fun initActionBarDrawerToggle() {
        mActionBarDrawerToggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(mActionBarDrawerToggle)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        mActionBarDrawerToggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mActionBarDrawerToggle.onConfigurationChanged(newConfig)
    }

    @SuppressLint("MissingPermission")
    override fun onClick(v: View) {
        v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
        when (v.id) {
            R.id.imvMyLoc -> {
                // Chạy audio
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.vi_tri_hien_tai)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.vi_tri_hien_tai_2)
                        }
                    }
                }
                onMyLocationButtonClicked()
            }
            R.id.imvReport -> {
                val intent = Intent(this, ReportMenuActivity::class.java)
                startActivity(intent)
            }
            R.id.imvBroadcast -> {
                // Chạy audio
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.giao_tiep_voi_tai_xe_gan_day)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.giao_tiep_voi_tai_xe_gan_day_2)
                        }
                    }
                }
                if (mPopupWindowBroadcast != null) {
                    if (mPopupWindowBroadcast!!.isShowing) {
                        mPopupWindowBroadcast!!.dismiss()
                    } else {
                        onBroadcastButtonClicked()
                    }
                } else {
                    onBroadcastButtonClicked()
                }
            }
            R.id.imvQuickNav->{
                animateQnab()
            }
            R.id.imvQuickNavPetrol->{
                animateQnab()
                getNearbyPlaces("gas_station", lastLocation, 1500, QUICK_NAV)
            }
            R.id.imvQuickNavParking->{
                animateQnab()
                getNearbyPlaces("parking", lastLocation, 1500, QUICK_NAV)
            }
            R.id.btnLogOut -> {
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.dang_xuat)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.dang_xuat_2)
                        }
                    }
                }
                onSignOut()
            }
            R.id.layout_setting -> {
                val intent = Intent(this, SettingActivity::class.java)
                startActivityForResult(intent, 3)
            }
            R.id.layout_group -> {
                val intent = Intent(this, GroupAppointmentActivity::class.java)
                startActivity(intent)
            }
            R.id.btn_update_user_avatar -> {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1080, 1080)
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(this)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                if(resultCode == Activity.RESULT_OK) {
                    val result = CropImage.getActivityResult(data)
                    val stream = FileInputStream(File(result.uri.path!!))
                    Picasso.get().load(result.uri).into(circleImageView_user_avatar)
                    val storageReference = FirebaseStorage.getInstance().reference
                            .child("images")
                            .child("User")
                            .child("UserAvatar")
                            .child(AppController.userProfile!!._id!!)
                    val uploadTask = storageReference.putStream(stream)
                    uploadTask.continueWithTask { task ->
                        if (!task.isSuccessful) {
                            TastyToast.makeText(this, "Failed", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                        }
                        storageReference.downloadUrl
                    }.addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            onUpdateUserAvatar(task.result.toString().substring(0, task.result.toString().indexOf("&token")))
                        }
                    }
                } else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    TastyToast.makeText(this, "Lỗi: Đã có sự cố khi cắt ảnh", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }
            3 -> {
                if (resultCode == Activity.RESULT_OK) {
                    // Set lần đầu cho setting âm thanh
                    when (AppController.soundMode) {
//                        1 -> {
//                            imQuickSettingSound.setImageResource(R.drawable.ic_sound_on)
//                            tvQuickSettingSound.text = "MỞ"
//                        }
//                        2 -> {
//                            imQuickSettingSound.setImageResource(R.drawable.ic_sound_alerts)
//                            tvQuickSettingSound.text = "CHỈ CÁC BÁO HIỆU"
//                        }
//                        3 -> {
//                            imQuickSettingSound.setImageResource(R.drawable.ic_sound_mute)
//                            tvQuickSettingSound.text = "TẮT"
//                        }
                    }

                    if (AppController.settingSocket == "true" && AppController.settingSocket != currentSocketSetting) {
                        initSocket()
                        currentSocketSetting = AppController.settingSocket
                    }

                    if (AppController.settingSocket == "false" && AppController.settingSocket != currentSocketSetting) {
                        destroySocket()
                        currentSocketSetting = AppController.settingSocket
                    }

                    if (AppController.settingInvisible == "false") {
                        val listGeo: List<Double> = listOf(0.0, 0.0)
                        val newGeo = Geometry("Point", listGeo)
                        val user = User("", "", "", "", "", "", newGeo, 0.0, 0.0, 0.0, 0.0, false, "", emptyList(), emptyList(), emptyList(), "")
                        onUpdateStatus(user)
                    }

                    if (AppController.settingInvisible == "true") {
                        val listGeo: List<Double> = listOf(0.0, 0.0)
                        val newGeo = Geometry("Point", listGeo)
                        val user = User("", "", "", "", "", "", newGeo, 0.0, 0.0, 0.0, 0.0, true, "", emptyList(), emptyList(), emptyList(), "")
                        onUpdateStatus(user)
                    }
                }
            }
            PICK_PLACE_HISTORY_REQUEST -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val place = SimplePlace(data.getStringExtra("PLACE_NAME"),
                            LatLng(data.getDoubleExtra("PLACE_LAT", 0.0), data.getDoubleExtra("PLACE_LONG", 0.0)))
                    currentDirectionRoute.clear()
                    currentDirectionRoute.add(SimplePlace("Vị trí của bạn", LatLng(lastLocation.latitude, lastLocation.longitude)))
                    currentDirectionRoute.add(SimplePlace(place.name, LatLng(place.location!!.latitude, place.location!!.longitude)))
                    drawer_layout.closeDrawer(GravityCompat.START)
                    onBtnStartDirectionClick(currentDirectionRoute)
                }
            }
        }

    }


    // ================================================================================================================================================== //
    // ======== VỀ PERMISSION LOCATION VÀ MAP =========================================================================================================== //
    // ================================================================================================================================================== //

    // Permission Requirement functions
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        Log.d(TAG, "requestCode: $requestCode")

        when (requestCode) {
            MY_LOCATION_PERMISSION_REQUEST_CODE -> mLocationPermission.onRequestPermissionsResult(grantResults)
        }
    }

    @SuppressLint("MissingPermission")
    private fun initLocationPermission() {
        mLocationPermission = Permission(this, android.Manifest.permission.ACCESS_FINE_LOCATION,
                object : Permission.PermissionListener {
                    override fun onPermissionGranted() {
                        if (mLocationPermission.checkPermissions()) {
                            mMap.isMyLocationEnabled = true
                            mMap.uiSettings.isMyLocationButtonEnabled = false
                        }
                        if (::fusedLocationClient.isInitialized) {
                            fusedLocationClient.lastLocation
                                    .addOnSuccessListener { location: Location? ->
                                        // Got last known location. In some rare situations this can be null.
                                        location ?: return@addOnSuccessListener
                                        lastLocation = location

                                        // Dùng khi chưa có grant permission - chạy lần đầu
                                        if (::lastLocation.isInitialized) {
                                            val currentLatLng = LatLng(location.latitude, location.longitude)
                                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 17f))
                                            val listGeo: List<Double> = listOf(lastLocation.longitude, lastLocation.latitude)
                                            val newGeo = Geometry("Point", listGeo)
                                            AppController.userProfile?.currentLocation = newGeo
                                        }
                                    }
                        }
                    }

                    override fun onShouldProvideRationale() {

                    }

                    override fun onRequestPermission() {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(
                                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                                    MY_LOCATION_PERMISSION_REQUEST_CODE)
                        }
                    }

                    override fun onPermissionDenied() {

                    }

                })
    }

    // Google Map functions
    private fun obtainMapFragment() {
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        //Set Custom InfoWindow Adapter
        val adapter = CustomInfoWindowAdapter(this)
        mMap.setInfoWindowAdapter(adapter)

        mMap.setOnMarkerClickListener(this)
        mMap.setOnInfoWindowClickListener(this)
        mMap.setOnInfoWindowCloseListener(this)
        mMap.setOnPolylineClickListener(this)
        if(AppController.settingTrafficLayer == "true") {
            mMap.isTrafficEnabled = true
        }

        initLocationPermission()

        mLocationPermission.execute()

        initLocation()

        // Set myLocationButton visible
        imvMyLoc.visibility = View.VISIBLE
        mMap.setOnMapClickListener {
            if(isPlaceInfoWindowUp) {
                dismissPopupWindowPlaceInfo()
            }
            mPopupWindowUser?.dismiss()
            if(isOpen) {
                animateQnab()
            }
        }
    }

    // Location functions
    @SuppressLint("MissingPermission")
    private fun initLocation() {

        // Create location services client
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if (mLocationPermission.checkPermissions()) {
            fusedLocationClient.lastLocation
                    .addOnSuccessListener { location: Location? ->
                        // Got last known location. In some rare situations this can be null.
                        location ?: return@addOnSuccessListener
                        lastLocation = location
                        // Khi đã có permission rồi, chạy trước locationCallback

                        if (::lastLocation.isInitialized) {
                            val currentLatLng = LatLng(location.latitude, location.longitude)
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 17f))
                            val listGeo: List<Double> = listOf(lastLocation.longitude, lastLocation.latitude)
                            val newGeo = Geometry("Point", listGeo)
                            AppController.userProfile?.currentLocation = newGeo
                        }
                    }
        }

        // Define the location update callback
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    Log.d("TestCam", "Location null")
                    return
                }

                for (location in locationResult.locations) {
                    lastLocation = location

                    val listGeo: List<Double> = listOf(lastLocation.longitude, lastLocation.latitude)
                    val newGeo = Geometry("Point", listGeo)
                    AppController.userProfile?.currentLocation = newGeo
                }

                // Update Navigation UI
                if (mPopupWindowNavigationInfo != null && mPopupWindowNavigationInfo!!.isShowing) {
                    Log.d("TestCam", "mPopupWindowNavigationInfo success")
                    val currentRoute = currentPolyline.tag as Route
                    fusedLocationClient.lastLocation.addOnSuccessListener {
                        if (it != null)
                            lastLocation = it
                    }
                    if (::nextReportMarkerPopupOnNavigation.isInitialized)
                        Log.d("UpdateListReport", "nextReportMarkerPopupOnNavigation inited")
                    else
                        Log.d("UpdateListReport", "nextReportMarkerPopupOnNavigation not inited")
                    updateUINavigation(currentRoute)
                    updateListReportMarkerCurrentRoute()
                    listReportMarkerCurrentRoute.sortedWith(compareBy { (it.tag as Report).distance })

                    Log.d("UpdateListReport", listReportMarkerCurrentRoute.size.toString())
                    for (i in 0 until listReportMarkerCurrentRoute.size) {
                        val location = Location("tempLocation")
                        location.latitude = listReportMarkerCurrentRoute[i].position.latitude
                        location.longitude = listReportMarkerCurrentRoute[i].position.longitude
                        Log.d("UpdateListReport", lastLocation.distanceTo(location).toString() + " m")
                        if (lastLocation.distanceTo(location) <= REPORT_ON_ROUTE_DISTANCE_NAVIGATION) {
                            Log.d("UpdateListReport", "distance success")
                            if (currentReportMarkerPopupOnNavigation == null
                                    || listReportMarkerCurrentRoute[i] != currentReportMarkerPopupOnNavigation) {
                                currentReportMarkerPopupOnNavigation = listReportMarkerCurrentRoute[i]
                                listReportMarkerPassedCurrentRoute.add(currentReportMarkerPopupOnNavigation!!)
                                onOpenReportMarker(currentReportMarkerPopupOnNavigation!!)
                                if (i + 1 < listReportMarkerCurrentRoute.size && !listReportMarkerPassedCurrentRoute.contains(listReportMarkerCurrentRoute[i + 1])) {
                                    nextReportMarkerPopupOnNavigation = listReportMarkerCurrentRoute[i + 1]
                                    nextReportPopupOnNavigation = listReportMarkerCurrentRoute[i + 1].tag as Report
                                    Log.d("UpdateListReport", "nextReportMarkerPopupOnNavigation=listReportMarkerCurrentRoute[i+1]")
                                } else {
                                    Log.d("UpdateListReport", "ListReport reached max")
                                }
                            } else {
                                Log.d("UpdateListReport", "currentReportMarkerPopupOnNavigation fail")
                            }
                            break
                        } else {
                            Log.d("UpdateListReport", "distance fail")
                            if (currentReportMarkerPopupOnNavigation != null) {
                                if (currentReportMarkerPopupOnNavigation == listReportMarkerCurrentRoute[i]) {
                                    mPopupWindowReport?.dismiss()
                                }
                            }
                        }
                    }
                } else {
                    Log.d("TestCam", "mPopupWindowNavigationInfo fail")
                }
            }
        }

        createLocationRequest()
    }

    private lateinit var currentRoute: Route
    private lateinit var nextReportMarkerPopupOnNavigation: Marker
    private lateinit var nextReportPopupOnNavigation: Report
    private var listReportMarkerPassedCurrentRoute: MutableList<Marker> = ArrayList()

    private fun updateListReportMarkerCurrentRoute() {
        listReportMarkerCurrentRoute = ArrayList()
        for (i in 0 until listReportMarker.size) {
            if (PolyUtil.isLocationOnPath(LatLng(listReportMarker[i].position.latitude, listReportMarker[i].position.longitude), currentRoute.points, true, REPORT_ON_ROUTE_DISTANCE_DIRECTION)) {
                listReportMarkerCurrentRoute.add(listReportMarker[i])
                listReportMarkerCurrentRoute[listReportMarkerCurrentRoute.size - 1].tag = listReportMarker[i].tag as Report
            }
        }
    }

    private lateinit var locationTask: Task<LocationSettingsResponse>

    @SuppressLint("RestrictedApi")
    private fun createLocationRequest() {
        locationRequest = LocationRequest().apply {
            interval = 10000
            // change to 2000
            fastestInterval = 3000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
        val client = LocationServices.getSettingsClient(this)

        locationTask = client.checkLocationSettings(builder.build())

        locationTask.addOnSuccessListener {
            startLocationUpdates()
        }

        locationTask.addOnFailureListener { e ->
            // 6
            if (e is ResolvableApiException) {
                val lm: LocationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                var needToEnableLocation = false
                try {
                    val isGpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
                    val isNetworkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                    if (!isGpsEnabled && !isNetworkEnabled) {
                        needToEnableLocation = true
                    }
                } catch (ex: Exception) {
                }


                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    if (needToEnableLocation) {
                        e.startResolutionForResult(this@MainActivity,
                                REQUEST_CHECK_SETTINGS)
                    }
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }

    }

    private fun startLocationUpdates() {
        @SuppressLint("MissingPermission")
        if (mLocationPermission.checkPermissions()) {
            fusedLocationClient.requestLocationUpdates(locationRequest,
                    locationCallback,
                    null /* Looper */)
        }
    }

    // ================================================================================================================================================== //
    // ======== VỀ CÁC NÚT TRÊN APP BAR MAIN ============================================================================================================ //
    // ================================================================================================================================================== //


    @SuppressLint("MissingPermission")
    private fun onMyLocationButtonClicked() {
        mLocationPermission.execute()
        if (::locationTask.isInitialized) {
            locationTask.addOnFailureListener { e ->
                // 6
                if (e is ResolvableApiException) {
                    val lm: LocationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                    var needToEnableLocation = false
                    try {
                        val isGpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
                        val isNetworkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                        if (!isGpsEnabled && !isNetworkEnabled) {
                            needToEnableLocation = true
                        }
                    } catch (ex: Exception) {
                    }
                    try {
                        if (needToEnableLocation) {
                            e.startResolutionForResult(this@MainActivity,
                                    REQUEST_CHECK_SETTINGS)
                        }
                    } catch (sendEx: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }
                }
            }
        }
        if (::fusedLocationClient.isInitialized) {
            fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
                if (location != null) lastLocation = location
            }
        }
        if (::lastLocation.isInitialized) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lastLocation.latitude, lastLocation.longitude), 17f))
        } else {

        }
    }

    @SuppressLint("InflateParams")
    private fun onBroadcastButtonClicked() {
        val listUserExceptMe: MutableList<User> = ArrayList()
        if (::listUser.isInitialized) {
            for (i in 0 until listUser.size) {
                if (listUser[i].email != AppController.userProfile!!.email) {
                    listUserExceptMe.add(listUser[i])
                }
            }
        }
        if (listUserExceptMe.isEmpty()) {
            TastyToast.makeText(this, "Không có tài xế nào gần bạn", TastyToast.LENGTH_SHORT, TastyToast.INFO).show()
        }
//        if (listUserExceptMe.isEmpty()) {
//            TastyToast.makeText(this, "Không có tài xế nào gần bạn", TastyToast.LENGTH_SHORT, TastyToast.INFO).show()
//        } else {
//            val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//            val viewChatPopup = inflater.inflate(R.layout.list_broadcast_chat_dialog_layout, null)
//
//            // Layout mới
//            Log.d("viewChatPopup", viewChatPopup.toString())
//            mPopupWindowChat = PopupWindow(viewChatPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
//            val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
//            mPopupWindowChat!!.showAtLocation(parent, Gravity.CENTER, 0, 0)
//            val btnSendAll = viewChatPopup.findViewById<Button>(R.id.btnSendAll_list_nearby_user_chat_dialog)
//            val layoutOutside = viewChatPopup.findViewById<LinearLayout>(R.id.bg_to_remove_list_nearby_user_chat_dialog)
//
//            if(AppController.settingFilterCar == "true") {
//                initListNearbyUserRecyclerView(listUserExceptMe, viewChatPopup)
//            }
//
//            layoutOutside.setOnClickListener {
//                it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
//                mPopupWindowChat!!.dismiss()
//            }
//            btnSendAll.setOnClickListener {
//                it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
//                mPopupWindowChat!!.dismiss()
//                onOpenChatAll(listUserExceptMe)
//            }
//        }
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val viewBroadcastPopup = inflater.inflate(R.layout.list_broadcast_chat_dialog_layout, null)

        // Layout mới
        Log.d("viewChatPopup", viewBroadcastPopup.toString())
        mPopupWindowBroadcast = PopupWindow(viewBroadcastPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
        mPopupWindowBroadcast!!.showAtLocation(parent, Gravity.CENTER, 0, 0)
        val layoutOutside = viewBroadcastPopup.findViewById<LinearLayout>(R.id.bg_to_remove_list_nearby_user_chat_dialog)

        layoutOutside.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            mPopupWindowBroadcast!!.dismiss()
        }

        val listBroadcastChat = ArrayList<TypeBroadcastChat>()
        listBroadcastChat.add(TypeBroadcastChat(R.drawable.ic_hello, "Chào hỏi"))
        listBroadcastChat.add(TypeBroadcastChat(R.drawable.ic_headlights_on, "Hạ đèn pha"))
        listBroadcastChat.add(TypeBroadcastChat(R.drawable.ic_report_turn_around, "Quay đầu xe"))

        val viewAdapterNearbyUser = BroadcastAdapter(listBroadcastChat) {
            run {
                when(it.imageBroadcastChat) {
                    R.drawable.ic_hello -> {
                        for (element in listUserExceptMe) {
                            attemptHello(AppController.userProfile?.email.toString(), AppController.userProfile?.name.toString(), element.socketID.toString())
                        }
                    }
                    R.drawable.ic_headlights_on -> {
                        for (element in listUserExceptMe) {
                            attemptWarnStrongLight(AppController.userProfile?.email.toString(), AppController.userProfile?.name.toString(), element.socketID.toString())
                        }
                    }
                    R.drawable.ic_report_turn_around -> {
                        for (element in listUserExceptMe) {
                            attemptWarnTurnAround(AppController.userProfile?.email.toString(), AppController.userProfile?.name.toString(), element.socketID.toString())
                        }
                    }
                }
            }
        }

        val viewManagerEditDirection = androidx.recyclerview.widget.LinearLayoutManager(this)
        viewBroadcastPopup.findViewById<RecyclerView>(R.id.recycler_view_list_broadcast_chat_dialog).apply {
            layoutManager = viewManagerEditDirection
            adapter = viewAdapterNearbyUser
        }
    }



    override fun onBackPressed() {
        when {
            drawer_layout.isDrawerOpen(GravityCompat.START) -> {
                drawer_layout.closeDrawer(GravityCompat.START)
                return
            }
            isAddPlaceWindowUp -> {
                dismissAddPlacePopup()
                return
            }
            currentStepsLayout != null -> {
                currentStepsLayout!!.visibility = View.GONE
                currentStepsLayout = null
                if (listReportMarkerCurrentRoute.size > 0) {
                    val layoutReport = viewRoutePopup.findViewById<LinearLayout>(R.id.layoutReport_detail)
                    layoutReport.visibility = View.VISIBLE
                }
                return
            }
            isEditDirectionWindowUp -> {
                onFinishEditDirection()
                return
            }
            isNavigationInfoWindowUp -> {
                dismissPopupWindowNavigationInfo()

                // Đóng popup windows
                mPopupWindowReport?.dismiss()
                curMarkerReport = null

                // Update Camera
                val camPos = CameraPosition.builder()
                        .target(LatLng(lastLocation.latitude, lastLocation.longitude))
                        .zoom(16f)
                        .tilt(0f)
                        .build()
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPos))
                mMap.uiSettings.setAllGesturesEnabled(true)

                return
            }
            ::polylinePaths.isInitialized && polylinePaths.isNotEmpty() && isRouteInfoWindowUp && isDirectionInfoWindowUp -> {
                removeCurrentDirectionPolyline()
                dismissPopupWindowRouteInfo()
                dismissPopupWindowDirectionInfo()
                return
            }
            isPlaceInfoWindowUp -> {
                dismissPopupWindowPlaceInfo()
                return
            }
            currentSelectedPlace != null -> {
                removeCurrentSelectedPlace()
                return
            }
            mPopupWindowUser != null && mPopupWindowUser!!.isShowing -> {
                mPopupWindowUser?.dismiss()
                return
            }
            nearbyPlacesResultMarkers.size > 0 -> {
                if (nearbyPlacesResultMarkers.size > 0) {
                    for (i in 0 until nearbyPlacesResultMarkers.size) {
                        nearbyPlacesResultMarkers[i].remove()
                    }
                    nearbyPlacesResultMarkers.clear()
                }
            }
            else -> {
                mPopupWindowDirectionInfo = null
                dismissPopupWindowNavigationInfo()
                val builder = androidx.appcompat.app.AlertDialog.Builder(this)
                builder.setMessage("Bạn có muốn thoát khỏi ứng dụng?")
                        .setCancelable(false)
                        .setPositiveButton("Có") { _, _ -> /*finish()*/super.onBackPressed() }
                        .setNegativeButton("Không") { dialog, _ -> dialog.cancel() }
                val alert = builder.create()
                alert.show()
            }
        }
    }

    private fun dismissPopupWindowPlaceInfo() {
        mPopupWindowPlaceInfo?.dismiss()
        isPlaceInfoWindowUp = false
        imvReport.visibility = View.VISIBLE
    }

    private fun removeCurrentSelectedPlace() {
        currentSelectedPlaceMarker?.remove()
        // outdated locationAPI
//        placeAutoComplete.setText(null)

        currentSelectedPlace = null
    }

    private var currentSelectedPlaceMarker: Marker? = null
    fun addMarker(p: Place) {
        val markerOptions = MarkerOptions()
        markerOptions.position(p.latLng!!)
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))

        currentSelectedPlaceMarker = mMap.addMarker(markerOptions)
        currentSelectedPlaceMarker?.title = "current_place"
    }

    private fun onUpdateUserAvatar(imageUri: String) {
        val userAvatar = User(imageUri)
        val service = APIServiceGenerator.createService(UserService::class.java)
        val call = service.updateAvatar(userAvatar)

        call.enqueue(object : Callback<SuccessResponse> {
            override fun onResponse(call: Call<SuccessResponse>, response: Response<SuccessResponse>) {
                if(response.isSuccessful) {
                    TastyToast.makeText(this@MainActivity, "Cập nhật ảnh nhóm thành công", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@MainActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<SuccessResponse>, t: Throwable) {
                TastyToast.makeText(this@MainActivity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }
        })
    }

    // ================================================================================================================================================= //
    // ======== VỀ THÔNG TIN NGƯỜI DÙNG USER =========================================================================================================== //
    // ================================================================================================================================================= //
    private fun loadUserProfile() {
        if (AppController.accessToken != null && AppController.accessToken.toString().isNotEmpty()) {
            val service = APIServiceGenerator.createService(UserService::class.java)
            val call = service.userProfile
            call.enqueue(object : Callback<UserProfileResponse> {
                override fun onResponse(call: Call<UserProfileResponse>, response: Response<UserProfileResponse>) {
                    if (response.isSuccessful) {
                        AppController.userProfile = response.body()!!.user
                        updateInformation()
                    } else {
                        val apiError = ErrorUtils.parseError(response)
                        TastyToast.makeText(this@MainActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                    }
                }

                override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                    Log.d("DEBUG", "LỖI GÌ RỒI")
                }
            })
        }
        //TastyToast.makeText(this@MainActivity, AppController.userProfile!!.latHomeLocation.toString() + " sdfgsdfg " + AppController.userProfile!!.longHomeLocation.toString(),  TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
    }

    private fun updateInformation() {
        val user = AppController.userProfile
        tvName.text = user?.name
        tvEmail.text = user?.email
        if((user != null) and (user?.avatar != "")) {
            Picasso.get().load(user?.avatar).into(circleImageView_user_avatar)
        }

        if (user!!.latHomeLocation != null && user.longHomeLocation != null) {
            try {
//                // Lấy địa chỉ nhà sử dụng Geocoder
//                val geocoder = Geocoder(this, Locale.getDefault())
//                val yourAddresses: List<Address>
//                yourAddresses = geocoder.getFromLocation(user.latHomeLocation!!, user.longHomeLocation!!, 10)
//
//                if (yourAddresses.isNotEmpty()) {
//                    var locality = " "
//                    for (address in yourAddresses) {
//                        if (address.locality != null) {
//                            locality = address.locality
//                            break;
//                        }
//                    }
//
//                    val address = yourAddresses[0].thoroughfare + ", " + locality + ", " + yourAddresses[0].subAdminArea + ", " + yourAddresses[0].adminArea + ", " + yourAddresses[0].countryName
//                    tvAddressHome_menu.text = address
//                }
            } catch (ex: Exception) {
                TastyToast.makeText(this, "Kết nối mạng yếu", TastyToast.LENGTH_SHORT, TastyToast.CONFUSING).show()
            }
        }

        if (user.latWorkLocation != null && user.longWorkLocation != null) {
            try {
//                // Lấy địa chỉ nhà sử dụng Geocoder
//                val geocoder = Geocoder(this, Locale.getDefault())
//                val yourAddresses: List<Address>
//                yourAddresses = geocoder.getFromLocation(user.latWorkLocation!!, user.longWorkLocation!!, 10)
//
//                if (yourAddresses.isNotEmpty()) {
//                    var locality = " "
//                    for (address in yourAddresses) {
//                        if (address.locality != null) {
//                            locality = address.locality
//                            break;
//                        }
//                    }
//
//                    val address = yourAddresses[0].thoroughfare + ", " + locality + ", " + yourAddresses[0].subAdminArea + ", " + yourAddresses.get(0).adminArea + ", " + yourAddresses.get(0).countryName
//                    tvAddressWork_menu.text = address
//                }
            } catch (ex: Exception) {
                TastyToast.makeText(this, "Kết nối mạng yếu", TastyToast.LENGTH_SHORT, TastyToast.CONFUSING).show()
            }
        }
    }

    private fun onSignOut() {
        destroySocket()
        when(AppController.userProfile!!.authProvider) {
            "google" -> {
                val mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(getString(R.string.default_web_client_id))
                        .requestEmail()
                        .build()
                val mGoogleSignInClient = GoogleSignIn.getClient(this, mGoogleSignInOptions)
                mGoogleSignInClient.revokeAccess().addOnCompleteListener(this, OnCompleteListener<Void> {
                    AppController.signOut()
                    TastyToast.makeText(this, "Đăng xuất thành công!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                    val intent = Intent(this, LoggedOutActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    finish()
                    startActivity(intent)
                })
            }
            "SMUser" -> {
                AppController.signOut()
                TastyToast.makeText(this, "Đăng xuất thành công!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                val intent = Intent(this, LoggedOutActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                finish()
                startActivity(intent)
            }
        }
    }

    // ================================================================================================================================================= //
    // ======== VỂ CLICK MARKER ======================================================================================================================== //
    // ================================================================================================================================================= //
    override fun onMarkerClick(p0: Marker): Boolean {

        if(p0.title == "appointment_place") {
            if (!isPlaceInfoWindowUp && currentSelectedPlace != null) {
                showPlaceInfoPopup(currentSelectedPlace!!)
                return false
            }
        }

        if (p0.title == "current_place") {
            if (!isPlaceInfoWindowUp && currentSelectedPlace != null) {
                showPlaceInfoPopup(currentSelectedPlace!!)
                return false
            }
        }

        if (p0.title == "nearby_places_result") {
            val place = p0.tag as SimplePlace
            currentDirectionRoute.add(currentDirectionRoute.size - 1, place)
            viewAdapterEditDirection.notifyDataSetChanged()
            return false
        }

        if(p0.title == "nearby_places_result_quick_navigate") {
            dismissPopupWindowPlaceInfo()
            isPlaceInfoWindowUp = false
            val place = p0.tag as Place
            if (!isPlaceInfoWindowUp) {
                showPlaceInfoPopup(place)
                return false
            }
        }

        onOpenReportMarker(p0)
        return false
    }

    private fun updateDistanceReportMarkerPopup(report: Report) {
        if (mPopupWindowReport == null) {
            return
        }
        if (!mPopupWindowReport!!.isShowing || !::viewReportPopup.isInitialized) {
            return
        }
    }

    private lateinit var viewReportPopup: View

    @SuppressLint("InflateParams", "SetTextI18n")
    private fun onOpenReportMarker(marker: Marker) {
        if (marker.title == "report") {
            val dataReport: Report = marker.tag as Report
            if (dataReport.type == "help" || dataReport.type == "other") {
                if (dataReport.type == "help") {
                    val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    viewReportPopup = inflater.inflate(R.layout.marker_report_layout_help, null)
                    mPopupWindowReport = PopupWindow(viewReportPopup, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                    mPopupWindowReport!!.showAtLocation(parent, Gravity.TOP, 0, 0)

                    // Phải có con trỏ vào customViewPopup, nếu không sẽ null
                    val tvType = viewReportPopup.findViewById<TextView>(R.id.tvType_marker_report_help)
                    val tvDistance = viewReportPopup.findViewById<TextView>(R.id.tvDistance_marker_report_help)
                    val tvLocation = viewReportPopup.findViewById<TextView>(R.id.tvLocation_marker_report_help)
                    val tvDescription = viewReportPopup.findViewById<TextView>(R.id.tvDescription_marker_report_help)
                    val imvType = viewReportPopup.findViewById<ImageView>(R.id.imvType_marker_report_help)
                    val imvUpVote = viewReportPopup.findViewById<LinearLayout>(R.id.imvUpVote_marker_report_help)
                    val imvDownVote = viewReportPopup.findViewById<ImageView>(R.id.imvDownVote_marker_report_help)
                    val imvRecord = viewReportPopup.findViewById<ImageView>(R.id.imRecord_marker_report_help)
                    val imvImage = viewReportPopup.findViewById<ImageView>(R.id.imImage_marker_report_help)
                    val imvCall = viewReportPopup.findViewById<ImageView>(R.id.imCall_marker_report_help)
                    val tvNumReport = viewReportPopup.findViewById<TextView>(R.id.tvNumReport_marker_report_help)

                    // Làm tròn số double
                    val decimalFormat = DecimalFormat("#")
                    decimalFormat.roundingMode = RoundingMode.CEILING

                    tvDistance.text = "Cách " + decimalFormat.format(dataReport.distance) + " m"

                    // Lấy địa chỉ sử dụng Geocoder
                    try {
                        val geocoder = Geocoder(this, Locale.getDefault())
                        val yourAddresses: List<Address>
                        yourAddresses = geocoder.getFromLocation(dataReport.geometry!!.coordinates!![1], dataReport.geometry!!.coordinates!![0], 1)

                        if (yourAddresses.isNotEmpty()) {
                            val address = yourAddresses.get(0).thoroughfare + ", " + yourAddresses.get(0).locality + ", " + yourAddresses.get(0).subAdminArea
                            tvLocation.text = address
                        }

                    } catch (ex: Exception) {
                    }


                    tvDescription.text = dataReport.description.toString()
                    when (dataReport.type) {
                        "help" -> {
                            imvType.setImageResource(R.drawable.imv_report_assistance_activity_report)
                            tvType.text = "Cứu hộ"
                        }
                    }

                    curMarkerReport = marker

                    // Số report
                    tvNumReport.text = dataReport.numReport.toString()

                    imvUpVote.setOnClickListener {
                        viewReportPopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                        onUpdateNumReport(dataReport._id.toString())
                        tvNumReport.text = (dataReport.numReport!!.toInt() + 1).toString()
                    }

                    // Nếu là người sở hữu, sửa nút DownVote thành nút Xoá
                    var isDelete = false
                    if (AppController.userProfile!!._id == dataReport.userID) {
                        imvDownVote.setImageResource(R.drawable.ic_delete_white)
                        isDelete = true
                    }
                    imvDownVote.setOnClickListener {
                        viewReportPopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                        if (isDelete) {
//                    Toast.makeText(this, "Remove Report", Toast.LENGTH_SHORT).show()

                            // Chạy audio
                            if (AppController.soundMode == 1) {
                                when (AppController.voiceType) {
                                    1 -> {
                                        mAudioPlayer.play(this, R.raw.xoa_bao_hieu)
                                    }
                                    2 -> {
                                        mAudioPlayer.play(this, R.raw.xoa_bao_hieu_2)
                                    }
                                }
                            }

                            mPopupWindowReport!!.dismiss()
                            curMarkerReport = null

                            val inflater2 = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                            val viewDeletePopup = inflater2.inflate(R.layout.confirm_delete_report_dialog_layout, null)
                            mPopupWindowDelete = PopupWindow(viewDeletePopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                            val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                            mPopupWindowDelete!!.showAtLocation(parent, Gravity.CENTER, 0, 0)
                            val btnYes = viewDeletePopup.findViewById<Button>(R.id.btnYes_confirm_delete_report)
                            val btnNo = viewDeletePopup.findViewById<Button>(R.id.btnNo_confirm_delete_report)
                            val layoutOutside = viewDeletePopup.findViewById<LinearLayout>(R.id.bg_to_remove_confirm_delete_report)
                            btnYes.setOnClickListener {
                                viewDeletePopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                                mPopupWindowDelete!!.dismiss()
                                onDeleteReport(dataReport._id.toString())
                            }

                            btnNo.setOnClickListener {
                                viewDeletePopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                                mPopupWindowDelete!!.dismiss()
                            }

                            layoutOutside.setOnClickListener {
                                viewDeletePopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                                mPopupWindowDelete!!.dismiss()
                            }
                        } else {
//                    Toast.makeText(this, "Down Vote", Toast.LENGTH_SHORT).show()
                            onUpdateNumDelete(dataReport._id.toString())
                            mPopupWindowReport!!.dismiss()
                            curMarkerReport = null
                        }
                    }
                    imvRecord.setOnClickListener {
                        openVoiceOnReport(dataReport._id!!)
                    }
                    imvImage.setOnClickListener {
                        openImageOnReport(dataReport._id!!)
                    }
                    imvCall.setOnClickListener {
                        if (dataReport.phoneNumber == AppController.userProfile!!.phoneNumber) {
                            TastyToast.makeText(this, "Không thể gọi vì đây là báo hiệu của bạn", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
                        } else {
                            val callURI = "tel:" + dataReport.phoneNumber
                            val intent = Intent(Intent.ACTION_DIAL)
                            intent.data = Uri.parse(callURI)
                            startActivity(intent)
                        }
                    }
                } else {
                    val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    val viewReportPopup = inflater.inflate(R.layout.marker_report_layout_other, null)
                    mPopupWindowReport = PopupWindow(viewReportPopup, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                    mPopupWindowReport!!.showAtLocation(parent, Gravity.TOP, 0, 0)

                    // Phải có con trỏ vào customViewPopup, nếu không sẽ null
                    val tvType = viewReportPopup.findViewById<TextView>(R.id.tvType_marker_report_other)
                    val tvDistance = viewReportPopup.findViewById<TextView>(R.id.tvDistance_marker_report_other)
                    val tvLocation = viewReportPopup.findViewById<TextView>(R.id.tvLocation_marker_report_other)
                    val tvDescription = viewReportPopup.findViewById<TextView>(R.id.tvDescription_marker_report_other)
                    val imvType = viewReportPopup.findViewById<ImageView>(R.id.imvType_marker_report_other)
                    val imvUpVote = viewReportPopup.findViewById<LinearLayout>(R.id.imvUpVote_marker_report_other)
                    val imvDownVote = viewReportPopup.findViewById<ImageView>(R.id.imvDownVote_marker_report_other)
                    val imvImage = viewReportPopup.findViewById<ImageView>(R.id.imImage_marker_report_other)
                    val tvNumReport = viewReportPopup.findViewById<TextView>(R.id.tvNumReport_marker_report_other)

                    // Làm tròn số double
                    val decimalFormat = DecimalFormat("#")
                    decimalFormat.roundingMode = RoundingMode.CEILING

                    tvDistance.text = "Cách " + decimalFormat.format(dataReport.distance) + " m"

                    // Lấy địa chỉ sử dụng Geocoder
                    try {
                        val geocoder = Geocoder(this, Locale.getDefault())
                        val yourAddresses: List<Address>
                        yourAddresses = geocoder.getFromLocation(dataReport.geometry!!.coordinates!![1], dataReport.geometry!!.coordinates!![0], 1)

                        if (yourAddresses.isNotEmpty()) {
                            val address = yourAddresses.get(0).thoroughfare + ", " + yourAddresses.get(0).locality + ", " + yourAddresses.get(0).subAdminArea
                            tvLocation.text = address
                        }

                    } catch (ex: Exception) {
                    }


                    tvDescription.text = "Biển số: " + dataReport.description.toString()
                    when (dataReport.subtype1) {
                        "careless_driver" -> {
                            imvType.setImageResource(R.drawable.imv_report_other_activity_report)
                            tvType.text = "Tài xế chạy ẩu"
                            // Chạy audio
                            if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                                when (AppController.voiceType) {
                                    1 -> {
                                        mAudioPlayer.play(this, R.raw.canh_bao_co_tai_xe_chay_au)
                                    }
                                    2 -> {
                                        mAudioPlayer.play(this, R.raw.canh_bao_co_tai_xe_chay_au_2)
                                    }
                                }
                            }
                        }
                        "piggy" -> {
                            imvType.background = getDrawable(R.drawable.bg_btn_report_traffic)
                            imvType.setImageResource(R.drawable.ic_dangerous_zone)
                            tvType.text = "Nguy hiểm khác"
                            // Chạy audio
                            if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                                when (AppController.voiceType) {
                                    1 -> {
                                        mAudioPlayer.play(this, R.raw.canh_bao_co_nguy_hiem_khac)
                                    }
                                    2 -> {
                                        // CHƯA THAY
                                        mAudioPlayer.play(this, R.raw.canh_bao_co_nguy_hiem_khac_2)
                                    }
                                }
                            }
                        }
                    }

                    curMarkerReport = marker

                    // Số report
                    tvNumReport.text = dataReport.numReport.toString()

                    imvUpVote.setOnClickListener {
                        viewReportPopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                        onUpdateNumReport(dataReport._id.toString())
                        tvNumReport.text = (dataReport.numReport!!.toInt() + 1).toString()
                    }

                    // Nếu là người sở hữu, sửa nút DownVote thành nút Xoá
                    var isDelete = false
                    if (AppController.userProfile!!._id == dataReport.userID) {
                        imvDownVote.setImageResource(R.drawable.ic_delete_white)
                        isDelete = true
                    }
                    imvDownVote.setOnClickListener {
                        viewReportPopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                        if (isDelete) {
//                    Toast.makeText(this, "Remove Report", Toast.LENGTH_SHORT).show()

                            // Chạy audio
                            if (AppController.soundMode == 1) {
                                when (AppController.voiceType) {
                                    1 -> {
                                        mAudioPlayer.play(this, R.raw.xoa_bao_hieu)
                                    }
                                    2 -> {
                                        mAudioPlayer.play(this, R.raw.xoa_bao_hieu_2)
                                    }
                                }
                            }

                            mPopupWindowReport!!.dismiss()
                            curMarkerReport = null

                            val inflater2 = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                            val viewDeletePopup = inflater2.inflate(R.layout.confirm_delete_report_dialog_layout, null)
                            mPopupWindowDelete = PopupWindow(viewDeletePopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                            val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                            mPopupWindowDelete!!.showAtLocation(parent, Gravity.CENTER, 0, 0)
                            val btnYes = viewDeletePopup.findViewById<Button>(R.id.btnYes_confirm_delete_report)
                            val btnNo = viewDeletePopup.findViewById<Button>(R.id.btnNo_confirm_delete_report)
                            val layoutOutside = viewDeletePopup.findViewById<LinearLayout>(R.id.bg_to_remove_confirm_delete_report)
                            btnYes.setOnClickListener {
                                viewDeletePopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                                mPopupWindowDelete!!.dismiss()
                                onDeleteReport(dataReport._id.toString())
                            }

                            btnNo.setOnClickListener {
                                viewDeletePopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                                mPopupWindowDelete!!.dismiss()
                            }

                            layoutOutside.setOnClickListener {
                                viewDeletePopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                                mPopupWindowDelete!!.dismiss()
                            }
                        } else {
//                    Toast.makeText(this, "Down Vote", Toast.LENGTH_SHORT).show()
                            onUpdateNumDelete(dataReport._id.toString())
                            mPopupWindowReport!!.dismiss()
                            curMarkerReport = null
                        }
                    }
                    imvImage.setOnClickListener {
                        openImageOnReport(dataReport._id!!)
                    }
                }
            } else {
                val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val viewReportPopup = inflater.inflate(R.layout.marker_report_layout, null)
                mPopupWindowReport = PopupWindow(viewReportPopup, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                mPopupWindowReport!!.showAtLocation(parent, Gravity.TOP, 0, 0)

                // Phải có con trỏ vào customViewPopup, nếu không sẽ null
                val tvType = viewReportPopup.findViewById<TextView>(R.id.tvType_marker_report)
                val tvDistance = viewReportPopup.findViewById<TextView>(R.id.tvDistance_marker_report)
                val tvLocation = viewReportPopup.findViewById<TextView>(R.id.tvLocation_marker_report)
                val tvDescription = viewReportPopup.findViewById<TextView>(R.id.tvDescription_marker_report)
                val imvType = viewReportPopup.findViewById<ImageView>(R.id.imvType_marker_report)
                val imvUpVote = viewReportPopup.findViewById<LinearLayout>(R.id.imvUpVote_marker_report)
                val imvDownVote = viewReportPopup.findViewById<ImageView>(R.id.imvDownVote_marker_report)
                val imvRecord = viewReportPopup.findViewById<ImageView>(R.id.imRecord_marker_report)
                val imvImage = viewReportPopup.findViewById<ImageView>(R.id.imImage_marker_report)
                val tvNumReport = viewReportPopup.findViewById<TextView>(R.id.tvNumReport_marker_report)

                // Làm tròn số double
                val decimalFormat = DecimalFormat("#")
                decimalFormat.roundingMode = RoundingMode.CEILING

                tvDistance.text = "Cách " + decimalFormat.format(dataReport.distance) + " m"

                // Lấy địa chỉ sử dụng Geocoder
                try {
                    val geocoder = Geocoder(this, Locale.getDefault())
                    val yourAddresses: List<Address>
                    yourAddresses = geocoder.getFromLocation(dataReport.geometry!!.coordinates!![1], dataReport.geometry!!.coordinates!![0], 1)

                    if (yourAddresses.isNotEmpty()) {
                        val address = yourAddresses.get(0).thoroughfare + ", " + yourAddresses.get(0).locality + ", " + yourAddresses.get(0).subAdminArea
                        tvLocation.text = address
                    }

                } catch (ex: Exception) {
                }



                tvDescription.text = dataReport.description.toString()
                when (dataReport.type) {
                    "traffic" -> {
                        imvType.background = getDrawable(R.drawable.imv_report_traffic_activity_report)
                        tvType.text = "Kẹt xe"
                    }
                    "crash" -> {
                        imvType.background = getDrawable(R.drawable.imv_report_crash_activity_report)
                        tvType.text = "Tai nạn"
                    }
                    "hazard" -> {
                        imvType.background = getDrawable(R.drawable.imv_report_hazard_activity_report)
                        tvType.text = "Nguy Hiểm"
                    }
                }

                curMarkerReport = marker

                // Số report
                tvNumReport.text = dataReport.numReport.toString()

                imvUpVote.setOnClickListener {
                    viewReportPopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                    onUpdateNumReport(dataReport._id.toString())
                    tvNumReport.text = (dataReport.numReport!!.toInt() + 1).toString()
                }

                // Nếu là người sở hữu, sửa nút DownVote thành nút Xoá
                var isDelete = false
                if (AppController.userProfile!!._id == dataReport.userID) {
                    imvDownVote.setImageResource(R.drawable.ic_delete_white)
                    isDelete = true
                }
                imvDownVote.setOnClickListener {
                    viewReportPopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                    if (isDelete) {

                        // Chạy audio
                        if (AppController.soundMode == 1) {
                            when (AppController.voiceType) {
                                1 -> {
                                    mAudioPlayer.play(this, R.raw.xoa_bao_hieu)
                                }
                                2 -> {
                                    mAudioPlayer.play(this, R.raw.xoa_bao_hieu_2)
                                }
                            }
                        }

                        mPopupWindowReport!!.dismiss()
                        curMarkerReport = null

                        val inflater2 = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                        val viewDeletePopup = inflater2.inflate(R.layout.confirm_delete_report_dialog_layout, null)
                        mPopupWindowDelete = PopupWindow(viewDeletePopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                        val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                        mPopupWindowDelete!!.showAtLocation(parent, Gravity.CENTER, 0, 0)
                        val btnYes = viewDeletePopup.findViewById<Button>(R.id.btnYes_confirm_delete_report)
                        val btnNo = viewDeletePopup.findViewById<Button>(R.id.btnNo_confirm_delete_report)
                        val layoutOutside = viewDeletePopup.findViewById<LinearLayout>(R.id.bg_to_remove_confirm_delete_report)
                        btnYes.setOnClickListener {
                            viewDeletePopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                            mPopupWindowDelete!!.dismiss()
                            onDeleteReport(dataReport._id.toString())
                        }

                        btnNo.setOnClickListener {
                            viewDeletePopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                            mPopupWindowDelete!!.dismiss()
                        }

                        layoutOutside.setOnClickListener {
                            viewDeletePopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                            mPopupWindowDelete!!.dismiss()
                        }
                    } else {
                        onUpdateNumDelete(dataReport._id.toString())
                        mPopupWindowReport!!.dismiss()
                        curMarkerReport = null
                    }
                }
                imvRecord.setOnClickListener {
                    openVoiceOnReport(dataReport._id!!)
                }
                imvImage.setOnClickListener {
                    openImageOnReport(dataReport._id!!)
                }
            }

        }
        if (marker.title == "user") {
            mPopupWindowUser?.dismiss()

            val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val viewUserPopup = inflater.inflate(R.layout.marker_user_layout, null)
            mPopupWindowUser = PopupWindow(viewUserPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
            mPopupWindowUser!!.showAtLocation(parent, Gravity.BOTTOM, 0, 0)

            val imvAvatar = viewUserPopup.findViewById<ImageView>(R.id.imvAvatar_user)
            val tvName = viewUserPopup.findViewById<TextView>(R.id.tvName_marker_user)
            val btnHello = viewUserPopup.findViewById<Button>(R.id.btnHello_marker_user)

            val btnConfirm = viewUserPopup.findViewById<LinearLayout>(R.id.layoutConfirm_marker_user)
            val imvType = viewUserPopup.findViewById<ImageView>(R.id.imvType_marker_user)
            val tvType = viewUserPopup.findViewById<TextView>(R.id.tvType_marker_user)
            val imvInstruction = viewUserPopup.findViewById<ImageView>(R.id.imInstruction_marker_user)

            val dataUser: User = marker.tag as User
            tvName.text = dataUser.name.toString()
            if((dataUser.avatar != null) and (dataUser.avatar != "")) {
                Picasso.get().load(dataUser.avatar).into(imvAvatar)
            }
            btnConfirm.visibility = View.INVISIBLE


            curMarkerUser = marker
            btnHello.setOnClickListener {
                if (AppController.settingSocket == "true") {
                    // Chạy audio
                    if (AppController.soundMode == 1) {
                        when (AppController.voiceType) {
                            1 -> {
                                mAudioPlayer.play(this, R.raw.gui_loi_chao)
                            }
                            2 -> {
                                mAudioPlayer.play(this, R.raw.gui_loi_chao_2)
                            }
                        }
                    }

                    attemptHello(AppController.userProfile?.email.toString(), AppController.userProfile?.name.toString(), dataUser.socketID.toString())
                    mPopupWindowUser!!.dismiss()
                    curMarkerUser = null
                    viewUserPopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                } else {
                    TastyToast.makeText(this, "Bạn phải bật chết độ giao tiếp với tài xế để có thể thực hiện thao tác này!", TastyToast.LENGTH_LONG, TastyToast.INFO).show()
                }
            }

            // Lấy view của viewTouch
            val viewTouch = viewUserPopup.findViewById<View>(R.id.viewTouch_maker_user)
            val sfg = SimpleFingerGestures()
            sfg.setDebug(true)
            sfg.consumeTouchEvents = true

            var mType: Number = 0

            sfg.setOnFingerGestureListener(object : SimpleFingerGestures.OnFingerGestureListener {
                override fun onDoubleTap(fingers: Int): Boolean {
                    btnConfirm.visibility = View.VISIBLE
                    //==== Ban ngày
                    // Chưa biết

                    //==== Ban đêm
                    // Nháy báo hiệu xe ngược chiều giảm độ sáng đèn pha
                    mType = 1
                    imvType.setImageResource(R.drawable.ic_headlights_on_44dp)
                    tvType.text = "HẠ ĐỘ SÁNG ĐÈN PHA"

                    // return true thì sẽ cho phép các gesture khác cũng bắt được sự kiện đó
                    // return false thì chỉ sự kiện nào bắt được đúng sự iện đó và không gửi đi tiếp
                    return false
                }

                override fun onPinch(fingers: Int, gestureDuration: Long, gestureDistance: Double): Boolean {
                   btnConfirm.visibility = View.INVISIBLE
                    mType = 0
                    return false
                }

                override fun onUnpinch(fingers: Int, gestureDuration: Long, gestureDistance: Double): Boolean {
                    btnConfirm.visibility = View.INVISIBLE
                    mType = 0
                    return false
                }

                override fun onSwipeDown(fingers: Int, gestureDuration: Long, gestureDistance: Double): Boolean {
                    btnConfirm.visibility = View.INVISIBLE
                    mType = 0
                    if (fingers == 2 && gestureDistance >= 120) {
                        // Báo giảm tốc độ
                        btnConfirm.visibility = View.VISIBLE
                        mType = 2
                        imvType.setImageResource(R.drawable.ic_report_hazard_44dp)
                        tvType.text = "NGUY HIỂM NÊN GIẢM TỐC ĐỘ"
                    }
                    if (fingers == 3 && gestureDistance >= 120) {
                        // Báo có giám sát
                        btnConfirm.visibility = View.VISIBLE
                        mType = 3
                        imvType.setImageResource(R.drawable.ic_report_camera_trafficlight_44dp)
                        tvType.text = "CÓ GIÁM SÁT GẦN ĐÓ"
                    }
                    return false
                }

                override fun onSwipeUp(fingers: Int, gestureDuration: Long, gestureDistance: Double): Boolean {
                    btnConfirm.visibility = View.INVISIBLE
                    mType = 0
                    if (fingers == 2 && gestureDistance >= 120) {
                        // Báo nên quay đầu
                        btnConfirm.visibility = View.VISIBLE
                        mType = 4
                        imvType.setImageResource(R.drawable.ic_report_turn_around_44dp)
                        tvType.text = "NGUY HIỂM NÊN QUAY ĐẦU"
                    }
                    return false
                }

                override fun onSwipeLeft(fingers: Int, gestureDuration: Long, gestureDistance: Double): Boolean {
                    btnConfirm.visibility = View.INVISIBLE
                    mType = 0
                    return false
                }

                override fun onSwipeRight(fingers: Int, gestureDuration: Long, gestureDistance: Double): Boolean {
                    btnConfirm.visibility = View.INVISIBLE
                    mType = 0
                    return false
                }
            })

            viewTouch.setOnTouchListener(sfg)

            btnConfirm.setOnClickListener {
                if (AppController.settingSocket == "true") {
                    it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                    when (mType) {
                        1 -> {
                            attemptWarnStrongLight(AppController.userProfile?.email.toString(), AppController.userProfile?.name.toString(), dataUser.socketID.toString())
                        }
                        2 -> {
                            attemptWarnSlowDown(AppController.userProfile?.email.toString(), AppController.userProfile?.name.toString(), dataUser.socketID.toString())
                        }
                        3 -> {
                            attemptWarnWatcher(AppController.userProfile?.email.toString(), AppController.userProfile?.name.toString(), dataUser.socketID.toString())
                        }
                        4 -> {
                            attemptWarnTurnAround(AppController.userProfile?.email.toString(), AppController.userProfile?.name.toString(), dataUser.socketID.toString())
                        }
                    }
                    TastyToast.makeText(this, "Đã gửi cảnh báo cho tài xế", TastyToast.LENGTH_LONG, TastyToast.SUCCESS).show()
                    btnConfirm.visibility = View.INVISIBLE
                    mType = 0
                } else {
                    TastyToast.makeText(this, "Bạn phải bật chết độ giao tiếp với tài xế để có thể thực hiện thao tác này!", TastyToast.LENGTH_LONG, TastyToast.INFO).show()
                }
            }

            imvInstruction.setOnClickListener {
                val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val viewPopup = inflater.inflate(R.layout.marker_user_instruction_dialog_layout, null)

                val mPopupWindow = PopupWindow(viewPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                mPopupWindow.showAtLocation(parent, Gravity.CENTER, 0, 0)

                val btnClose = viewPopup.findViewById<ImageView>(R.id.btnClose_marker_user_instruction_dialog_layout)
                val layout1 = viewPopup.findViewById<LinearLayout>(R.id.layout1_marker_user_instruction_dialog_layout)
                val layout2 = viewPopup.findViewById<LinearLayout>(R.id.layout2_marker_user_instruction_dialog_layout)
                val layout3 = viewPopup.findViewById<LinearLayout>(R.id.layout3_marker_user_instruction_dialog_layout)
                val layout4 = viewPopup.findViewById<LinearLayout>(R.id.layout4_marker_user_instruction_dialog_layout)
                val layout5 = viewPopup.findViewById<LinearLayout>(R.id.layout5_marker_user_instruction_dialog_layout)

                layout1.setOnClickListener {
                    mPopupWindow.dismiss()
                    // Báo hạ độ sáng đèn pha
                    btnConfirm.visibility = View.VISIBLE
                    mType = 1
                    imvType.setImageResource(R.drawable.ic_headlights_on_44dp)
                    tvType.text = "HẠ ĐỘ SÁNG ĐÈN PHA"
                }
                layout2.setOnClickListener {
                    mPopupWindow.dismiss()
                    // Báo giảm tốc độ
                    btnConfirm.visibility = View.VISIBLE
                    mType = 2
                    imvType.setImageResource(R.drawable.ic_report_hazard_44dp)
                    tvType.text = "NGUY HIỂM NÊN GIẢM TỐC ĐỘ"
                }
                layout3.setOnClickListener {
                    mPopupWindow.dismiss()
                    // Báo có giám sát
                    btnConfirm.visibility = View.VISIBLE
                    mType = 3
                    imvType.setImageResource(R.drawable.ic_report_camera_trafficlight_44dp)
                    tvType.text = "CÓ GIÁM SÁT GẦN ĐÓ"
                }
                layout4.setOnClickListener {
                    mPopupWindow.dismiss()
                    // Báo nên quay đầu
                    btnConfirm.visibility = View.VISIBLE
                    mType = 4
                    imvType.setImageResource(R.drawable.ic_report_turn_around_44dp)
                    tvType.text = "NGUY HIỂM NÊN QUAY ĐẦU"
                }
                layout5.setOnClickListener {
                    mPopupWindow.dismiss()
                    btnHello.performClick()
                }
                btnClose.setOnClickListener {
                    mPopupWindow.dismiss()
                }
            }
        }
    }

    private fun openImageOnReport(id: String) {
        // outdate progressDialog
//        val progress = ProgressDialog.show(this, null, "Đang tải hình ảnh...", true)
        showLoadingDialog();

        AsyncTask.execute {
            try {
                runOnUiThread {
                    val service = APIServiceGenerator.createService(ReportService::class.java)
                    val call = service.getBase64ImageReport(id)
                    call.enqueue(object : Callback<ReportResponse> {
                        override fun onResponse(call: Call<ReportResponse>, response: Response<ReportResponse>) {
                            if (response.isSuccessful) {
                                if (response.body()!!.report!!.byteImageFile == "") {
                                    TastyToast.makeText(this@MainActivity, "Không có dữ liệu hình ảnh", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
                                } else {
                                    val intent = Intent(this@MainActivity, CustomCameraActivity::class.java)
                                    intent.putExtra("base64Image", response.body()!!.report!!.byteImageFile!!)
                                    startActivity(intent)
                                }
                            } else {
                           }
                        }

                        override fun onFailure(call: Call<ReportResponse>, t: Throwable) {
                            t.printStackTrace()
                        }
                    })
                }

            } catch (exception: Exception) {
                runOnUiThread {
                    TastyToast.makeText(this, exception.toString(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            // outdate progressDialog
//            progress.dismiss()
            hideLoadingDialog();
        }
    }

    private fun openVoiceOnReport(id: String) {
        // outdate progressDialog
//        val progress = ProgressDialog.show(this, null, "Đang tải âm thanh...", true)
        showLoadingDialog();

        AsyncTask.execute {
            try {
                runOnUiThread {
                    val service = APIServiceGenerator.createService(ReportService::class.java)
                    val call = service.getBase64VoiceReport(id)
                    call.enqueue(object : Callback<ReportResponse> {
                        override fun onResponse(call: Call<ReportResponse>, response: Response<ReportResponse>) {
                            if (response.isSuccessful) {
                                if (response.body()!!.report!!.byteAudioFile == "") {
                                    hideLoadingDialog()
                                    TastyToast.makeText(this@MainActivity, "Không có dữ liệu thu âm", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
                                } else {
                                    hideLoadingDialog()
                                    val filePath = externalCacheDir!!.absolutePath + "/audio_decoded.3gp"
                                    FileUtils.decodeAudioFile(response.body()!!.report!!.byteAudioFile!!, filePath)
                                }
                            } else {
                                hideLoadingDialog()
                            }
                        }

                        override fun onFailure(call: Call<ReportResponse>, t: Throwable) {
                            t.printStackTrace()
                            hideLoadingDialog()
                        }
                    })
                }

            } catch (exception: Exception) {
                hideLoadingDialog()
                runOnUiThread {
                    TastyToast.makeText(this, exception.toString(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            // oudated progressDialog
//            progress.dismiss()
        }
    }

    // Sự kiện khi click vào info windows
    override fun onInfoWindowClick(p0: Marker) {
    }

    override fun onInfoWindowClose(p0: Marker?) {
        if (p0?.title == "report") {
            // Đóng popup windows
            mPopupWindowReport?.dismiss()
            curMarkerReport = null
        }
        if (p0?.title == "user") {
            mPopupWindowUser?.dismiss()
            curMarkerUser = null
        }
    }

    // ====================================================================================================================================================== //
    // ======== VỀ GỌI API VÀ LISTENER USER ================================================================================================================= //
    // ====================================================================================================================================================== //

    private fun onAllUserProfileSuccess(response: List<User>) {
        listUser = response

        if (AppController.settingFilterCar == "true") {
            drawValidUsers()
        }
    }

    private fun onGetAllParticipantsLocation(appointmentId: String) {
        val service = APIServiceGenerator.createService(AppointmentService::class.java)
        val call = service.participantsLocation(appointmentId)

        call.enqueue(object : Callback<UserProfileResponse> {
            override fun onResponse(call: Call<UserProfileResponse>, response: Response<UserProfileResponse>) {
                if(response.isSuccessful) {
                    // Xóa marker cũ
                    if(listParticipantMarker.size > 0) {
                        for(i in 0 until listParticipantMarker.size) {
                            listParticipantMarker[i].remove()
                        }
                        listParticipantMarker.clear()
                    }

                    response.body()!!.participants!!.forEach { participant->
                        participantsMarker(participant)
                    }
                } else {
                    Log.d("vcl", "VCCCCCCCCCCCCCCCCCCCCCCCCCLLLLLLLLLLLLLLLLLLLLL")
                }
            }

            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun onGetNearbyUsers() {
        val service = APIServiceGenerator.createService(UserService::class.java)
        val call = service.getNearbyUsers(AppController.userProfile!!._id,lastLocation.latitude, lastLocation.longitude, AppController.settingUserRadius!!.toFloat())
        call.enqueue(object : Callback<NearbyUserResponse> {
            override fun onResponse(call: Call<NearbyUserResponse>, response: Response<NearbyUserResponse>) {
                if (response.isSuccessful) {
                    response.body()!!.users?.let { onAllUserProfileSuccess(it) }
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@MainActivity, "Lỗi: " + apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<NearbyUserResponse>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun drawValidUsers() {
        if (curMarkerUser == null || listUserMarker.size == 0) {
            for (i in 0 until listUserMarker.size) {
                listUserMarker[i].remove()
            }
            listUserMarker.clear()
            for (i in 0 until (listUser.size)) {
                if (listUser[i].email != AppController.userProfile!!.email) {
                    addUserMarker(listUser[i])
                }
            }
        }
        if (curMarkerUser != null) {
            val size = listUserMarker.size
            for (i in (size - 1) downTo 0) {
                if (curMarkerUser!!.id != listUserMarker[i].id) {
                    listUserMarker[i].remove()
                    listUserMarker.removeAt(i)
                }
            }
            val dataUser = curMarkerUser!!.tag as User
            for (i in 0 until (listUser.size)) {
                if (listUser[i].email != AppController.userProfile!!.email && listUser[i]._id != dataUser._id) {
                    addUserMarker(listUser[i])
                }
            }
        }
    }

    private fun addUserMarker(user: User) {
        val markerOptions = MarkerOptions()

        markerOptions.position(LatLng(user.currentLocation!!.coordinates!![1], user.currentLocation!!.coordinates!![0]))
        markerOptions.title("user")
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_car_44dp))
        val marker = mMap.addMarker(markerOptions)
        listUserMarker.add(marker)
        marker.tag = user
    }

    private fun participantsMarker(user: User) {
        val markerOptions = MarkerOptions()

        markerOptions.position(LatLng(user.currentLocation!!.coordinates!![1], user.currentLocation!!.coordinates!![0]))
        markerOptions.title("user")
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_participant_marker))
        val marker = mMap.addMarker(markerOptions)
        listParticipantMarker.add(marker)
        marker.tag = user
    }

    private fun onUpdateCurrentLocation(user: User) {
        val service = APIServiceGenerator.createService(UserService::class.java)
        val call = service.updateCurrentLocation(user)
        call.enqueue(object : Callback<UserProfileResponse> {
            override fun onResponse(call: Call<UserProfileResponse>, response: Response<UserProfileResponse>) {
                if (response.isSuccessful) {

                } else {

                }
            }

            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                Log.e("Failure", "Error: " + t.message)
            }
        })
    }

    private fun onUpdateSocketID(user: User) {
        val service = APIServiceGenerator.createService(UserService::class.java)
        val call = service.updateSocketID(user)
        call.enqueue(object : Callback<UserProfileResponse> {
            override fun onResponse(call: Call<UserProfileResponse>, response: Response<UserProfileResponse>) {
                if (response.isSuccessful) {
                } else {
                }
            }

            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                Log.e("Failure", "Error: " + t.message)
            }
        })
    }

    private fun onUpdateStatus(user: User) {
        val service = APIServiceGenerator.createService(UserService::class.java)
        val call = service.updateStatus(user)
        call.enqueue(object : Callback<UserProfileResponse> {
            override fun onResponse(call: Call<UserProfileResponse>, response: Response<UserProfileResponse>) {
                if (response.isSuccessful) {

                } else {
                }
            }

            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                Log.e("Failure", "Error: " + t.message)
            }
        })
    }

    // ======================================================================================================================================================== //
    // ======== SOCKET EVENT ================================================================================================================================== //
    // ======================================================================================================================================================== //
    private fun initSocket() {
        socket = SocketService().getSocket()
        socket.on(Socket.EVENT_CONNECT, onConnect)
        socket.on(Socket.EVENT_DISCONNECT, onDisconnect)
        socket.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        socket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)

        socket.on("event_hello_socket", onSayHello)
        socket.on("event_warn_strong_light_socket", onWarnStrongLight)
        socket.on("event_warn_watcher_socket", onWarnWatcher)
        socket.on("event_warn_slow_down_socket", onWarnSlowDown)
        socket.on("event_warn_turn_around_socket", onWarnTurnAround)
        socket.on("event_warn_thank_socket", onWarnThank)

        socket.on("event_report_other_socket", onReportOther)
        socket.connect()
    }

    private fun destroySocket() {
        socket.off(Socket.EVENT_CONNECT, onConnect)
        socket.off(Socket.EVENT_DISCONNECT, onDisconnect)
        socket.off(Socket.EVENT_CONNECT_ERROR, onConnectError)
        socket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)

        socket.off("event_hello_socket", onSayHello)
        socket.off("event_warn_strong_light_socket", onWarnStrongLight)
        socket.off("event_warn_watcher_socket", onWarnWatcher)
        socket.off("event_warn_slow_down_socket", onWarnSlowDown)
        socket.off("event_warn_turn_around_socket", onWarnTurnAround)
        socket.off("event_warn_thank_socket", onWarnThank)

        socket.off("event_report_other_socket", onReportOther)
        socket.disconnect()
    }

    private val onConnect = Emitter.Listener {
        this.runOnUiThread {
            // Gán socket ID vào cho socketID của người dùng
            if(AppController.userProfile != null) {
                AppController.userProfile?.socketID = socket.id()
                onUpdateSocketID(AppController.userProfile!!)
            }
        }
    }

    private val onDisconnect = Emitter.Listener {
        this.runOnUiThread {
        }
    }

    private val onConnectError = Emitter.Listener {
        this.runOnUiThread {
        }
    }

    private val onSayHello = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            val email: String
            val name: String
            val sendID: String
            val message: String
            try {
                email = args[0] as String
                name = args[1] as String
                sendID = args[2] as String
                message = args[3] as String
            } catch (e: JSONException) {
                Log.e("LOG", e.message!!)
                return@Runnable
            }
            if (message == "hello") {
                if (mPopupWindowHello != null) {
                    mPopupWindowHello?.dismiss()
                }
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tai_xe_da_chao_ban_chuc_thuong_lo_binh_an)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tai_xe_da_chao_ban_chuc_thuong_lo_binh_an_2)
                        }
                    }
                }

                val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val viewHelloPopup = inflater.inflate(R.layout.hello_dialog_layout, null)

                // Layout mới
                mPopupWindowHello = PopupWindow(viewHelloPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                mPopupWindowHello!!.showAtLocation(parent, Gravity.CENTER, 0, 0)

                val tvEmail = viewHelloPopup.findViewById<TextView>(R.id.tvEmail_hello_dialog)
                val btnHello = viewHelloPopup.findViewById<Button>(R.id.btnHello_hello_dialog)
                val imImage = viewHelloPopup.findViewById<ImageView>(R.id.imImage_hello_dialog)
                val btnOther = viewHelloPopup.findViewById<Button>(R.id.btnOther_hello_dialog)

                tvEmail.text = name

                val animShake = android.view.animation.AnimationUtils.loadAnimation(this, R.anim.shake)
                imImage.startAnimation(animShake)

                object : CountDownTimer(3000, 500) {
                    override fun onTick(millisUntilFinished: Long) {
                        btnHello.text = String.format(Locale.getDefault(), "%s (%d)",
                                "CHÀO LẠI",
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) + 1)
                        viewHelloPopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                    }

                    override fun onFinish() {
                        mPopupWindowHello!!.dismiss()
                    }
                }.start()

                btnHello.setOnClickListener {
                    // Chạy audio
                    if (AppController.soundMode == 1) {
                        when (AppController.voiceType) {
                            1 -> {
                                mAudioPlayer.play(this, R.raw.gui_loi_chao)
                            }
                            2 -> {
                                mAudioPlayer.play(this, R.raw.gui_loi_chao_2)
                            }
                        }
                    }
                    attemptHello(AppController.userProfile?.email.toString(), AppController.userProfile?.name.toString(), sendID)
                    mPopupWindowHello!!.dismiss()
                    it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                }

                btnOther.setOnClickListener {
                    mPopupWindowHello!!.dismiss()
                    it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                    var i = 0
                    var isFinish = false
                    while (!isFinish && i < listUserMarker.size) {
                        val data: User = listUserMarker[i].tag as User
                        if (data.email == email) {
                            isFinish = true
                            onOpenReportMarker(listUserMarker[i])
                        } else {
                            i++
                        }
                    }
                }
            }
        })
    }

    private fun attemptHello(email: String, name: String, receiveSocketID: String) {
        if (!socket.connected()) return

        // perform the sending message attempt.

        socket.emit("event_hello_server", email, name, socket.id(), receiveSocketID, "hello")
    }

    private val onWarnStrongLight = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            val email: String
            val name: String
            val sendID: String
            val message: String
            try {
                email = args[0] as String
                name = args[1] as String
                sendID = args[2] as String
                message = args[3] as String

            } catch (e: JSONException) {
                Log.e("LOG", e.message)
                return@Runnable
            }
            if (message == "strong light") {
                if (mPopupWindowStrongLight != null) {
                    mPopupWindowStrongLight?.dismiss()
                }
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.canh_bao_ha_do_sang_den_pha)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.canh_bao_ha_do_sang_den_pha_2)
                        }
                    }
                }

                val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val viewWarnStrongLightPopup = inflater.inflate(R.layout.warn_strong_light_dialog_layout, null)

                // Layout mới
                mPopupWindowStrongLight = PopupWindow(viewWarnStrongLightPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                mPopupWindowStrongLight!!.showAtLocation(parent, Gravity.CENTER, 0, 0)

                val tvEmail = viewWarnStrongLightPopup.findViewById<TextView>(R.id.tvEmail_warn_strong_light_dialog)
                val btnThank = viewWarnStrongLightPopup.findViewById<Button>(R.id.btnThank_warn_strong_light_dialog)
                val imImage = viewWarnStrongLightPopup.findViewById<ImageView>(R.id.imImage_warn_strong_light_dialog)

                tvEmail.text = name
                val animShake = android.view.animation.AnimationUtils.loadAnimation(this, R.anim.shake)
                imImage.startAnimation(animShake)

                object : CountDownTimer(3000, 500) {
                    override fun onTick(millisUntilFinished: Long) {
                        btnThank.text = String.format(Locale.getDefault(), "%s (%d)",
                                "CẢM ƠN",
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) + 1)
                        viewWarnStrongLightPopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                    }

                    override fun onFinish() {
                        mPopupWindowStrongLight!!.dismiss()
                    }
                }.start()

                btnThank.setOnClickListener {
                    attemptWarnThank(AppController.userProfile?.email.toString(), AppController.userProfile?.name.toString(), sendID)
                    mPopupWindowStrongLight!!.dismiss()
                    it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                }
            }
        })
    }

    private fun attemptWarnStrongLight(email: String, name: String, receiveSocketID: String) {
        if (!socket.connected()) return

        // perform the sending message attempt.
        socket.emit("event_warn_strong_light_server", email, name, socket.id(), receiveSocketID, "strong light")
    }

    @SuppressLint("InflateParams")
    private val onWarnWatcher = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            //            val data : JSONObject = args[0] as JSONObject
            val email: String
            val name: String
            val sendID: String
            val message: String
            try {
                email = args[0] as String
                name = args[1] as String
                sendID = args[2] as String
                message = args[3] as String

            } catch (e: JSONException) {
                Log.e("LOG", e.message)
                return@Runnable
            }
            if (message == "watcher") {
                if (mPopupWindowWatcher != null) {
                    mPopupWindowWatcher?.dismiss()
                }
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.canh_bao_co_giam_sat_gan_do)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.canh_bao_co_giam_sat_gan_do_2)
                        }
                    }
                }

                val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val viewWarnWatcherPopup = inflater.inflate(R.layout.warn_watcher_dialog_layout, null)

                // Layout mới
                mPopupWindowWatcher = PopupWindow(viewWarnWatcherPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                mPopupWindowWatcher!!.showAtLocation(parent, Gravity.CENTER, 0, 0)

                val tvEmail = viewWarnWatcherPopup.findViewById<TextView>(R.id.tvEmail_warn_watcher_dialog)
                val btnThank = viewWarnWatcherPopup.findViewById<Button>(R.id.btnThank_warn_watcher_dialog)
                val imImage = viewWarnWatcherPopup.findViewById<ImageView>(R.id.imImage_warn_watcher_dialog)

                tvEmail.text = name

                val animShake = android.view.animation.AnimationUtils.loadAnimation(this, R.anim.shake)
                imImage.startAnimation(animShake)

                object : CountDownTimer(3000, 500) {
                    override fun onTick(millisUntilFinished: Long) {
                        btnThank.text = String.format(Locale.getDefault(), "%s (%d)",
                                "CẢM ƠN",
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) + 1)
                        viewWarnWatcherPopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                    }

                    override fun onFinish() {
                        mPopupWindowWatcher!!.dismiss()
                    }
                }.start()

                btnThank.setOnClickListener {
                    attemptWarnThank(AppController.userProfile?.email.toString(), AppController.userProfile?.name.toString(), sendID)
                    mPopupWindowWatcher!!.dismiss()
                    it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                }
            }
        })
    }

    private fun attemptWarnWatcher(email: String, name: String, receiveSocketID: String) {
        if (!socket.connected()) return

        // perform the sending message attempt.
        socket.emit("event_warn_watcher_server", email, name, socket.id(), receiveSocketID, "watcher")
    }

    @SuppressLint("InflateParams")
    private val onWarnSlowDown = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            val email: String
            val name: String
            val sendID: String
            val message: String
            try {
                email = args[0] as String
                name = args[1] as String
                sendID = args[2] as String
                message = args[3] as String

            } catch (e: JSONException) {
                Log.e("LOG", e.message)
                return@Runnable
            }
            if (message == "slow down") {
                if (mPopupWindowSlowDown != null) {
                    mPopupWindowSlowDown?.dismiss()
                }
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.canh_bao_nguy_hiem_nen_giam_toc_do)
                        }
                        2 -> {
                            //CHƯA THAY
                            mAudioPlayer.play(this, R.raw.canh_bao_nguy_hiem_nen_giam_toc_do_2)
                        }
                    }
                }

                val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val viewWarnSlowDownPopup = inflater.inflate(R.layout.warn_slow_down_dialog_layout, null)

                // Layout mới
                mPopupWindowSlowDown = PopupWindow(viewWarnSlowDownPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                mPopupWindowSlowDown!!.showAtLocation(parent, Gravity.CENTER, 0, 0)

                val tvEmail = viewWarnSlowDownPopup.findViewById<TextView>(R.id.tvEmail_warn_slow_down_dialog)
                val btnThank = viewWarnSlowDownPopup.findViewById<Button>(R.id.btnThank_warn_slow_down_dialog)
                val imImage = viewWarnSlowDownPopup.findViewById<ImageView>(R.id.imImage_warn_slow_down_dialog)

                tvEmail.text = name

                val animShake = android.view.animation.AnimationUtils.loadAnimation(this, R.anim.shake)
                imImage.startAnimation(animShake)

                object : CountDownTimer(3000, 500) {
                    override fun onTick(millisUntilFinished: Long) {
                        btnThank.text = String.format(Locale.getDefault(), "%s (%d)",
                                "CẢM ƠN",
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) + 1)
                        viewWarnSlowDownPopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                    }

                    override fun onFinish() {
                        mPopupWindowSlowDown!!.dismiss()
                    }
                }.start()

                btnThank.setOnClickListener {
                    attemptWarnThank(AppController.userProfile?.email.toString(), AppController.userProfile?.name.toString(), sendID)
                    mPopupWindowSlowDown!!.dismiss()
                    it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                }
            }
        })
    }

    private fun attemptWarnSlowDown(email: String, name: String, receiveSocketID: String) {
        if (!socket.connected()) return

        // perform the sending message attempt.
        socket.emit("event_warn_slow_down_server", email, name, socket.id(), receiveSocketID, "slow down")
    }

    @SuppressLint("InflateParams")
    private val onWarnTurnAround = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            val email: String
            val name: String
            val sendID: String
            val message: String
            try {
                email = args[0] as String
                name = args[1] as String
                sendID = args[2] as String
                message = args[3] as String

            } catch (e: JSONException) {
                Log.e("LOG", e.message)
                return@Runnable
            }
            if (message == "turn around") {
                if (mPopupWindowTurnAround != null) {
                    mPopupWindowTurnAround?.dismiss()
                }
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.canh_bao_nguy_hiem_nen_quay_dau_xe)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.canh_bao_nguy_hiem_nen_quay_dau_xe_2)
                        }
                    }
                }

                val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val viewWarnTurnAroundPopup = inflater.inflate(R.layout.warn_turn_around_dialog_layout, null)

                // Layout mới
                mPopupWindowTurnAround = PopupWindow(viewWarnTurnAroundPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                mPopupWindowTurnAround!!.showAtLocation(parent, Gravity.CENTER, 0, 0)

                val tvEmail = viewWarnTurnAroundPopup.findViewById<TextView>(R.id.tvEmail_warn_turn_around_dialog)
                val btnThank = viewWarnTurnAroundPopup.findViewById<Button>(R.id.btnThank_warn_turn_around_dialog)
                val imImage = viewWarnTurnAroundPopup.findViewById<ImageView>(R.id.imImage_warn_turn_around_dialog)

                tvEmail.text = name

                val animShake = android.view.animation.AnimationUtils.loadAnimation(this, R.anim.shake)
                imImage.startAnimation(animShake)

                object : CountDownTimer(3000, 500) {
                    override fun onTick(millisUntilFinished: Long) {
                        btnThank.text = String.format(Locale.getDefault(), "%s (%d)",
                                "CẢM ƠN",
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) + 1)
                        viewWarnTurnAroundPopup.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                    }

                    override fun onFinish() {
                        mPopupWindowTurnAround!!.dismiss()
                    }
                }.start()

                btnThank.setOnClickListener {
                    attemptWarnThank(AppController.userProfile?.email.toString(), AppController.userProfile?.name.toString(), sendID)
                    mPopupWindowTurnAround!!.dismiss()
                    it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                }
            }
        })
    }

    private fun attemptWarnTurnAround(email: String, name: String, receiveSocketID: String) {
        if (!socket.connected()) return

        // perform the sending message attempt.
        socket.emit("event_warn_turn_around_server", email, name, socket.id(), receiveSocketID, "turn around")
    }

    @SuppressLint("InflateParams")
    private val onWarnThank = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            val email: String
            val name: String
            val sendID: String
            val message: String
            try {
                email = args[0] as String
                name = args[1] as String
                sendID = args[2] as String
                message = args[3] as String

            } catch (e: JSONException) {
                Log.e("LOG", e.message)
                return@Runnable
            }
            if (message == "thank") {
                if (mPopupWindowThank != null) {
                    mPopupWindowThank?.dismiss()
                }
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.tai_xe_da_cam_on_ban)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.tai_xe_da_cam_on_ban_2)
                        }
                    }
                }

                val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val viewWarnThankPopup = inflater.inflate(R.layout.warn_thank_dialog_layout, null)

                // Layout mới
                mPopupWindowThank = PopupWindow(viewWarnThankPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                mPopupWindowThank!!.showAtLocation(parent, Gravity.CENTER, 0, 0)

                val tvEmail = viewWarnThankPopup.findViewById<TextView>(R.id.tvEmail_warn_thank_dialog)
                val imImage = viewWarnThankPopup.findViewById<ImageView>(R.id.imImage_warn_thank_dialog)

                tvEmail.text = name

                val animShake = android.view.animation.AnimationUtils.loadAnimation(this, R.anim.shake)
                imImage.startAnimation(animShake)

                object : CountDownTimer(3000, 500) {
                    override fun onTick(millisUntilFinished: Long) {
                    }

                    override fun onFinish() {
                        mPopupWindowThank!!.dismiss()
                    }
                }.start()
            }
        })
    }

    private fun attemptWarnThank(email: String, name: String, receiveSocketID: String) {
        if (!socket.connected()) return

        // perform the sending message attempt.
        socket.emit("event_warn_thank_server", email, name, socket.id(), receiveSocketID, "thank")
    }

    @SuppressLint("InflateParams")
    private val onReportOther = Emitter.Listener { args ->
        this.runOnUiThread(Runnable {
            val email: String
            val name: String
            val sendID: String
            val type: String
            val base64Image: String
            val licensePLate: String
            try {
                email = args[0] as String
                name = args[1] as String
                sendID = args[2] as String
                type = args[3] as String
                base64Image = args[4] as String
                licensePLate = args[5] as String

            } catch (e: JSONException) {
                Log.e("LOG", e.message)
                return@Runnable
            }
            if (type == "careless_driver") {
//                Toast.makeText(this, "Đã vào", Toast.LENGTH_SHORT).show()
                if (mPopupWindowOther != null) {
                    mPopupWindowOther?.dismiss()
                }
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.canh_bao_co_tai_xe_chay_au)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.canh_bao_co_tai_xe_chay_au_2)
                        }
                    }
                }

                val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val viewReportOtherPopup = inflater.inflate(R.layout.report_other_dialog_layout, null)

                // Layout mới
                mPopupWindowOther = PopupWindow(viewReportOtherPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                mPopupWindowOther!!.showAtLocation(parent, Gravity.CENTER, 0, 0)

                val tvEmail = viewReportOtherPopup.findViewById<TextView>(R.id.tvEmail_report_other_dialog)
                val imImage = viewReportOtherPopup.findViewById<ImageView>(R.id.imImage_report_other_dialog)
                val btnLicensePlate = viewReportOtherPopup.findViewById<Button>(R.id.btnLicensePlate_report_other_dialog)
                val imPicture = viewReportOtherPopup.findViewById<ImageView>(R.id.imPicture_report_other_dialog)
                val btnClose = viewReportOtherPopup.findViewById<Button>(R.id.btnClose_report_other_dialog)

                tvEmail.text = name
                btnLicensePlate.text = licensePLate

                val animShake = android.view.animation.AnimationUtils.loadAnimation(this, R.anim.shake)
                imImage.startAnimation(animShake)

                imPicture.setOnClickListener {
                    it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                    val intent = Intent(this, CustomCameraActivity::class.java)
                    intent.putExtra("base64Image", base64Image)
                    startActivity(intent)
                }

                btnClose.setOnClickListener {
                    mPopupWindowOther!!.dismiss()
                }
            }
            if (type == "piggy") {
                if (mPopupWindowOther != null) {
                    mPopupWindowOther?.dismiss()
                }
                // Chạy audio
                if (AppController.soundMode == 1 || AppController.soundMode == 2) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.canh_bao_co_nguy_hiem_khac)
                        }
                        2 -> {
                            //CHƯA THAY
                            mAudioPlayer.play(this, R.raw.canh_bao_co_nguy_hiem_khac_2)
                        }
                    }
                }

                val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val viewReportOtherPopup = inflater.inflate(R.layout.report_other_other_dialog_layout, null)
                mPopupWindowOther = PopupWindow(viewReportOtherPopup, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                val parent = findViewById<ConstraintLayout>(R.id.app_bar_main)
                mPopupWindowOther!!.showAtLocation(parent, Gravity.CENTER, 0, 0)

                val tvEmail = viewReportOtherPopup.findViewById<TextView>(R.id.tvEmail_report_other_other_dialog)
                val imImage = viewReportOtherPopup.findViewById<ImageView>(R.id.imImage_report_other_other_dialog)
                val btnLicensePlate = viewReportOtherPopup.findViewById<Button>(R.id.btnLicensePlate_report_other_other_dialog)
                val imPicture = viewReportOtherPopup.findViewById<ImageView>(R.id.imPicture_report_other_other_dialog)
                val btnClose = viewReportOtherPopup.findViewById<Button>(R.id.btnClose_report_other_other_dialog)
                val tvMess = viewReportOtherPopup.findViewById<TextView>(R.id.tvMess_report_other_other_dialog)

                tvEmail.text = name

                btnLicensePlate.text = licensePLate

                val animShake = android.view.animation.AnimationUtils.loadAnimation(this, R.anim.shake)
                imImage.startAnimation(animShake)

                imPicture.setOnClickListener {
                    it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                    val intent = Intent(this, CustomCameraActivity::class.java)
                    intent.putExtra("base64Image", base64Image)
                    startActivity(intent)
                }

                btnClose.setOnClickListener {
                    mPopupWindowOther!!.dismiss()
                }
            }
        })
    }

    private fun attemptReportOther(email: String, name: String, receiveSocketID: String, type: String, base64Image: String, licensePlate: String) {
        if (!socket.connected()) return

        // perform the sending message attempt.
        socket.emit("event_report_other_server", email, name, socket.id(), receiveSocketID, type, base64Image, licensePlate)
    }

    // ====================================================================================================================================================== //
    // ======== VỀ GỌI API VÀ LISTENER REPORT =============================================================================================================== //
    // ====================================================================================================================================================== //

    private fun onGetNearbyReports() {
        val service = APIServiceGenerator.createService(ReportService::class.java)
        val call = service.getNearbyReports(lastLocation.latitude, lastLocation.longitude, AppController.settingReportRadius!!.toFloat())
        call.enqueue(object : Callback<NearbyReportsResponse> {
            override fun onResponse(call: Call<NearbyReportsResponse>, response: Response<NearbyReportsResponse>) {
                if (response.isSuccessful) {
                    onNearbyReportsSuccess(response.body()!!)
                } else {
                }
            }

            override fun onFailure(call: Call<NearbyReportsResponse>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun onAllReportSuccess(response: List<Report>) {
        listReport = response
        drawValidReports()
    }

    private fun onNearbyReportsSuccess(response: NearbyReportsResponse) {
        listReport = response.reports!!

        for (i in 0 until (response.reports!!.size)) {
            listReport[i].distance = response.reports!![i].distance
        }

        if (AppController.settingFilterReport == "true") {
            drawValidReports()
        }
    }

    private fun drawValidReports() {
        if (curMarkerReport == null || listReportMarker.size == 0) {
            for (i in 0 until listReportMarker.size) {
                listReportMarker[i].remove()
            }
            listReportMarker.clear()
            for (i in 0 until (listReport.size)) {
                addReportMarker(listReport[i])
            }
        }
        if (curMarkerReport != null) {
            val size = listReportMarker.size
            for (i in (size - 1) downTo 0) {
                if (curMarkerReport!!.id != listReportMarker[i].id) {
                    listReportMarker[i].remove()
                    listReportMarker.removeAt(i)
                }
            }
            val dataReport = curMarkerReport!!.tag as Report
            for (i in 0 until (listReport.size)) {
                if (listReport[i]._id != dataReport._id) {
                    addReportMarker(listReport[i])
                }
            }
        }
    }

    private fun addReportMarker(report: Report) {
        val markerOptions = MarkerOptions()
        // LatLag điền theo thứ tự latitude, longitude
        // Còn ở server Geo là theo thứ tự longitude, latitude

        markerOptions.position(LatLng(report.geometry!!.coordinates!![1], report.geometry!!.coordinates!![0]))
        markerOptions.title("report")

        when (report.type.toString()) {
            "traffic" -> {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.traffic_bar_report_trafficjam))
            }
            "crash" -> {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.traffic_bar_report_accident))
            }
            "hazard" -> {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.traffic_bar_report_hazard))
            }
            "help" -> {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.traffic_bar_report_assistance))
            }
            "other" -> {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.traffic_bar_report_other))
            }
        }
        val marker = mMap.addMarker(markerOptions)
        listReportMarker.add(marker)
        marker.tag = report
    }

    private fun onDeleteReport(reportID: String) {
        val service = APIServiceGenerator.createService(ReportService::class.java)
        val call = service.deleteReport(reportID)
        call.enqueue(object : Callback<ReportResponse> {
            override fun onResponse(call: Call<ReportResponse>, response: Response<ReportResponse>) {
                if (response.isSuccessful) {
                    TastyToast.makeText(this@MainActivity, "Xoá báo hiệu thành công!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                } else {
                }
            }

            override fun onFailure(call: Call<ReportResponse>, t: Throwable) {
                Log.e("Failure", "Error: " + t.message)
            }
        })
    }

    private fun onUpdateNumReport(id: String) {
        val service = APIServiceGenerator.createService(ReportService::class.java)
        val call = service.updateNumReport(id)
        call.enqueue(object : Callback<ReportResponse> {
            override fun onResponse(call: Call<ReportResponse>, response: Response<ReportResponse>) {
                if (response.isSuccessful) {
                    TastyToast.makeText(this@MainActivity, "Bình chọn báo cáo thành công!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                } else {
                }
            }

            override fun onFailure(call: Call<ReportResponse>, t: Throwable) {
                Log.e("Failure", "Error: " + t.message)
            }
        })
    }

    private fun onUpdateNumDelete(id: String) {
        val service = APIServiceGenerator.createService(ReportService::class.java)
        val call = service.updateNumDelete(id)
        call.enqueue(object : Callback<ReportResponse> {
            override fun onResponse(call: Call<ReportResponse>, response: Response<ReportResponse>) {
                if (response.isSuccessful) {
                    TastyToast.makeText(this@MainActivity, "Yêu cầu bỏ báo cáo thành công!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                } else {
                }
            }

            override fun onFailure(call: Call<ReportResponse>, t: Throwable) {
                Log.e("Failure", "Error: " + t.message)
            }
        })
    }


// ====================================================================================================================================================== //
// ======== GESTURE DETECTOR ============================================================================================================================ //
// ====================================================================================================================================================== //

    private fun initStepRecyclerView(myDataSet: Route, view: View) {
        val viewManagerStep = androidx.recyclerview.widget.LinearLayoutManager(this)
        val viewAdapterStep = StepAdapter(getStepSet(myDataSet))

        val recyclerViewStep = view.findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycler_view_steps_layout)

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerViewStep.setHasFixedSize(true)

        // use a linear layout manager
        recyclerViewStep.layoutManager = viewManagerStep

        // specify an viewAdapterStep (see also next example)
        recyclerViewStep.adapter = viewAdapterStep
    }

    private lateinit var mItemTouchHelper: ItemTouchHelper

    private lateinit var viewAdapterEditDirection: PlaceAdapter

    private fun initDirectionRecyclerView(myDataSet: ArrayList<SimplePlace>, view: View, btnAdd: TextView) {
        val viewManagerEditDirection = androidx.recyclerview.widget.LinearLayoutManager(this)
        viewAdapterEditDirection = PlaceAdapter(myDataSet, this)

        val recyclerViewEditDirection = view.findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycler_view_edit_direction_layout).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManagerEditDirection

            // specify an viewAdapterStep (see also next example)
            adapter = viewAdapterEditDirection
        }
        val callback = SimpleItemTouchHelperCallback(viewAdapterEditDirection)
        mItemTouchHelper = ItemTouchHelper(callback)
        mItemTouchHelper.attachToRecyclerView(recyclerViewEditDirection)

        btnAdd.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            // Chạy audio
            if (AppController.soundMode == 1) {
                when (AppController.voiceType) {
                    1 -> {
                        mAudioPlayer.play(this, R.raw.them_diem_dung)
                    }
                    2 -> {
                        mAudioPlayer.play(this, R.raw.them_diem_dung_2)
                    }
                }
            }
            showAddPlacePopup(myDataSet, viewAdapterEditDirection)
        }
    }

    private fun getStepSet(route: Route): ArrayList<Step> {
        val stepSet: ArrayList<Step> = ArrayList()
        for (iL in 0 until route.legs!!.size) {
            for (iS in 0 until route.legs!![iL].steps!!.size) {
                stepSet.add(route.legs!![iL].steps!![iS])
            }
        }
        return stepSet
    }

    override fun onStartDrag(viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder) {
        mItemTouchHelper.startDrag(viewHolder)
    }

    private var nearbyPlacesResultMarkers: ArrayList<Marker> = ArrayList()

    private fun getNearbyPlaces(type: String, location: Location, radius: Int, feature: Int) {
        showLoadingDialog()
        if (nearbyPlacesResultMarkers.size > 0) {
            for (i in 0 until nearbyPlacesResultMarkers.size) {
                nearbyPlacesResultMarkers[i].remove()
            }
            nearbyPlacesResultMarkers.clear()
        }

        // Build retrofit
        val retrofit = Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/maps/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
        val service = retrofit.create(NearbyPlacesInterface::class.java)
        // Send request
        val call = service.getNearbyPlaces(type, location.latitude.toString() + "," + location.longitude.toString(), radius)
        call.enqueue(object : Callback<NearbyPlacesResponse> {
            override fun onFailure(call: Call<NearbyPlacesResponse>?, t: Throwable?) {
                Log.d("onFailure", t.toString())
                hideLoadingDialog()
            }

            override fun onResponse(call: Call<NearbyPlacesResponse>?, response: Response<NearbyPlacesResponse>) {
                hideLoadingDialog();

                try {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lastLocation.latitude, lastLocation.longitude), 14f))
                    nearbyPlacesResultMarkers.clear()
                    for (i in response.body()!!.results.indices) {
                        val name = response.body()!!.results[i].name
                        val lat = response.body()!!.results[i].geometry.location.lat
                        val lng = response.body()!!.results[i].geometry.location.lng
                        val latLng = LatLng(lat, lng)
                        val simplePlace = SimplePlace(name, LatLng(lat, lng))
                        val place = Place.builder()
                        val markerOption = MarkerOptions()
                        markerOption.position(latLng)
                        when(feature) {
                            WAY_POINT -> {
                                markerOption.title("nearby_places_result")
                            }
                            QUICK_NAV -> {
                                markerOption.title("nearby_places_result_quick_navigate")
                                place.setName(name)
                                place.setLatLng(LatLng(lat,lng))
                            }
                        }
                        val marker = mMap.addMarker(markerOption)
                        when(feature) {
                            WAY_POINT -> {
                                marker.tag = simplePlace
                            }
                            QUICK_NAV -> {
                                marker.tag = place.build()
                            }
                        }
                        nearbyPlacesResultMarkers.add(marker)
                    }

                } catch (e: Exception) {
                    Log.d("onResponse", "There is an error")
                    e.printStackTrace()
                }
            }
        })

        // Handle response
    }

//    private lateinit var viewAdapterNearbyUser: NearbyUserAdapter
//    private fun initListNearbyUserRecyclerView(myDataSet: MutableList<User>, view: View) {
//        val viewManagerEditDirection = androidx.recyclerview.widget.LinearLayoutManager(this)
//        viewAdapterNearbyUser = NearbyUserAdapter(myDataSet) { userItem: User ->
//            run {
//                for (userMarker in listUserMarker) {
//                    if (userItem.email == (userMarker.tag as User).email) {
//                        onOpenReportMarker(userMarker)
//                        mPopupWindowChat?.dismiss()
//                        break
//                    }
//                }
//            }
//        }
//
//        view.findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycler_view_list_nearby_user_chat_dialog).apply {
//            // use this setting to improve performance if you know that changes
//            // in content do not change the layout size of the RecyclerView
//            setHasFixedSize(true)
//
//            // use a linear layout manager
//            layoutManager = viewManagerEditDirection
//
//            // specify an viewAdapterStep (see also next example)
//            adapter = viewAdapterNearbyUser
//
//        }
//    }

    // The deep dark abyss that is called 'Test Region'. Uppon crossing this land, the only way to come back is to discard all changes.
    // TRYNA MERGE THIS INTO MASTER AND IMMA FCKING SLY YOU BRUH
    private fun initPlaces() {
        Places.initialize(this, getString(R.string.places_api))
        placesClient = Places.createClient(this)
    }

    private fun setUpPlacesAutoComplete() {
        val autoCompleteFragment = supportFragmentManager.findFragmentById(R.id.place_autocomplete) as AutocompleteSupportFragment
        autoCompleteFragment.setPlaceFields(placeFields)

        autoCompleteFragment.setOnPlaceSelectedListener(object:com.google.android.libraries.places.widget.listener.PlaceSelectionListener{
            override fun onPlaceSelected(p0: Place) {

                // Set current selected place
                currentSelectedPlace = p0
                addMarker(p0)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(p0.latLng, 17f))

                showPlaceInfoPopup(p0)

                // Thêm place vào AppController
                if (AppController.listHistoryPlace.size >= 3) {
                    for (i in 0 until 2) {
                        if (i == 2) {
                            AppController.listHistoryPlace[i] = p0
                        } else {
                            val temp = i + 1
                            AppController.listHistoryPlace[i] = AppController.listHistoryPlace[temp]
                        }
                    }
                } else {
                    AppController.listHistoryPlace.add(p0)
                }
            }

            override fun onError(p0: Status) {
                Toast.makeText(this@MainActivity, ""+p0.statusMessage, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun showLoadingDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setView(R.layout.layout_loading_dialog)
        loadingAlert = builder.create()
        loadingAlert?.show()
    }

    private fun hideLoadingDialog() {
        loadingAlert?.dismiss()
    }

    private fun animateQnab() {
        if(isOpen) {
            imvQuickNavParking.startAnimation(qnabClose)
            imvQuickNavPetrol.startAnimation(qnabClose)
            imvQuickNavParking.isClickable = false
            imvQuickNavPetrol.isClickable = false
            isOpen = false
        } else {
            imvQuickNavParking.startAnimation(qnabOpen)
            imvQuickNavPetrol.startAnimation(qnabOpen)
            imvQuickNavParking.isClickable = true
            imvQuickNavPetrol.isClickable = true
            isOpen = true
        }
    }
}