package vn.edu.driverteam.govn.services.models

import vn.edu.driverteam.govn.models.Appointment

class AppointmentResponse {
    var success: Boolean? = null
    var appointment: Appointment? = null
    var appointments: List<Appointment>? = null

}