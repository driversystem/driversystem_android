package vn.edu.driverteam.govn.services

import io.socket.client.IO
import io.socket.client.Socket
import java.net.URISyntaxException

class SocketService {
    // Socket
    private var socket: Socket

    constructor(){
        try {
            socket = IO.socket("https://driversystemapp.herokuapp.com/")
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }
    }
     fun getSocket() : Socket{
         return socket
     }
}