package vn.edu.driverteam.govn.activity.common

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.format.DateFormat
import android.widget.*
import androidx.annotation.RequiresApi
import butterknife.BindView
import butterknife.ButterKnife
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.models.Appointment
import vn.edu.driverteam.govn.models.AppointmentProperties
import vn.edu.driverteam.govn.models.Geometry
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.AppointmentService
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.GroupTravelService
import vn.edu.driverteam.govn.services.models.SuccessResponse
import vn.edu.driverteam.govn.utils.NotificationPublisher
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.sdsmdg.tastytoast.TastyToast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*

class CreateAppointmentActivity : AppCompatActivity() {
    @BindView(R.id.imBack_create_appointment)
    lateinit var btnBack: LinearLayout

    @BindView(R.id.dp_date)
    lateinit var datePicker: DatePicker

    @BindView(R.id.tp_time)
    lateinit var timePicker: TimePicker

    @BindView(R.id.btn_create_appointment)
    lateinit var btnCreateAppointment: Button

    @BindView(R.id.et_appointment_message)
    lateinit var appointmentMessage: EditText

    @BindView(R.id.switch_groupTravel)
    lateinit var switchGroupTravel: Switch

    private lateinit var groupId: String
    private var groupName: String? = null
    private var chosenPlace: Place? = null
    private lateinit var placesClient: PlacesClient
    private var placeFields = listOf(Place.Field.ID,
            Place.Field.NAME,
            Place.Field.ADDRESS,
            Place.Field.LAT_LNG)

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_appointment)
        groupId = intent.getSerializableExtra("group_id") as String
        groupName = intent.getStringExtra("group_name")
        ButterKnife.bind(this)

        initComponent()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initComponent() {
        btnBack.setOnClickListener {
            finish()
        }
        timePicker.setIs24HourView(true)
        btnCreateAppointment.setOnClickListener {
            if (chosenPlace != null) {
                onCreateAppointment()
            }
        }

        initPlaces()
        setUpPlacesAutoComplete()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun onCreateAppointment() {
        val reminderTime = LocalDateTime.of(datePicker.year, (datePicker.month + 1), datePicker.dayOfMonth, timePicker.hour.minus(1), timePicker.minute)
        val time = LocalDateTime.of(datePicker.year, (datePicker.month + 1), datePicker.dayOfMonth, timePicker.hour, timePicker.minute)

        val listGeo: List<Double> = listOf(chosenPlace!!.latLng!!.longitude, chosenPlace!!.latLng!!.latitude)
        val location = Geometry("Point", listGeo)
        val properties = AppointmentProperties(switchGroupTravel.isChecked)
        val newAppointment = Appointment(time.atOffset(ZoneOffset.ofHours(7)).toString(),
                reminderTime.atOffset(ZoneOffset.ofHours(7)).toString(),
                location,
                appointmentMessage.text.toString(),
                chosenPlace!!.name.toString(),
                groupId,
                properties,
                "Pending",
                chosenPlace!!.address.toString())

        val service = APIServiceGenerator.createService(AppointmentService::class.java)
        val call = service.createAppointment(newAppointment)

        call.enqueue(object : Callback<SuccessResponse> {
            override fun onResponse(call: Call<SuccessResponse>, response: Response<SuccessResponse>) {
                if (response.isSuccessful) {
                    TastyToast.makeText(this@CreateAppointmentActivity, "Tạo lịch hẹn thành công", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                    val timeDate = Calendar.getInstance()
                    val remindersTimeDate = Calendar.getInstance()
                    timeDate.set(datePicker.year, datePicker.month, datePicker.dayOfMonth, timePicker.hour, timePicker.minute)
                    remindersTimeDate.set(datePicker.year, datePicker.month, datePicker.dayOfMonth, timePicker.hour.minus(1), timePicker.minute)
                    // Notification
                    startAlarm(timeDate.time, remindersTimeDate.time)
                    // Automation triger group travel
                    if(switchGroupTravel.isChecked) {
                        turnOnGroupTravel(timeDate.time)
                    }
                    finish()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@CreateAppointmentActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<SuccessResponse>, t: Throwable) {
                TastyToast.makeText(this@CreateAppointmentActivity, "Kết nối mạng yếu!!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }

        })
    }

    private fun initPlaces() {
        Places.initialize(this, getString(R.string.places_api))
        placesClient = Places.createClient(this)
    }

    private fun setUpPlacesAutoComplete() {
        val autoCompleteFragment = supportFragmentManager.findFragmentById(R.id.search_location_appointment) as AutocompleteSupportFragment
        autoCompleteFragment.setPlaceFields(placeFields)

        autoCompleteFragment.setOnPlaceSelectedListener(object : com.google.android.libraries.places.widget.listener.PlaceSelectionListener {
            override fun onPlaceSelected(p0: Place) {
                chosenPlace = p0
            }

            override fun onError(p0: Status) {
                Toast.makeText(this@CreateAppointmentActivity, "" + p0.statusMessage, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun startAlarm(appointmentTime: Date, appointmentReminderTime: Date) {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, NotificationPublisher::class.java)
        val time = DateFormat.format("hh:mm 'ngày' dd/MM/yyyy", appointmentTime)
        intent.putExtra("time", time)
        intent.putExtra("groupName", groupName)
        val pendingIntent = PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, appointmentReminderTime.time, pendingIntent)
    }

    private fun turnOnGroupTravel(appointmentTime: Date) {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, GroupTravelService::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 2, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        alarmManager.setExact(AlarmManager.RTC, appointmentTime.time, pendingIntent)
    }
}
