package vn.edu.driverteam.govn.models.group

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.models.Group
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

class ListGroupAdapter(private val myListGroup: MutableList<Group>, val clickListener: (Group) -> Unit) : RecyclerView.Adapter<ListGroupAdapter.ViewHolder>() {
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycler_view_list_group_item, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return myListGroup.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val groupName = holder.view.findViewById<TextView>(R.id.tv_groupName)
        val avatar = holder.view.findViewById<CircleImageView>(R.id.circleImageView_avatar)
        val description = holder.view.findViewById<TextView>(R.id.tv_groupDescription)

        if(myListGroup[position].groupName!!.length < 12){
            groupName.text = myListGroup[position].groupName
        } else {
            groupName.text = myListGroup[position].groupName!!.subSequence(0, 12).toString() + "..."
        }
        if((myListGroup[position].avatar != "") and (myListGroup[position].avatar != null)) {
            Picasso.get().load(myListGroup[position].avatar).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(avatar)
        }
        description.text = myListGroup[position].description

        holder.view.setOnClickListener{ clickListener(myListGroup[position])}
    }

    fun getGroupId(position: Int):String? {
        return myListGroup[position]._id
    }
}