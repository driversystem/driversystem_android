package vn.edu.driverteam.govn.activity.common

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import vn.edu.driverteam.govn.R

class ListLocationCollection : AppCompatActivity() {

    private lateinit var btnBack: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_location_collection)

        initComponent()
    }

    private fun initComponent() {
        btnBack = findViewById(R.id.imBack_list_location_collection)

        btnBack.setOnClickListener {
            finish()
        }


    }
}
