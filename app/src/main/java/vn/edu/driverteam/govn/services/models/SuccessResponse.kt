package vn.edu.driverteam.govn.services.models

class SuccessResponse {
    var success: Boolean? = null
    var message: String? = null
}