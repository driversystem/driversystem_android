package vn.edu.driverteam.govn.activity.common

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.format.DateFormat
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.activity.MainActivity
import vn.edu.driverteam.govn.controllers.AppController
import vn.edu.driverteam.govn.models.Appointment
import vn.edu.driverteam.govn.models.User
import vn.edu.driverteam.govn.models.user.ListMemberAdapter
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.AppointmentService
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.GroupTravelService
import vn.edu.driverteam.govn.services.models.AppointmentResponse
import vn.edu.driverteam.govn.utils.NotificationPublisher
import com.google.gson.internal.LinkedTreeMap
import com.sdsmdg.tastytoast.TastyToast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class AppointmentDetailActivity : AppCompatActivity() {
    @BindView(R.id.imBack_appointment_detail)
    lateinit var btnBack: LinearLayout
    @BindView(R.id.tv_groupName_appointmentDetail)
    lateinit var groupName: TextView
    @BindView(R.id.tv_month_appointmentDetail)
    lateinit var month: TextView
    @BindView(R.id.tv_dayOfWeek_appointmentDetail)
    lateinit var dayOfWeek: TextView
    @BindView(R.id.tv_dayOfMonth_appointmentDetail)
    lateinit var dayOfMonth: TextView
    @BindView(R.id.tv_time_appointmentDetail)
    lateinit var time:TextView
    @BindView(R.id.tv_remaining_time_appointmentDetail)
    lateinit var remainingTime: TextView
    @BindView(R.id.tv_name_appointmentDetail)
    lateinit var locationName: TextView
    @BindView(R.id.tv_address_appointmentDetail)
    lateinit var address: TextView
    @BindView(R.id.tv_appointment_message_appointmentDetail)
    lateinit var appointmentMessage: TextView
    @BindView(R.id.list_member_joined)
    lateinit var recyclerViewListMemberAccepted: RecyclerView
    @BindView(R.id.btn_join_appointment)
    lateinit var btnJoinAppointment: Button
    @BindView(R.id.btn_quick_navigate)
    lateinit var btnQuickNavigate: ImageButton
    @BindView(R.id.tv_groupTravelMode)
    lateinit var tvGroupTravelMode: TextView

    private lateinit var appointmentId: String
    private lateinit var loadingAlert: AlertDialog
    private lateinit var appointment: Appointment
    private lateinit var groupId: String
    private lateinit var appointmentTime: Date
    private lateinit var appointmentReminderTime: Date

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_appointment_detail)
        ButterKnife.bind(this)

        appointmentId = intent.getStringExtra("appointment_id")!!

        loadAppointment()
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.O)
    private fun initComponents(appointment: Appointment) {

        val inputFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val date: LocalDateTime = LocalDateTime.parse((appointment.time as String), inputFormatter)
        val dateReminder: LocalDateTime = LocalDateTime.parse((appointment.reminderTime as String), inputFormatter)

        appointmentTime = Date.from(date.atOffset(ZoneOffset.UTC).toInstant())
        appointmentReminderTime = Date.from(dateReminder.atOffset(ZoneOffset.UTC).toInstant())

        val remainingTimeLong = (appointmentTime.time.minus(Date().time))

        btnBack.setOnClickListener {
            finish()
        }

        if(appointment.properties?.groupTravel!!) {
            tvGroupTravelMode.text = "LỊCH HẸN DI CHUYỂN NHÓM"
        }
        groupId = (appointment.group!! as LinkedTreeMap<*, *>)["_id"].toString()
        groupName.text = (appointment.group!! as LinkedTreeMap<*, *>)["groupName"].toString()
        month.text = (appointmentTime.toInstant().atZone(ZoneId.systemDefault())).month.toString()
        dayOfWeek.text = (appointmentTime.toInstant().atZone(ZoneId.systemDefault())).dayOfWeek.toString()
        dayOfMonth.text = (appointmentTime.toInstant().atZone(ZoneId.systemDefault())).dayOfMonth.toString()
        time.text = (appointmentTime.toInstant().atZone(ZoneId.systemDefault())).hour.toString() + ":" + (appointmentTime.toInstant().atZone(ZoneId.systemDefault())).minute.toString()
        address.text = appointment.locationAddress
        locationName.text = appointment.locationName
        if(remainingTimeLong > 0) {
            val remainingTimeDate = Date(remainingTimeLong).toInstant().atZone(ZoneId.systemDefault())
            remainingTime.text = "Còn " + remainingTimeDate.dayOfYear.toString() + " ngày"
        } else {
                remainingTime.text = "Quá hạn"
        }
        appointmentMessage.text = appointment.reminderMsg

        loadAcceptedMember()

        if(isJoining()) {
            btnJoinAppointment.text = "Hủy"
            btnJoinAppointment.setBackgroundResource(R.drawable.bg_withdraw_appointment)
            btnJoinAppointment.setOnClickListener {
                onWithdrawAppointment()
            }
        } else {
            btnJoinAppointment.setOnClickListener {
                onJoinAppointment()
            }
        }

        btnQuickNavigate.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.putExtra("appointment_location_lng", appointment.location!!.coordinates!![0])
            intent.putExtra("appointment_location_lat", appointment.location!!.coordinates!![1])
            intent.putExtra("appointment_location_name", appointment.locationName)
            intent.putExtra("appointment_location_address", appointment.locationAddress)
            startActivity(intent)
        }
    }

    private fun onJoinAppointment() {
        val service = APIServiceGenerator.createService(AppointmentService::class.java)
        appointment.groupID = groupId
        val call = service.joinAppointment(appointmentId, appointment)

        call.enqueue(object : Callback<AppointmentResponse> {
            override fun onResponse(call: Call<AppointmentResponse>, response: Response<AppointmentResponse>) {
                if(response.isSuccessful) {
                    appointment = response.body()!!.appointment!!
                    loadAcceptedMember()

                    startAlarm()
                    if(appointment.properties!!.groupTravel!!){
                        turnOnGroupTravel()
                    }

                    btnJoinAppointment.text = "Hủy"
                    btnJoinAppointment.setBackgroundResource(R.drawable.bg_withdraw_appointment)
                    btnJoinAppointment.setOnClickListener {
                        onWithdrawAppointment()
                    }
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@AppointmentDetailActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<AppointmentResponse>, t: Throwable) {
                TastyToast.makeText(this@AppointmentDetailActivity, "Kết nối mạng yếu!!!", TastyToast.LENGTH_LONG, TastyToast.WARNING).show()
            }

        })
    }

    private fun onWithdrawAppointment() {
    val service = APIServiceGenerator.createService(AppointmentService::class.java)
    appointment.groupID = groupId
    val call = service.withdrawAppointment(appointmentId, appointment)

    call.enqueue(object : Callback<AppointmentResponse> {
        @SuppressLint("SetTextI18n")
        override fun onResponse(call: Call<AppointmentResponse>, response: Response<AppointmentResponse>) {
            if(response.isSuccessful) {
                appointment = response.body()!!.appointment!!
                loadAcceptedMember()

                cancelAlarm()
                if(appointment.properties!!.groupTravel!!){
                    turnOffGroupTravel()
                }

                btnJoinAppointment.text = "Tham gia"
                btnJoinAppointment.setBackgroundResource(R.drawable.bg_btn_logout)
                btnJoinAppointment.setOnClickListener {
                    onJoinAppointment()
                }
            } else {
                val apiError = ErrorUtils.parseError(response)
                TastyToast.makeText(this@AppointmentDetailActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
            }
        }

        override fun onFailure(call: Call<AppointmentResponse>, t: Throwable) {
            TastyToast.makeText(this@AppointmentDetailActivity, "Kết nối mạng yếu!!!", TastyToast.LENGTH_LONG, TastyToast.WARNING).show()
        }

    })
}

    private fun loadAppointment() {
        showLoadingDialog()
        val service = APIServiceGenerator.createService(AppointmentService::class.java)
        val call = service.getAppointmentById(appointmentId)

        call.enqueue(object : Callback<AppointmentResponse> {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onResponse(call: Call<AppointmentResponse>, response: Response<AppointmentResponse>) {
                if(response.isSuccessful){
                    appointment = response.body()!!.appointment!!
                    initComponents(appointment)
                    hideLoadingDialog()
                } else {
                    hideLoadingDialog()
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@AppointmentDetailActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<AppointmentResponse>, t: Throwable) {
                hideLoadingDialog()
                TastyToast.makeText(this@AppointmentDetailActivity, t.message, TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }

        })
    }

    private fun showLoadingDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setCancelable(false)
        builder.setView(R.layout.layout_loading_dialog)
        loadingAlert = builder.create()
        loadingAlert.show()
    }

    private fun hideLoadingDialog() {
        loadingAlert.dismiss()
    }

    private fun isJoining() : Boolean {
        appointment.accepted!!.forEach { user ->
            if(AppController.userProfile!!._id == user._id) {
                return true
            }
        }
        return false
    }

    private fun loadAcceptedMember() {
        val listRole = ArrayList<String>()
        for (i in appointment.accepted!!.indices){
            listRole.add("")
        }
        recyclerViewListMemberAccepted.apply {
            layoutManager = LinearLayoutManager(this@AppointmentDetailActivity)
            adapter = ListMemberAdapter(appointment.accepted as ArrayList<User>, listRole) {

            }
        }
    }

    @SuppressLint("ServiceCast")
    private fun startAlarm() {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, NotificationPublisher::class.java)
        val time = DateFormat.format("hh:mm 'ngày' dd/MM/yyyy", appointmentTime)
        intent.putExtra("time", time)
        intent.putExtra("groupName", groupName.text)
        val pendingIntent = PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, appointmentReminderTime.time, pendingIntent)
    }

    private fun cancelAlarm() {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, NotificationPublisher::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        alarmManager.cancel(pendingIntent)
    }

    private fun turnOnGroupTravel() {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, GroupTravelService::class.java)
        intent.putExtra("appointmentId", appointmentId)
        intent.putExtra("appointment_location_lng", appointment.location!!.coordinates!![0])
        intent.putExtra("appointment_location_lat", appointment.location!!.coordinates!![1])
        intent.putExtra("appointment_location_name", appointment.locationName)
        intent.putExtra("appointment_location_address", appointment.locationAddress)
        val pendingIntent = PendingIntent.getBroadcast(this, 2, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, appointmentTime.time, pendingIntent)
    }

    private fun turnOffGroupTravel() {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, GroupTravelService::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 2, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        alarmManager.cancel(pendingIntent)
    }
}
