package vn.edu.driverteam.govn.activity.common

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.make
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.ButterKnife
import vn.edu.driverteam.govn.activity.MainActivity
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.controllers.AppController
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.AuthenticationService
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.models.RefreshTokenResponse
import vn.edu.driverteam.govn.utils.SharePrefs
import com.sdsmdg.tastytoast.TastyToast
import kotlinx.android.synthetic.main.activity_splash.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SplashActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        // Init SharePreference
        SharePrefs.Initialize(applicationContext)
        ButterKnife.bind(this)
        initComponents()
    }

    private fun initComponents() {
        swipeRefresh.setOnRefreshListener(this)
        swipeRefresh.isRefreshing = true
        verifyAndRefreshNewToken()
    }

    private fun verifyAndRefreshNewToken() {
        if (AppController.accessToken != null && AppController.accessToken.toString().isNotEmpty()) {
            // Refresh new token
            val service = APIServiceGenerator.createService(AuthenticationService::class.java)
            val call = service.refreshToken(AppController.accessToken.toString())
            call.enqueue(object : Callback<RefreshTokenResponse> {
                override fun onResponse(call: Call<RefreshTokenResponse>, response: Response<RefreshTokenResponse>) {
                    swipeRefresh.isRefreshing = false

                    if (response.isSuccessful) {
                        val accessToken = response.body()!!.token
                        AppController.accessToken = accessToken

                        startMainActivity()
                    } else {
                        val apiError = ErrorUtils.parseError(response)
                        TastyToast.makeText(this@SplashActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()

                        val intent = Intent(this@SplashActivity, LoggedOutActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }

                override fun onFailure(call: Call<RefreshTokenResponse>, t: Throwable) {
                    make(findViewById(R.id.splashLayout), "Không có kết nối Internet", Snackbar.LENGTH_INDEFINITE).show()
                    swipeRefresh.isRefreshing = false
                }
            })
        } else {
            swipeRefresh.isRefreshing = false
            startLoggedOutActivity()
        }
    }

    private fun startMainActivity() {
        val h = Handler()
        h.postDelayed({
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, 1000)
    }

    private fun startLoggedOutActivity() {
        val h = Handler()
        h.postDelayed({
            val intent = Intent(applicationContext, LoggedOutActivity::class.java)
            startActivity(intent)
            finish()
        }, 1000)
    }

    override fun onRefresh() {
        swipeRefresh.isRefreshing = true
        verifyAndRefreshNewToken()
    }
}
