package vn.edu.driverteam.govn.services.models

import vn.edu.driverteam.govn.models.User

class UserProfileResponse {
    var success: Boolean? = null
    var user: User? = null
    var users: List<User>? = null
    var participants: List<User>? = null
}