package vn.edu.driverteam.govn.services.models

import vn.edu.driverteam.govn.models.Report


class ReportResponse {
    var success: Boolean? = null
    var report: Report? = null
}