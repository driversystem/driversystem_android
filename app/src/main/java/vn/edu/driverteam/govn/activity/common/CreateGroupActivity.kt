package vn.edu.driverteam.govn.activity.common

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.models.Group
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.GroupService
import vn.edu.driverteam.govn.services.models.GroupResponse
import com.sdsmdg.tastytoast.TastyToast
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_create_group.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CreateGroupActivity : AppCompatActivity() {

//    companion object {
//        private const val GALLERY_REQUEST_CODE = 1
//    }

    private lateinit var etGroupName: EditText
    private lateinit var btnCreateGroup: Button
    private lateinit var btnCancel: ImageView
    private lateinit var etDescription: EditText
    private lateinit var imvGroupAvatar: CircleImageView
//    private lateinit var storageReference: StorageReference

//    private var imageUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_group)

        initComponent()
    }

    private fun initComponent() {
        btnCancel = findViewById(R.id.btn_cancel_create_group)
        btnCreateGroup = findViewById(R.id.btn_create_group)
        etGroupName = findViewById(R.id.txtGroupName)
        etDescription = findViewById(R.id.txtDescription)
        imvGroupAvatar = findViewById(R.id.avatar_group)
//        storageReference = FirebaseStorage.getInstance().reference.child("images").child("Group").child("GroupAvatar").child("abc")


        btnCreateGroup.setOnClickListener{
            if(etGroupName.text.toString() != "") {
                uploadImageToStorageAndCreateGroup()
            }
        }

        btnCancel.setOnClickListener {
            finish()
        }

//        imvGroupAvatar.setOnClickListener {
//            openGallery()
//        }
    }

//    private fun openGallery() {
//        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//        intent.type = "image/*"
//        val mimeTypes = arrayOf("image/jpeg", "image/png", "image/jpg")
//        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
//        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
//        startActivityForResult(intent, GALLERY_REQUEST_CODE)
//    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//        when(requestCode) {
//            GALLERY_REQUEST_CODE -> {
//                if(resultCode == Activity.RESULT_OK) {
//                    data?.data?.let {
//                        launchImageCrop(it)
//                    }
//                }
//            }
//
//            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
//                if(resultCode == Activity.RESULT_OK) {
//                    val result = CropImage.getActivityResult(data)
//                    result.uri?.let { setImage(it) }
//                } else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                    TastyToast.makeText(this, "Lỗi: Đã có sự cố khi cắt ảnh", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
//                }
//            }
//        }
//    }
//
//    private fun setImage(uri: Uri) {
//        imageUri = uri
//        Picasso.get().load(uri).into(imvGroupAvatar)
//    }

//    private fun launchImageCrop(uri: Uri) {
//        CropImage.activity(uri)
//                .setGuidelines(CropImageView.Guidelines.ON)
//                .setAspectRatio(1080, 1080)
//                .setCropShape(CropImageView.CropShape.RECTANGLE)
//                .start(this)
//    }

    private fun onCreateGroup(imageUri: String) {
        val newGroup = Group(txtGroupName.text!!.toString(), emptyList(), emptyList(), txtDescription.text!!.toString(), imageUri, emptyList())
        val service = APIServiceGenerator.createService(GroupService::class.java)
        val call = service.createGroup(newGroup)
        call.enqueue(object : Callback<GroupResponse>{
            override fun onResponse(call: Call<GroupResponse>, response: Response<GroupResponse>) {
                if (response.isSuccessful) {
                    TastyToast.makeText(this@CreateGroupActivity, "Tạo nhóm thành công", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                    finish()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@CreateGroupActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<GroupResponse>, t: Throwable) {
                TastyToast.makeText(this@CreateGroupActivity, "Kết nối mạng yếu!!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }
        })
    }

    private fun uploadImageToStorageAndCreateGroup() {
        onCreateGroup("https://firebasestorage.googleapis.com/v0/b/super-map-4556d.appspot.com/o/images%2FGroup%2Fic_avatar.jpg?alt=media")
    }
}
