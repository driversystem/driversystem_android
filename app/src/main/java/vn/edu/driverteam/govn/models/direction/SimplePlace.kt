package vn.edu.driverteam.govn.models.direction

import com.google.android.gms.maps.model.LatLng

class SimplePlace(var name:String ?= null, var location:LatLng ?= null)