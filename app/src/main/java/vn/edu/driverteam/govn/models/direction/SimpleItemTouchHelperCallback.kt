package vn.edu.driverteam.govn.models.direction

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper

class SimpleItemTouchHelperCallback(private var mAdapter: ItemTouchHelperAdapter) : ItemTouchHelper.Callback() {
    override fun onSwiped(viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder, direction: Int) {
//        mAdapter.onItemDismiss(viewHolder.adapterPosition)
    }


    override fun isLongPressDragEnabled(): Boolean {
        return true
    }

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        // Set movement flags based on the layout manager
        return if (recyclerView.layoutManager is androidx.recyclerview.widget.GridLayoutManager) {
            val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN or ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
            val swipeFlags = 0
            ItemTouchHelper.Callback.makeMovementFlags(dragFlags, swipeFlags)
        } else {
            val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
            val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
            ItemTouchHelper.Callback.makeMovementFlags(dragFlags, swipeFlags)
        }
    }

    override fun onMove(recyclerView: RecyclerView, source: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        if (target != null) {
            if (source != null) {
                if (source.itemViewType != target.itemViewType) {
                    return false
                }
            }
        }

        // Notify the adapter of the move
        if (source != null) {
            if (target != null) {
                mAdapter.onItemMove(source.adapterPosition, target.adapterPosition)
            }
        }
        return true
    }

    override fun isItemViewSwipeEnabled(): Boolean {
        return false
    }
}