package vn.edu.driverteam.govn.activity.common

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.controllers.AppController
import vn.edu.driverteam.govn.utils.AudioPlayer
import com.github.angads25.toggle.LabeledSwitch

class SettingActivity : AppCompatActivity() {

    @BindView(R.id.switchInvisible_setting)
    lateinit var switchInvisible: LabeledSwitch
//    @BindView(R.id.switchSocket_setting)
//    lateinit var switchSocket: LabeledSwitch
    @BindView(R.id.imBack_setting)
    lateinit var btnBack: LinearLayout
    @BindView(R.id.btnSound_setting)
    lateinit var btnSound: Button
    @BindView(R.id.btnVoice_setting)
    lateinit var btnVoice: Button

    private var mPopupSoundWindow: PopupWindow? = null

    private var mPopupVoiceWindow: PopupWindow? = null

    private var mAudioPlayer = AudioPlayer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        ButterKnife.bind(this)
        initComponents()
    }

    @SuppressLint("SetTextI18n", "InflateParams")
    private fun initComponents() {

        switchInvisible.isOn = AppController.settingInvisible == "true"

//        switchSocket.isOn = AppController.settingSocket == "true"

        when (AppController.soundMode) {
            1 -> {
                btnSound.background = getDrawable(R.drawable.bg_btn_green_main)
                btnSound.text = "MỞ"
            }
            2 -> {
                btnSound.background = getDrawable(R.drawable.bg_btn_send)
                btnSound.text = "BÁO HIỆU"
            }
            3 -> {
                btnSound.background = getDrawable(R.drawable.bg_btn_dismiss)
                btnSound.text = "TẮT"
            }
        }

        when (AppController.voiceType) {
            1 -> {
                btnVoice.background = getDrawable(R.drawable.bg_btn_green_main)
                btnVoice.text = "CHUẨN"
            }
            2 -> {
                btnVoice.background = getDrawable(R.drawable.bg_btn_dismiss)
                btnVoice.text = "NỮ"
            }
        }

//        editLicensePlate.setText(AppController.userProfile!!.vehicle!![0].licensePlate, TextView.BufferType.EDITABLE)
        switchInvisible.setOnToggledListener { _, isOn ->
            if (isOn) {
                // Chạy audio
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this@SettingActivity, R.raw.an_danh_voi_tai_xe_khac)
                        }
                        2 -> {
                            mAudioPlayer.play(this@SettingActivity, R.raw.an_danh_voi_tai_xe_khac_2)
                        }
                    }
                }
                AppController.settingInvisible = "true"
            } else {
                // Chạy audio
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this@SettingActivity, R.raw.hien_thi_voi_tai_xe_khac)
                        }
                        2 -> {
                            mAudioPlayer.play(this@SettingActivity, R.raw.hien_thi_voi_tai_xe_khac_2)
                        }
                    }
                }
                AppController.settingInvisible = "false"
            }
        }

//        switchSocket.setOnToggledListener { _, isOn ->
//            if (isOn) {
//                // Chạy audio
//                if (AppController.soundMode == 1) {
//                    when (AppController.voiceType) {
//                        1 -> {
//                            mAudioPlayer.play(this@SettingActivity, R.raw.giao_tiep_voi_tai_xe_khac)
//                        }
//                        2 -> {
//                            mAudioPlayer.play(this@SettingActivity, R.raw.giao_tiep_voi_tai_xe_khac_2)
//                        }
//                    }
//                }
//                AppController.settingSocket = "true"
//            } else {
//                // Chạy audio
//                if (AppController.soundMode == 1) {
//                    when (AppController.voiceType) {
//                        1 -> {
//                            mAudioPlayer.play(this@SettingActivity, R.raw.khong_giao_tiep_voi_tai_xe_khac)
//                        }
//                        2 -> {
//                            mAudioPlayer.play(this@SettingActivity, R.raw.khong_giao_tiep_voi_tai_xe_khac_2)
//                        }
//                    }
//                }
//                AppController.settingSocket = "false"
//            }
//        }

        btnSound.setOnClickListener { it ->
            val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val viewPopup = inflater.inflate(R.layout.setting_sound_dialog_layout, null)
            // Dùng với layout cũ
//            mPopupSoundWindow = PopupWindow(viewPopup, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
//            mPopupSoundWindow!!.showAtLocation(it, Gravity.NO_GRAVITY, btnSound.x.toInt(), btnSound.y.toInt())

            // Layout mới
            mPopupSoundWindow = PopupWindow(viewPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            mPopupSoundWindow!!.showAtLocation(it, Gravity.CENTER, 0, 0)
            val layoutSoundOn = viewPopup.findViewById<LinearLayout>(R.id.layoutSoundOn_setting_sound_dialog_layout)
            val layoutSoundAlert = viewPopup.findViewById<LinearLayout>(R.id.layoutSoundAlert_setting_sound_dialog_layout)
            val layoutSoundOff = viewPopup.findViewById<LinearLayout>(R.id.layoutSoundOff_setting_sound_dialog_layout)
            val layoutOutside = viewPopup.findViewById<LinearLayout>(R.id.bg_to_remove_setting_sound_dialog_layout)
            val layoutPopup = viewPopup.findViewById<LinearLayout>(R.id.popup_soundSetting)
            val location = IntArray(2)
            btnSound.getLocationOnScreen(location)
            val param = layoutPopup.layoutParams as ViewGroup.MarginLayoutParams
            param.setMargins((location[0]+btnSound.width)-param.width, location[1] - param.height, 0, 0)
            layoutPopup.layoutParams = param


            layoutSoundOn.setOnClickListener {
                // Chạy audio
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.am_thanh_mo)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.am_thanh_mo_2)
                        }
                    }
                }
                it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                btnSound.text = "MỞ"
                AppController.soundMode = 1
                btnSound.background = getDrawable(R.drawable.bg_btn_green_main)
                mPopupSoundWindow!!.dismiss()
            }
            layoutSoundAlert.setOnClickListener {
                // Chạy audio
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.am_thanh_chi_bao_hieu)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.am_thanh_chi_bao_hieu_2)
                        }
                    }
                }
                it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                btnSound.text = "BÁO HIỆU"
                AppController.soundMode = 2
                btnSound.background = getDrawable(R.drawable.bg_btn_send)
                mPopupSoundWindow!!.dismiss()
            }
            layoutSoundOff.setOnClickListener {
                // Chạy audio
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.am_thanh_tat)
                        }
                        2 -> {
                            mAudioPlayer.play(this, R.raw.am_thanh_tat_2)
                        }
                    }
                }
                it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                btnSound.text = "TẮT"
                AppController.soundMode = 3
                btnSound.background = getDrawable(R.drawable.bg_btn_dismiss)
                mPopupSoundWindow!!.dismiss()
            }
            layoutOutside.setOnClickListener {
                it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                mPopupSoundWindow!!.dismiss()
            }
        }


        btnVoice.setOnClickListener { it ->
            // Chạy audio
            if (AppController.soundMode == 1) {
                when (AppController.voiceType) {
                    1 -> {
                        mAudioPlayer.play(this, R.raw.giong_noi_huong_dan)
                    }
                    2 -> {
                        mAudioPlayer.play(this, R.raw.giong_noi_huong_dan_2)
                    }
                }
            }

            val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val viewPopup = inflater.inflate(R.layout.setting_voice_dialog_layout, null)
            // Dùng với layout cũ
//            mPopupSoundWindow = PopupWindow(viewPopup, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
//            mPopupSoundWindow!!.showAtLocation(it, Gravity.NO_GRAVITY, (btnReportRadius.x.toInt() / 2) + 10, (maxY / 2) - (maxY /10))

            // Layout mới
            mPopupVoiceWindow = PopupWindow(viewPopup, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            mPopupVoiceWindow!!.showAtLocation(it, Gravity.CENTER, 0, 0)
            val layoutVoiceStandard = viewPopup.findViewById<LinearLayout>(R.id.layoutVoiceStandard_setting_voice_dialog_layout)
            val layoutVoiceGirl = viewPopup.findViewById<LinearLayout>(R.id.layoutVoiceGirl_setting_voice_dialog_layout)
            val layoutOutside = viewPopup.findViewById<LinearLayout>(R.id.bg_to_remove_setting_voice_dialog_layout)
            val layoutPopup = viewPopup.findViewById<LinearLayout>(R.id.popup_voiceSetting)
            val location = IntArray(2)
            btnVoice.getLocationOnScreen(location)
            val param = layoutPopup.layoutParams as ViewGroup.MarginLayoutParams
            param.setMargins((location[0]+btnVoice.width)-param.width, location[1] - param.height, 0, 0)
            layoutPopup.layoutParams = param

            layoutVoiceStandard.setOnClickListener {
                it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                btnVoice.text = "CHUẨN"
                AppController.voiceType = 1
                btnVoice.background = getDrawable(R.drawable.bg_btn_green_main)
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        1 -> {
                            mAudioPlayer.play(this, R.raw.chao_ban_den_voi_car_map)
                        }
                    }
                }
                mPopupVoiceWindow!!.dismiss()
            }
            layoutVoiceGirl.setOnClickListener {
                it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                btnVoice.text = "NỮ"
                AppController.voiceType = 2
                btnVoice.background = getDrawable(R.drawable.bg_btn_dismiss)
                if (AppController.soundMode == 1) {
                    when (AppController.voiceType) {
                        2 -> {
                            mAudioPlayer.play(this, R.raw.chao_ban_den_voi_car_map_2)
                        }
                    }
                }
                mPopupVoiceWindow!!.dismiss()
            }
            layoutOutside.setOnClickListener {
                it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                mPopupVoiceWindow!!.dismiss()
            }
        }

        btnBack.setOnClickListener {
//            AppController.userProfile!!.vehicle!![0].licensePlate = editLicensePlate.text.toString()
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
