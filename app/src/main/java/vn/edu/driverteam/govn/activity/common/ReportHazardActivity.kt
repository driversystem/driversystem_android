package vn.edu.driverteam.govn.activity.common

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.controllers.AppController
import vn.edu.driverteam.govn.models.Report
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.ReportService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import androidx.core.content.FileProvider
import android.util.Log
import android.view.HapticFeedbackConstants
import android.widget.LinearLayout
import vn.edu.driverteam.govn.services.models.ReportResponse
import vn.edu.driverteam.govn.utils.AudioPlayer
import vn.edu.driverteam.govn.utils.FileUtils
import com.sdsmdg.tastytoast.TastyToast
import com.squareup.picasso.Picasso
import java.io.File
import java.io.IOException


class ReportHazardActivity : AppCompatActivity() {

    @BindView(R.id.txtMess_report_hazard)
    lateinit var textInputEdit: EditText
    @BindView(R.id.btnSend_report_hazard)
    lateinit var btnSend: TextView
    @BindView(R.id.btnClose_report_hazard)
    lateinit var btnCLose: ImageView
    @BindView(R.id.layout_take_photo_report_hazard)
    lateinit var btnTakePhoto: LinearLayout
    @BindView(R.id.tvTakePhoto_report_hazard)
    lateinit var tvTakePhoto: TextView
    @BindView(R.id.over_layout_report_hazard)
    lateinit var layoutReport: RelativeLayout
    @BindView(R.id.imv_photo_report_hazard)
    lateinit var imvPhotoReportHazard: ImageView

    companion object {
        private const val TARGET_IMAGE_WIDTH: Int = 614
        private const val TARGET_IMAGE_HEIGHT: Int = 818
    }

    private var sFileAudioPath: String = ""

    private var sBase64Image: String = ""

    // ==== Dùng cho lấy chất lượng ảnh JPEG gốc, bằng cách chụp xong lưu file ảnh lại
    private lateinit var photoURI: Uri

    // Audio Player
    private var mAudioPlayer = AudioPlayer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_hazard)
        ButterKnife.bind(this)
        initComponents()
    }

    private fun initComponents() {

        btnSend.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            if (mCurrentPhotoPath != "") {
                val oldFile = File(mCurrentPhotoPath)
                oldFile.delete()
            }
            onSend()
        }
        btnCLose.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            if (mCurrentPhotoPath != "") {
                val oldFile = File(mCurrentPhotoPath)
                oldFile.delete()
            }
            onClose()
        }

        btnTakePhoto.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)

            // Xoá ảnh cũ
            if (mCurrentPhotoPath != "") {
                val oldFile = File(mCurrentPhotoPath)
                oldFile.delete()
            }

//             ==== Dùng cho lấy chất lượng ảnh JPEG gốc, bằng cách chụp xong lưu file ảnh lại
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(packageManager) != null) {
                // Create the File where the photo should go
                var photoFile: File? = null
                try {
                    photoFile = createImageFile()
                } catch (e: IOException) {
                    // Error occurred while creating the File
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    photoURI = FileProvider.getUriForFile(this,
                            "vn.edu.driverteam.govn.here.this.library.provider",
                            photoFile)
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, 1)
                }
            }
        }
    }

    private fun onClose() {
        finish()
    }

    private fun onSend() {
        // reimplement here
        if (sFileAudioPath == "" || sBase64Image == "") {
            // Encode file ghi âm
            val encoded = FileUtils.encodeAudioFile(sFileAudioPath)
            val mReport = Report("hazard", textInputEdit.text.toString(), AppController.userProfile!!.currentLocation!!, AppController.userProfile!!._id.toString(), 1, 0, false, encoded, sBase64Image, "")
            onAddNewReportHazard(mReport, false)
        } else {
            val mReport = Report("hazard", textInputEdit.text.toString(), AppController.userProfile!!.currentLocation!!, AppController.userProfile!!._id.toString(), 1, 0, false, "", sBase64Image, "")
            onAddNewReportHazard(mReport, true)
        }
    }

    private fun onAddNewReportHazard(report: Report, bothAudioAndImage: Boolean) {
        val service = APIServiceGenerator.createService(ReportService::class.java)

        val call = service.addNewReport(report)
        call.enqueue(object : Callback<Report> {
            override fun onFailure(call: Call<Report>?, t: Throwable?) {
                TastyToast.makeText(this@ReportHazardActivity, "Gửi báo hiệu thất bại!", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
            }

            override fun onResponse(call: Call<Report>, response: Response<Report>) {
                if (response.isSuccessful) {
                    if (bothAudioAndImage) {
                        // Encode file ghi âm
                        val encoded = FileUtils.encodeAudioFile(sFileAudioPath)
                        onUpdateBase64Voice(response.body()!!._id.toString(), encoded)
                        finish()
                    } else {
                        // Chạy audio
                        if (AppController.soundMode == 1) {
                            when (AppController.voiceType) {
                                1 -> {
                                    mAudioPlayer.play(this@ReportHazardActivity, R.raw.gui_bao_hieu_thanh_cong)
                                }
                                2 -> {
                                    mAudioPlayer.play(this@ReportHazardActivity, R.raw.gui_bao_hieu_thanh_cong_2)
                                }
                            }
                        }
                        TastyToast.makeText(this@ReportHazardActivity, "Gửi báo hiệu thành công!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                        finish()
                    }
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@ReportHazardActivity, "Lỗi: " + apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }
        })
    }

    private fun onUpdateBase64Voice(id: String, base64Voice: String) {
        val service = APIServiceGenerator.createService(ReportService::class.java)
        val call = service.updateBase64Voice(id, base64Voice)
        call.enqueue(object : Callback<ReportResponse> {
            override fun onResponse(call: Call<ReportResponse>, response: Response<ReportResponse>) {
                if (response.isSuccessful) {
                    // Chạy audio
                    if (AppController.soundMode == 1) {
                        when (AppController.voiceType) {
                            1 -> {
                                mAudioPlayer.play(this@ReportHazardActivity, R.raw.ket_xe_vua)
                            }
                            2 -> {
                                mAudioPlayer.play(this@ReportHazardActivity, R.raw.ket_xe_vua_2)
                            }
                        }
                    }
                    TastyToast.makeText(this@ReportHazardActivity, "Gửi báo hiệu thành công!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@ReportHazardActivity, "Lỗi: " + apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<ReportResponse>, t: Throwable) {
                Log.e("Failure", "Error: " + t.message)
            }
        })
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1 -> {
                tvTakePhoto.text = "Chụp ảnh"
                if (resultCode == Activity.RESULT_OK) {
                    tvTakePhoto.text = "Đã chụp ảnh"

                    // Chỉ lấy thumbnail nên chất lượng ảnh không cao
                    // ==== Dùng cho lấy chất lượng ảnh JPEG gốc, bằng cách chụp xong lưu file ảnh lại

                    val options = BitmapFactory.Options()
                    // Số inSampleSize là ảnh mới sẽ bằng 1 / inSampleSize của ảnh gốc, tức chiều dài và rộng giảm đi inSampleSize lần
                    // inJustRebound = true là sẽ đọc resource của ảnh chứ ko laod ảnh vào bộ nhớ, sẽ giảm bộ nhớ sử dụng

                    options.inJustDecodeBounds = true
                    BitmapFactory.decodeFile(mCurrentPhotoPath, options)
                    options.inSampleSize = calculateInSampleSize(options)
                    options.inJustDecodeBounds = false
                    val imageStream = contentResolver.openInputStream(photoURI)
                    Picasso.get().load(photoURI).into(imvPhotoReportHazard)
                    val bitmap = BitmapFactory.decodeStream(imageStream, null, options)

                    val matrix = Matrix()
                    matrix.postRotate(90f)
                    val newBitmap: Bitmap = Bitmap.createBitmap(bitmap!!, 0, 0, bitmap.width, bitmap.height, matrix, true)
                    sBase64Image = if (bitmap.density > 320) {
                        FileUtils.encodeImageFile(newBitmap, "large")
                    } else {
                        FileUtils.encodeImageFile(newBitmap, "normal")
                    }
                }
            }
        }
    }

    // This method is used to calculate largest inSampleSize
    //which is used to decode bitmap in required bitmap.
    private fun calculateInSampleSize(bmOptions: BitmapFactory.Options): Int {
        // Raw height and width of image
        val photoWidth = bmOptions.outWidth
        val photoHeight = bmOptions.outHeight
        var scaleFactor = 2

        // Calculate the largest inSampleSize value that is a power of 2
        //and keeps both height and width larger than the requested height and width.

        // Test and replace with || ( or )
        while ((photoWidth / scaleFactor) >= TARGET_IMAGE_WIDTH && (photoHeight / scaleFactor) >= TARGET_IMAGE_HEIGHT) {

            scaleFactor *= 2
        }
        return scaleFactor
    }

    private var mCurrentPhotoPath: String = ""
    @Throws(IOException::class)
    private fun createImageFile(): File {
        val imageFileName = "image"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )

        Log.e("PATH", image.absolutePath)
//        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (mCurrentPhotoPath != "") {
            val oldFile = File(mCurrentPhotoPath)
            oldFile.delete()
        }
        onClose()
    }
}