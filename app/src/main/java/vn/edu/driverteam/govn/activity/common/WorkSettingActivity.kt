package vn.edu.driverteam.govn.activity.common

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.controllers.AppController
import com.google.android.gms.common.api.Status
import com.sdsmdg.tastytoast.TastyToast

import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment

class WorkSettingActivity : AppCompatActivity() {

    @BindView(R.id.imBack_work_setting)
    lateinit var btnBack: ImageView
    @BindView(R.id.tvAddress_work_setting)
    lateinit var tvAddress: TextView
    @BindView(R.id.btnDismiss_work_setting)
    lateinit var btnDismiss: Button
    @BindView(R.id.btnChoose_work_setting)
    lateinit var btnChoose: Button

    private var isNewChosen = false

    private var chosenPlace: Place? = null

    private lateinit var placesClient:PlacesClient
    private var placeFields = listOf(Place.Field.ID,
            Place.Field.NAME,
            Place.Field.ADDRESS,
            Place.Field.LAT_LNG)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_work_setting)

        ButterKnife.bind(this)
        initComponents()
    }

    private fun initComponents() {

        if (intent.getStringExtra("work_location") != "") {
            tvAddress.text = intent.getStringExtra("work_location")
        }

        // Set up autocomplete search bar
        initPlaces()
        setUpPlacesAutoComplete()

        btnBack.setOnClickListener {
            finish()
        }

        btnDismiss.setOnClickListener {
            finish()
        }

        btnChoose.setOnClickListener {
            if (isNewChosen) {
                AppController.userProfile!!.latWorkLocation = chosenPlace!!.latLng!!.latitude
                AppController.userProfile!!.longWorkLocation = chosenPlace!!.latLng!!.longitude
                intent.putExtra("work_location_new", tvAddress.text.toString())
                setResult(Activity.RESULT_OK, intent)
                finish()
            } else {
                TastyToast.makeText(this, "Vui lòng chọn địa chỉ mới", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }

        }
    }

    private fun initPlaces() {
        Places.initialize(this, getString(R.string.places_api))
        placesClient = Places.createClient(this)
    }

    private fun setUpPlacesAutoComplete() {
        val autoCompleteFragment = supportFragmentManager.findFragmentById(R.id.place_autocomplete_work_setting) as AutocompleteSupportFragment
        autoCompleteFragment.setPlaceFields(placeFields)

        autoCompleteFragment.setOnPlaceSelectedListener(object:com.google.android.libraries.places.widget.listener.PlaceSelectionListener{
            override fun onPlaceSelected(p0: Place) {
                Toast.makeText(this@WorkSettingActivity, ""+p0.address, Toast.LENGTH_SHORT).show()

                tvAddress.text = p0.address
                chosenPlace = p0
                isNewChosen = true
            }

            override fun onError(p0: Status) {
                Toast.makeText(this@WorkSettingActivity, ""+p0.statusMessage, Toast.LENGTH_SHORT).show()
            }
        })
    }
}