package vn.edu.driverteam.govn.models.group

import android.app.Activity
import android.content.Context
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import vn.edu.driverteam.govn.R
import com.squareup.picasso.Picasso

class ImageAdapter(private val groupPhotos: MutableList<String>,private val context: Context) : BaseAdapter() {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)

        val itemHeight = displayMetrics.widthPixels/3
        val imageView = ImageView(context)

        return if(groupPhotos[position] != ""){
            Picasso.get().load(groupPhotos[position]).into(imageView)
            imageView.scaleType = ImageView.ScaleType.CENTER_CROP
            imageView.layoutParams = ViewGroup.LayoutParams(-1, itemHeight)
            imageView
        } else {
            val paddingInDp: Int = 25
            val scale = context.resources.displayMetrics.density
            val paddingInPx = (paddingInDp * scale + 0.5f).toInt()

            imageView.setImageResource(R.drawable.ic_add_black_24dp)
            imageView.setPadding(paddingInPx, paddingInPx, paddingInPx, paddingInPx)
            imageView.scaleType = ImageView.ScaleType.CENTER_CROP
            imageView.layoutParams = ViewGroup.LayoutParams(-1, itemHeight)
            imageView
        }
    }

    override fun getItem(position: Int): Any {
        return groupPhotos[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return groupPhotos.size
    }

}