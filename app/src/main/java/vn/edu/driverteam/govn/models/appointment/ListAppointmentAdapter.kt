package vn.edu.driverteam.govn.models.appointment

import android.annotation.SuppressLint
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.models.Appointment
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*

class ListAppointmentAdapter(private val myListAppointment: MutableList<Appointment>, val clickListener: (Appointment) -> Unit)
    : RecyclerView.Adapter<ListAppointmentAdapter.ViewHolder>(){
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycler_view_list_appointment_item, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return myListAppointment.size
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val appointment = myListAppointment[position]

        val inputFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val date: LocalDateTime = LocalDateTime.parse((appointment.time as String), inputFormatter)

        val appointmentTime = Date.from(date.atOffset(ZoneOffset.UTC).toInstant())
        val remainingTimeLong = (appointmentTime.time.minus(Date().time))

        val month = holder.view.findViewById<TextView>(R.id.tv_month)
        val dayOfWeek = holder.view.findViewById<TextView>(R.id.tv_dayOfWeek)
        val dayOfMonth = holder.view.findViewById<TextView>(R.id.tv_dayOfMonth)
        val time = holder.view.findViewById<TextView>(R.id.tv_time)
        val address = holder.view.findViewById<TextView>(R.id.tv_address)
        val locationName = holder.view.findViewById<TextView>(R.id.tv_locationName)
        val remainingTime = holder.view.findViewById<TextView>(R.id.tv_remaining_time)

        month.text = (appointmentTime.toInstant().atZone(ZoneId.systemDefault())).month.toString()
        dayOfWeek.text = (appointmentTime.toInstant().atZone(ZoneId.systemDefault())).dayOfWeek.toString()
        dayOfMonth.text = (appointmentTime.toInstant().atZone(ZoneId.systemDefault())).dayOfMonth.toString()
        time.text = (appointmentTime.toInstant().atZone(ZoneId.systemDefault())).hour.toString() + ":" + (appointmentTime.toInstant().atZone(ZoneId.systemDefault())).minute.toString()
        address.text = myListAppointment[position].locationAddress
        locationName.text = myListAppointment[position].locationName
        if(remainingTimeLong > 0) {
            val remainingTimeDate = Date(remainingTimeLong).toInstant().atZone(ZoneId.systemDefault())
            remainingTime.text = "Còn " + remainingTimeDate.dayOfYear.toString() + " ngày"
        } else {
            remainingTime.text = "Quá hạn"
        }

        holder.view.setOnClickListener{ clickListener(myListAppointment[position])}
    }
}