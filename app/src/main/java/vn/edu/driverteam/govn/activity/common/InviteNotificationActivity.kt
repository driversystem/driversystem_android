package vn.edu.driverteam.govn.activity.common

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import butterknife.ButterKnife
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.models.group.ListInviteNotificationAdapter
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.UserService
import vn.edu.driverteam.govn.services.models.GroupResponse
import vn.edu.driverteam.govn.services.models.SuccessResponse
import com.sdsmdg.tastytoast.TastyToast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InviteNotificationActivity : AppCompatActivity() {
    @BindView(R.id.imBack_inviteNotification)
    lateinit var btnBack: LinearLayout
    @BindView(R.id.recycler_view_list_invitation)
    lateinit var recyclerViewListInvitation: RecyclerView
    @BindView(R.id.swipeRefresh_inviteNotification)
    lateinit var swipeContainer: SwipeRefreshLayout

    private lateinit var mAdapter : ListInviteNotificationAdapter
    private var loadingAlert: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invite_notification)
        ButterKnife.bind(this)

        initComponents()
    }

    private fun initComponents() {
        btnBack.setOnClickListener {
            finish()
        }
        swipeContainer.setOnRefreshListener {
            reloadInvitation()
        }
        loadInvitation()
    }

    private fun reloadInvitation () {
        val service = APIServiceGenerator.createService(UserService::class.java)
        val call = service.allInvitation

        call.enqueue(object : Callback<GroupResponse>{
            override fun onResponse(call: Call<GroupResponse>, response: Response<GroupResponse>) {
                if(response.isSuccessful){
                    swipeContainer.isRefreshing = false
                    mAdapter = ListInviteNotificationAdapter(response.body()!!.invitations!!, this@InviteNotificationActivity){
                        val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                            when(which){
                                DialogInterface.BUTTON_POSITIVE -> {
                                    onAccept(it._id)
                                }
                                DialogInterface.BUTTON_NEGATIVE -> {
                                    onDeny(it._id)
                                }
                            }
                        }
                        val builder = AlertDialog.Builder(this@InviteNotificationActivity)
                        builder.setMessage("Bạn có muốn vào nhóm này không?")
                                .setPositiveButton("Có", dialogClickListener)
                                .setNegativeButton("Không", dialogClickListener).show()
                    }

                    recyclerViewListInvitation.apply {
                        layoutManager = LinearLayoutManager(this@InviteNotificationActivity)
                        adapter = mAdapter
                    }

                } else {
                    swipeContainer.isRefreshing = false
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@InviteNotificationActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<GroupResponse>, t: Throwable) {
                swipeContainer.isRefreshing = false
                TastyToast.makeText(this@InviteNotificationActivity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }

        })
    }

    private fun loadInvitation() {
        showLoadingDialog()
        val service = APIServiceGenerator.createService(UserService::class.java)
        val call = service.allInvitation

        call.enqueue(object : Callback<GroupResponse>{
            override fun onResponse(call: Call<GroupResponse>, response: Response<GroupResponse>) {
                if(response.isSuccessful){
                    hideLoadingDialog()
                    mAdapter = ListInviteNotificationAdapter(response.body()!!.invitations!!, this@InviteNotificationActivity) {
                        val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                            when(which){
                                DialogInterface.BUTTON_POSITIVE -> {
                                    onAccept(it._id)
                                }
                                DialogInterface.BUTTON_NEGATIVE -> {
                                    onDeny(it._id)
                                }
                            }
                        }
                        val builder = AlertDialog.Builder(this@InviteNotificationActivity)
                        builder.setMessage("Bạn có muốn vào nhóm này không?")
                                .setPositiveButton("Có", dialogClickListener)
                                .setNegativeButton("Không", dialogClickListener).show()
                    }
                    recyclerViewListInvitation.apply {
                        layoutManager = LinearLayoutManager(this@InviteNotificationActivity)
                        adapter = mAdapter
                    }

                } else {
                    hideLoadingDialog()
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@InviteNotificationActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<GroupResponse>, t: Throwable) {
                hideLoadingDialog()
                TastyToast.makeText(this@InviteNotificationActivity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }

        })
    }



    private fun onAccept(groupId: String?) {
        val service = APIServiceGenerator.createService(UserService::class.java)
        val call = service.acceptInvite(groupId)

        call.enqueue(object : Callback<SuccessResponse>{
            override fun onResponse(call: Call<SuccessResponse>, response: Response<SuccessResponse>) {
                if(response.isSuccessful) {
                    TastyToast.makeText(this@InviteNotificationActivity, response.body()!!.message, TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                    reloadActivity()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@InviteNotificationActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                    reloadActivity()
                }
            }

            override fun onFailure(call: Call<SuccessResponse>, t: Throwable) {
                TastyToast.makeText(this@InviteNotificationActivity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
                reloadActivity()
            }

        })
    }

    private fun onDeny(groupId: String?) {
        val service = APIServiceGenerator.createService(UserService::class.java)
        val call = service.denyInvite(groupId)

        call.enqueue(object : Callback<SuccessResponse>{
            override fun onResponse(call: Call<SuccessResponse>, response: Response<SuccessResponse>) {
                if(response.isSuccessful) {
                    TastyToast.makeText(this@InviteNotificationActivity, response.body()!!.message, TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                    reloadActivity()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@InviteNotificationActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                    reloadActivity()
                }
            }

            override fun onFailure(call: Call<SuccessResponse>, t: Throwable) {
                TastyToast.makeText(this@InviteNotificationActivity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
                reloadActivity()
            }
        })
    }

    private fun reloadActivity() {
        this.finish()
        this.overridePendingTransition(0, 0)
        this.startActivity(this.intent)
        this.overridePendingTransition(0, 0)
    }

    private fun showLoadingDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setView(R.layout.layout_loading_dialog)
        loadingAlert = builder.create()
        loadingAlert?.show()
    }

    private fun hideLoadingDialog() {
        loadingAlert?.dismiss()
    }
}
