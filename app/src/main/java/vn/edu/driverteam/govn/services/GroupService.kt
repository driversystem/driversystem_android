package vn.edu.driverteam.govn.services

import vn.edu.driverteam.govn.models.Group
import vn.edu.driverteam.govn.services.models.GroupResponse
import vn.edu.driverteam.govn.services.models.SuccessResponse
import retrofit2.Call
import retrofit2.http.*

interface GroupService {
    @POST("group")
    fun createGroup(@Body group: Group): Call<GroupResponse>

    @GET("group/{id}")
    fun getGroupById(@Path("id") id: String): Call<GroupResponse>

    @DELETE("group/{id}")
    fun deleteGroupById(@Path("id") id: String): Call<SuccessResponse>

    @PUT("group/{id}/invite")
    fun inviteMember(@Path("id") groupId: String, @Query("id") memberId: String): Call<SuccessResponse>

    @PUT("group/{id}/moderator/remove")
    fun removeModerator(@Path("id") groupId: String, @Query("id") removedMemberId: String): Call<GroupResponse>

    @PUT("group/{id}/member/remove")
    fun removeMember(@Path("id") groupId: String, @Query("id") removedMemberId: String): Call<GroupResponse>

    @PUT("group/{id}/moderator/add")
    fun addModerator(@Path("id") groupId: String, @Query("id") newModId: String): Call<GroupResponse>

    @PUT("group/{id}/moderator/derole")
    fun deroleModerator(@Path("id") groupId: String, @Query("id") newModId: String): Call<GroupResponse>

    @POST("group/{groupID}/leave")
    fun leaveGroup(@Path("groupID") groupsId: String): Call<SuccessResponse>

    @POST("group/{groupID}/profile/picture")
    fun updateAvatar(@Path("groupID") groupId: String, @Body avatar: Group): Call<SuccessResponse>

    @PUT("group/{id}/photo/add")
    fun addPhoto(@Path("id") groupId: String, @Body photos: Group): Call<SuccessResponse>
}