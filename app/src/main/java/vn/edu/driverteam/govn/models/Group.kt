package vn.edu.driverteam.govn.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Group(groupName: String, groupModerators: List<User>, members: List<User>, description: String, avatar: String, photos: List<String>) : Serializable {
    @SerializedName("_id")
    @Expose
    var _id: String? = null
    @SerializedName("groupName")
    @Expose
    var groupName: String? = groupName
    @SerializedName("groupOwner")
    @Expose
    var groupOwner: User? = null
    @SerializedName("description")
    @Expose
    var description: String? = description
    @SerializedName("groupModerators")
    @Expose
    var groupModerators: List<User>? = groupModerators
    @SerializedName("members")
    @Expose
    var members: List<User>? = members
    @SerializedName("appointments")
    @Expose
    var appointments: List<Appointment>? = null
    @SerializedName("avatar")
    @Expose
    var avatar: String? = avatar
    @SerializedName("photos")
    @Expose
    var photos: List<String>? = photos

}