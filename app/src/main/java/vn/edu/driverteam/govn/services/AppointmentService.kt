package vn.edu.driverteam.govn.services

import vn.edu.driverteam.govn.models.Appointment
import vn.edu.driverteam.govn.services.models.AppointmentResponse
import vn.edu.driverteam.govn.services.models.SuccessResponse
import vn.edu.driverteam.govn.services.models.UserProfileResponse
import retrofit2.Call
import retrofit2.http.*

interface AppointmentService {
    @POST("appointment")
    fun createAppointment(@Body appointment: Appointment): Call<SuccessResponse>

    @GET("appointment/{id}")
    fun getAppointmentById(@Path("id")appointmentId: String): Call<AppointmentResponse>

    @POST("appointment/{id}/accept")
    fun joinAppointment(@Path("id") appointmentId: String, @Body appointment: Appointment): Call<AppointmentResponse>

    @POST("appointment/{id}/withdraw")
    fun withdrawAppointment(@Path("id") appointmentId: String, @Body appointment: Appointment): Call<AppointmentResponse>

    @DELETE("appointment/{id}")
    fun deleteAppointment(@Path("id") appointmentId: String): Call<AppointmentResponse>

    @GET("appointment/{id}/participants/location")
    fun participantsLocation(@Path("id") appointmentId: String): Call<UserProfileResponse>
    
}