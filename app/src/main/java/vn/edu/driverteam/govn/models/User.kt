package vn.edu.driverteam.govn.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class User() : Parcelable {
    @SerializedName("_id")
    @Expose
    var _id: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("role")
    @Expose
    var role: String? = null
    @SerializedName("location")
    @Expose
    var location: String? = null
    @SerializedName("avatar")
    @Expose
    var avatar: String? = null
    @SerializedName("birthDate")
    @Expose
    var birthDate: String? = null
    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null
    @SerializedName("socketID")
    @Expose
    var socketID: String? = null
    @SerializedName("currentLocation")
    @Expose
    var currentLocation: Geometry? = null
    @SerializedName("latHomeLocation")
    @Expose
    var latHomeLocation: Double? = null
    @SerializedName("longHomeLocation")
    @Expose
    var longHomeLocation: Double? = null
    @SerializedName("latWorkLocation")
    @Expose
    var latWorkLocation: Double? = null
    @SerializedName("longWorkLocation")
    @Expose
    var longWorkLocation: Double? = null
    @SerializedName("invisible")
    @Expose
    var invisible: Boolean? = null
    @SerializedName("phoneNumber")
    @Expose
    var phoneNumber: String? = null
    @SerializedName("vehicle")
    @Expose
    var vehicle: List<String>? = null
    @SerializedName("groups")
    @Expose
    var groups: List<String>? = null
    @SerializedName("collections")
    @Expose
    var collections: List<String>? = null
    @SerializedName("invitations")
    @Expose
    var invitations: List<String>?  = null
    @SerializedName("authProvider")
    @Expose
    var authProvider: String?  = null

    constructor(parcel: Parcel) : this() {
        _id = parcel.readString()
        email = parcel.readString()
        name = parcel.readString()
        role = parcel.readString()
        location = parcel.readString()
        avatar = parcel.readString()
        birthDate = parcel.readString()
        createdAt = parcel.readString()
        socketID = parcel.readString()
        authProvider = parcel.readString()
        latHomeLocation = parcel.readValue(Double::class.java.classLoader) as? Double
        longHomeLocation = parcel.readValue(Double::class.java.classLoader) as? Double
        latWorkLocation = parcel.readValue(Double::class.java.classLoader) as? Double
        longWorkLocation = parcel.readValue(Double::class.java.classLoader) as? Double
        invisible = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        phoneNumber = parcel.readString()
        vehicle = parcel.createStringArrayList()
        groups = parcel.createStringArrayList()
        collections = parcel.createStringArrayList()
        invitations = parcel.createStringArrayList()
    }

    constructor(avatar: String) : this () {
        this.avatar = avatar
    }
    constructor(email: String, name: String, avatar: String, birthDate: String,
                createdAt: String, socketID: String, currentLocation: Geometry, latHomeLocation: Double,
                longHomeLocation: Double, latWorkLocation: Double, longWorkLocation: Double, invisible: Boolean?,
                phoneNumber: String, vehicle: List<String>?, groups: List<String>, collections: List<String>, role: String) : this() {
        this.email = email
        this.name = name
        this.avatar = avatar
        this.birthDate = birthDate
        this.createdAt = createdAt
        this.socketID = socketID
        this.currentLocation = currentLocation
        this.latHomeLocation = latHomeLocation
        this.longHomeLocation = longHomeLocation
        this.latWorkLocation = latWorkLocation
        this.longWorkLocation = longWorkLocation
        this.invisible = invisible
        this.phoneNumber = phoneNumber
        this.vehicle = vehicle
        this.groups = groups
        this.collections = collections
        this.role = role
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_id)
        parcel.writeString(email)
        parcel.writeString(name)
        parcel.writeString(role)
        parcel.writeString(location)
        parcel.writeString(avatar)
        parcel.writeString(birthDate)
        parcel.writeString(createdAt)
        parcel.writeString(socketID)
        parcel.writeValue(latHomeLocation)
        parcel.writeValue(longHomeLocation)
        parcel.writeValue(latWorkLocation)
        parcel.writeValue(longWorkLocation)
        parcel.writeValue(invisible)
        parcel.writeString(phoneNumber)
        parcel.writeStringList(vehicle)
        parcel.writeStringList(groups)
        parcel.writeStringList(collections)
        parcel.writeStringList(invitations)
        parcel.writeString(authProvider)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}