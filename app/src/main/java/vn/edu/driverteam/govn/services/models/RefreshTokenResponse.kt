package vn.edu.driverteam.govn.services.models

class RefreshTokenResponse {
    var success: Boolean? = null
    var message: String? = null
    var token: String? = null
}
