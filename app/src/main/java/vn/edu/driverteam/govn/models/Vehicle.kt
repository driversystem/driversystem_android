package vn.edu.driverteam.govn.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Vehicle {
    @SerializedName("_id")
    @Expose
    var _id: String? = null
    @SerializedName("vehicleType")
    @Expose
    var vehicleType: String? = null
    @SerializedName("vehicleModel")
    @Expose
    var vehicleModel: String? = null
    @SerializedName("color")
    @Expose
    var color: String? = null
    @SerializedName("licensePlate")
    @Expose
    var licensePlate: String? = null

    constructor(vehicleType: String, vehicleModel: String, color: String, licensePlate: String){
        this.vehicleType = vehicleType
        this.vehicleModel = vehicleModel
        this.color = color
        this.licensePlate = licensePlate
    }
}