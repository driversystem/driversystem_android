package vn.edu.driverteam.govn.activity.common.ui.viewpage.groupdetail

import android.annotation.SuppressLint
import androidx.appcompat.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.activity.common.SearchUserActivity
import vn.edu.driverteam.govn.controllers.AppController
import vn.edu.driverteam.govn.models.User
import vn.edu.driverteam.govn.models.user.ListMemberAdapter
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.GroupService
import vn.edu.driverteam.govn.services.models.GroupResponse
import com.sdsmdg.tastytoast.TastyToast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val GROUP_ID = "groupId"
private const val GROUP_MODERATORS = "groupModerators"
private const val GROUP_OWNER = "groupOwner"
private const val GROUP_MEMBERS = "groupMembers"
/**
 * A simple [Fragment] subclass.
 * Use the [MembersFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MembersFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var groupModerators: List<User>? = null
    private var groupMembers: List<User>? = null
    private var groupOwner: User? = null
    private var groupId: String? = null
    private lateinit var recyclerViewListMember: RecyclerView
    private lateinit var btnInviteMembers: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            groupId = it.getString(GROUP_ID)
            groupModerators = it.getParcelableArrayList(GROUP_MODERATORS)
            groupMembers = it.getParcelableArrayList(GROUP_MEMBERS)
            groupOwner = it.getParcelable(GROUP_OWNER)
            }
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_members, container, false)
        initComponent(view)
        return view
    }

    private fun initComponent(view: View) {
        recyclerViewListMember = view.findViewById(R.id.list_member_groupDetail)
        btnInviteMembers = view.findViewById(R.id.layout_inviteMembers)

        btnInviteMembers.setOnClickListener {
            showSearchUser()
        }

        loadListMember()
    }

    @SuppressLint("ResourceType")
    private fun showSearchUser() {
        val intent = Intent(activity, SearchUserActivity::class.java)
        intent.putExtra("group_id", groupId)
        startActivity(intent)
    }

    private fun loadListMember() {
        val listMember = ArrayList<User>()
        val listRole = ArrayList<String>()
        listMember.add(groupOwner!!)
        listMember.addAll(groupModerators!!)
        listMember.addAll(groupMembers!!)
        listRole.add("Chủ")
        for (i in groupModerators!!.indices){
            listRole.add("Quản trị viên")
        }
        for (i in groupMembers!!.indices){
            listRole.add("")
        }

        recyclerViewListMember.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = ListMemberAdapter(listMember as MutableList<User>, listRole) {
                val callURI = "tel:" + it.phoneNumber
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse(callURI)
                startActivity(intent)
            }
            initSwipeToDeleteRecyclerView(listMember, listRole)
        }
    }

    private fun initSwipeToDeleteRecyclerView(listMember : ArrayList<User>, listRole : ArrayList<String>){
        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT){
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun isItemViewSwipeEnabled(): Boolean {
                return !isGroupMember(AppController.userProfile!!)
            }

            override fun getSwipeDirs(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
                if(viewHolder.adapterPosition == 0) return 0
                if(isGroupModerator(AppController.userProfile!!)){
                    if(listRole[viewHolder.adapterPosition] == "Quản trị viên"){
                        return 0
                    }
                }
                return super.getSwipeDirs(recyclerView, viewHolder)
            }

            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                val view = viewHolder.itemView
                val background = ColorDrawable()
                val deleteIcon = ContextCompat.getDrawable(context!!, R.drawable.ic_delete_white_24dp)
                val modIcon = ContextCompat.getDrawable(context!!, R.drawable.ic_update_moderator_white_36dp)
                if(dX > 0) {
                    val margin = ((view.bottom - view.top)/2 - modIcon?.minimumHeight!!/2)
                    background.color = 0xFF3345E8.toInt()
                    modIcon.setBounds(view.left + 50, view.top + margin, modIcon.minimumWidth + 50, view.bottom - margin)
                    background.setBounds(view.left, view.top, dX.toInt(), view.bottom)
                    background.draw(c)
                    modIcon.draw(c)
                } else if(dX < 0) {
                    val margin = ((view.bottom - view.top)/2 - deleteIcon?.minimumHeight!!/2)
                    background.color = Color.RED
                    deleteIcon.setBounds(view.right-deleteIcon.minimumWidth - 50, view.top + margin, view.right - 50, view.bottom - margin)
                    background.setBounds(view.right+dX.toInt(), view.top, view.right, view.bottom)
                    background.draw(c)
                    deleteIcon.draw(c)
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                if(direction == ItemTouchHelper.LEFT){
                    val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                        when(which){
                            DialogInterface.BUTTON_POSITIVE -> {
                                onDelete(listMember[viewHolder.adapterPosition])
                            }
                            DialogInterface.BUTTON_NEGATIVE -> {
                                loadListMember()
                            }
                        }
                    }
                    val builder = AlertDialog.Builder(context!!)
                    builder.setMessage("Bạn có chắc muốn xóa thành viên này không?")
                            .setPositiveButton("Có", dialogClickListener)
                            .setNegativeButton("Không", dialogClickListener).show()
                } else if(direction == ItemTouchHelper.RIGHT) {
                    if(isGroupMember(listMember[viewHolder.adapterPosition])){
                        val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                            when(which){
                                DialogInterface.BUTTON_POSITIVE -> {
                                    onAddModerator(listMember[viewHolder.adapterPosition]._id!!)
                                }
                                DialogInterface.BUTTON_NEGATIVE -> {
                                    loadListMember()
                                }
                            }
                        }
                        val builder = AlertDialog.Builder(context!!)
                        builder.setMessage("Bạn có chắc muốn thăng cấp thành viên này thành quản trị viên không?")
                                .setPositiveButton("Có", dialogClickListener)
                                .setNegativeButton("Không", dialogClickListener).show()
                    }
                    if(isGroupModerator(listMember[viewHolder.adapterPosition])) {
                        onDeroleModerator(listMember[viewHolder.adapterPosition]._id!!)
                    }
                }
            }
        }
        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(recyclerViewListMember)
    }

    private fun onDelete(user: User) {
        if(groupOwner!!._id == AppController.userProfile!!._id) {
            if(isGroupModerator(user)){
                onDeleteModerator(user._id!!)
            }
            if(isGroupMember(user)) {
                onDeleteMember(user._id!!)
            }
        } else if(isGroupModerator(AppController.userProfile!!)) {
            onDeleteMember(user._id!!)
        }
    }

    private fun onAddModerator(userId: String) {
        val service = APIServiceGenerator.createService(GroupService::class.java)
        val call = service.addModerator(groupId!!, userId)

        call.enqueue(object : Callback<GroupResponse> {
            override fun onResponse(call: Call<GroupResponse>, response: Response<GroupResponse>) {
                if(response.isSuccessful) {
                    val group = response.body()!!.group!!
                    groupModerators = group.groupModerators
                    groupMembers = group.members
                    loadListMember()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(activity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                    loadListMember()
                }
            }

            override fun onFailure(call: Call<GroupResponse>, t: Throwable) {
                TastyToast.makeText(activity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
                loadListMember()
            }

        })
    }

    private fun onDeroleModerator(userId: String) {
        val service = APIServiceGenerator.createService(GroupService::class.java)
        val call = service.deroleModerator(groupId!!, userId)

        call.enqueue(object : Callback<GroupResponse> {
            override fun onResponse(call: Call<GroupResponse>, response: Response<GroupResponse>) {
                if(response.isSuccessful) {
                    val group = response.body()!!.group!!
                    groupModerators = group.groupModerators
                    groupMembers = group.members
                    loadListMember()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(activity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<GroupResponse>, t: Throwable) {
                TastyToast.makeText(activity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }

        })
    }

    private fun onDeleteMember(userId: String) {
        val service = APIServiceGenerator.createService(GroupService::class.java)
        val call = service.removeMember(groupId!!, userId)

        call.enqueue(object : Callback<GroupResponse> {
            override fun onResponse(call: Call<GroupResponse>, response: Response<GroupResponse>) {
                if(response.isSuccessful) {
                    val group = response.body()!!.group!!
                    groupModerators = group.groupModerators
                    groupMembers = group.members
                    loadListMember()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(activity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                    loadListMember()
                }
            }

            override fun onFailure(call: Call<GroupResponse>, t: Throwable) {
                TastyToast.makeText(activity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
                loadListMember()
            }

        })
    }

    private fun onDeleteModerator(userId: String) {
        val service = APIServiceGenerator.createService(GroupService::class.java)
        val call = service.removeModerator(groupId!!, userId)

        call.enqueue(object : Callback<GroupResponse>{
            override fun onResponse(call: Call<GroupResponse>, response: Response<GroupResponse>) {
                if(response.isSuccessful) {
                    val group = response.body()!!.group!!
                    groupModerators = group.groupModerators
                    groupMembers = group.members
                    loadListMember()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(activity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<GroupResponse>, t: Throwable) {
                TastyToast.makeText(activity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }
        })
    }

    private fun isGroupMember(input: User) : Boolean {
        groupMembers!!.forEach { user ->
            if (user._id == input._id) {
                return true
            }
        }
        return false
    }

    private fun isGroupModerator(input: User) : Boolean {
        groupModerators!!.forEach { user ->
            if (user._id == input._id) {
                return true
            }
        }
        return false
    }

    companion object {
        @JvmStatic fun newInstance(groupId: String?, groupModerators: ArrayList<User>, groupMembers: ArrayList<User>, groupOwner: User) =
                MembersFragment().apply {
                    arguments = Bundle().apply {
                        putString(GROUP_ID, groupId)
                        putParcelableArrayList(GROUP_MODERATORS, groupModerators)
                        putParcelableArrayList(GROUP_MEMBERS, groupMembers)
                        putParcelable(GROUP_OWNER, groupOwner)
                }
            }
    }
}
