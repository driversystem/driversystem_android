package vn.edu.driverteam.govn.activity.common.ui.viewpage.groupappointment

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.activity.common.CreateGroupActivity
import vn.edu.driverteam.govn.activity.common.GroupDetailActivity
import vn.edu.driverteam.govn.models.Group
import vn.edu.driverteam.govn.models.group.ListGroupAdapter
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.GroupService
import vn.edu.driverteam.govn.services.UserService
import vn.edu.driverteam.govn.services.models.GroupResponse
import vn.edu.driverteam.govn.services.models.SuccessResponse
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.sdsmdg.tastytoast.TastyToast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ListGroupFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ListGroupFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var btnAddGroup: FloatingActionButton
    private lateinit var recyclerViewListGroup: RecyclerView
    private lateinit var mAdapter : ListGroupAdapter
    private lateinit var swipeContainer: SwipeRefreshLayout
    private var loadingAlert: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_list_group, container, false)
        initComponent(view)
        return view
    }

    private fun initComponent(view: View) {
        btnAddGroup = view.findViewById(R.id.floatingAddGroupButton)
        recyclerViewListGroup = view.findViewById(R.id.list_group)
        swipeContainer = view.findViewById(R.id.swipeRefresh)

        btnAddGroup.setOnClickListener {
            val intent = Intent(activity, CreateGroupActivity::class.java)
            startActivity(intent)
        }
        swipeContainer.setOnRefreshListener {
            val service = APIServiceGenerator.createService(UserService::class.java)
            val call = service.allUserGroup
            call.enqueue(object : Callback<GroupResponse> {
                @SuppressLint("ShowToast")
                override fun onResponse(call: Call<GroupResponse>, response: Response<GroupResponse>) {
                    if(response.isSuccessful) {
                        swipeContainer.isRefreshing = false
                        mAdapter = ListGroupAdapter(response.body()!!.groups!!) { group: Group ->
                            run {
                                val intent = Intent(activity, GroupDetailActivity::class.java)
                                intent.putExtra("group_id", group._id)
                                startActivity(intent)
                            }
                        }
                        recyclerViewListGroup.apply {
                            layoutManager = LinearLayoutManager(activity)
                            adapter = mAdapter
                        }
                        initSwipeToDeleteRecyclerView()
                    } else {
                        swipeContainer.isRefreshing = false
                        val apiError = ErrorUtils.parseError(response)
                        TastyToast.makeText(activity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                    }
                }

                override fun onFailure(call: Call<GroupResponse>, t: Throwable) {
                    swipeContainer.isRefreshing = false
                    Log.d("DEBUG", "LỖI GÌ RỒI")
                }
            })
        }
    }

    private fun loadAllUserGroup() {
        showLoadingDialog()
        val service = APIServiceGenerator.createService(UserService::class.java)
        val call = service.allUserGroup
        call.enqueue(object : Callback<GroupResponse> {
            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<GroupResponse>, response: Response<GroupResponse>) {
                if(response.isSuccessful) {
                    hideLoadingDialog()
                    swipeContainer.isRefreshing = false
                    mAdapter = ListGroupAdapter(response.body()!!.groups!!) { group: Group ->
                        run {
                            val intent = Intent(activity, GroupDetailActivity::class.java)
                            intent.putExtra("group_id", group._id)
                            startActivity(intent)
                        }
                    }
                    recyclerViewListGroup.apply {
                        layoutManager = LinearLayoutManager(activity)
                        adapter = mAdapter
                    }
                    initSwipeToDeleteRecyclerView()
                } else {
                    hideLoadingDialog()
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(activity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<GroupResponse>, t: Throwable) {
                hideLoadingDialog()
                Log.d("DEBUG", t.message!!)
            }
        })
    }

    private fun initSwipeToDeleteRecyclerView(){
        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT){
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                val view = viewHolder.itemView
                val background = ColorDrawable(Color.RED)
                val deleteIcon = ContextCompat.getDrawable(context!!, R.drawable.ic_delete_white_24dp)
                background.setBounds(view.right+dX.toInt(), view.top, view.right, view.bottom)
                deleteIcon!!.setBounds(view.right-deleteIcon.minimumWidth - 50, view.top + 120, view.right - 50, view.top + 120 + deleteIcon.minimumHeight)
                background.draw(c)
                deleteIcon.draw(c)
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                    when(which){
                        DialogInterface.BUTTON_POSITIVE -> {
                            mAdapter.getGroupId(viewHolder.adapterPosition)?.let { deleteGroup(it) }
                            mAdapter.notifyDataSetChanged()
                        }
                        DialogInterface.BUTTON_NEGATIVE -> {
                            mAdapter.notifyDataSetChanged()
                        }
                    }
                }
                val builder = AlertDialog.Builder(context!!)
                builder.setMessage("Bạn có chắc muốn xóa nhóm này không?")
                    .setPositiveButton("Có", dialogClickListener)
                    .setNegativeButton("Không", dialogClickListener).show()
            }
        }
        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(recyclerViewListGroup)
    }

    private fun deleteGroup(groupId: String) {
        val service = APIServiceGenerator.createService(GroupService::class.java)
        val call = service.deleteGroupById(groupId)
        call.enqueue(object : Callback<SuccessResponse>{
            override fun onResponse(call: Call<SuccessResponse>, response: Response<SuccessResponse>) {
                if(response.isSuccessful) {
                    TastyToast.makeText(activity, "Xóa nhóm thành công", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                    loadAllUserGroup()
                }
                else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(activity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                    loadAllUserGroup()
                }
            }

            override fun onFailure(call: Call<SuccessResponse>, t: Throwable) {
                TastyToast.makeText(activity, t.message, TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                loadAllUserGroup()
            }

        })
    }

    override fun onResume() {
        super.onResume()
        loadAllUserGroup()
    }

    private fun showLoadingDialog() {
        val builder = AlertDialog.Builder(this.context!!)
        builder.setCancelable(false)
        builder.setView(R.layout.layout_loading_dialog)
        loadingAlert = builder.create()
        loadingAlert?.show()
    }

    private fun hideLoadingDialog() {
        loadingAlert?.dismiss()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ListGroupFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ListGroupFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
