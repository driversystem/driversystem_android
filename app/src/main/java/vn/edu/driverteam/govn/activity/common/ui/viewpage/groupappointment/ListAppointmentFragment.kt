package vn.edu.driverteam.govn.activity.common.ui.viewpage.groupappointment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.activity.common.AppointmentDetailActivity
import vn.edu.driverteam.govn.models.Appointment
import vn.edu.driverteam.govn.models.appointment.ListAppointmentAdapter
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.UserService
import vn.edu.driverteam.govn.services.models.AppointmentResponse
import com.sdsmdg.tastytoast.TastyToast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ListAppointmentFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ListAppointmentFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var recyclerViewListAppointment: RecyclerView
    private lateinit var mAdapter : ListAppointmentAdapter
    private lateinit var swipeContainer: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        loadAllUserAppointment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list_appointment, container, false)
        initComponents(view)
        return view
    }

    private fun initComponents(view: View) {
        recyclerViewListAppointment = view.findViewById(R.id.list_appointment)
        swipeContainer = view.findViewById(R.id.swipeRefresh_listAppointment)

        swipeContainer.setOnRefreshListener {
            loadAllUserAppointment()
        }
    }

    private fun loadAllUserAppointment() {
        val service = APIServiceGenerator.createService(UserService::class.java)
        val call = service.allUserAppointment

        call.enqueue(object : Callback<AppointmentResponse> {
            override fun onResponse(call: Call<AppointmentResponse>, response: Response<AppointmentResponse>) {
                if(response.isSuccessful) {
                    swipeContainer.isRefreshing = false
                    mAdapter = ListAppointmentAdapter(response.body()!!.appointments!! as MutableList<Appointment>) { appointment: Appointment ->
                        val intent = Intent(activity, AppointmentDetailActivity::class.java)
                        intent.putExtra("appointment_id", appointment._id)
                        startActivity(intent)
                    }
                    recyclerViewListAppointment.apply {
                        layoutManager = LinearLayoutManager(activity)
                        adapter = mAdapter
                    }
                } else {
                    swipeContainer.isRefreshing = false
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(activity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<AppointmentResponse>, t: Throwable) {
                swipeContainer.isRefreshing = false
                TastyToast.makeText(activity, t.message, TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }

        })
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ListAppointmentFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
