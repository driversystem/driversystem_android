package vn.edu.driverteam.govn.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Appointment() : Parcelable {
    @SerializedName("_id")
    @Expose
    var _id: String? = null
    @SerializedName("time")
    @Expose
    var time: Any? = null
    @SerializedName("reminderTime")
    @Expose
    var reminderTime: Any? = null
    @SerializedName("location")
    @Expose
    var location: Geometry? = null
    @SerializedName("locationName")
    @Expose
    var locationName: String? = null
    @SerializedName("locationAddress")
    @Expose
    var locationAddress: String? = null
    @SerializedName("reminderMsg")
    @Expose
    var reminderMsg: String? = null
    @SerializedName("accepted")
    @Expose
    var accepted: List<User>? = null
    @SerializedName("group")
    @Expose
    var group: Any? = null
    @SerializedName("groupID")
    @Expose
    var groupID: String? = null
    @SerializedName("properties")
    @Expose
    var properties: AppointmentProperties? = null
    @SerializedName("status")
    @Expose
    var status: String? = null

    constructor(parcel: Parcel) : this() {
        _id = parcel.readString()
    }

    constructor(time: String, reminderTime: String, location: Geometry, reminderMsg: String,
                locationName: String, groupID: String, properties: AppointmentProperties, status: String, locationAddress: String) : this() {
        this.time = time
        this.reminderTime = reminderTime
        this.location = location
        this.locationName = locationName
        this.reminderMsg = reminderMsg
        this.groupID = groupID
        this.properties = properties
        this.status = status
        this.locationAddress = locationAddress
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Appointment> {
        override fun createFromParcel(parcel: Parcel): Appointment {
            return Appointment(parcel)
        }

        override fun newArray(size: Int): Array<Appointment?> {
            return arrayOfNulls(size)
        }
    }
}