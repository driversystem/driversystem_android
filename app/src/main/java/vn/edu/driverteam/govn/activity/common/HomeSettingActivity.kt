package vn.edu.driverteam.govn.activity.common

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.controllers.AppController
import com.google.android.gms.common.api.Status
import com.sdsmdg.tastytoast.TastyToast

import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment

class HomeSettingActivity : AppCompatActivity() {

    @BindView(R.id.imBack_home_setting)
    lateinit var btnBack: ImageView
    @BindView(R.id.tvAddress_home_setting)
    lateinit var tvAddress: TextView
    @BindView(R.id.btnDismiss_home_setting)
    lateinit var btnDismiss: Button
    @BindView(R.id.btnChoose_home_setting)
    lateinit var btnChoose: Button


    private var isNewChosen = false

    private var chosenPlace: Place? = null

    private lateinit var placesClient:PlacesClient
    private var placeFields = listOf(Place.Field.ID,
            Place.Field.NAME,
            Place.Field.ADDRESS,
            Place.Field.LAT_LNG)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_setting)

        ButterKnife.bind(this)
        initComponents()
    }

    private fun initComponents() {

        if (intent.getStringExtra("home_location") != "") {
            tvAddress.text = intent.getStringExtra("home_location")
        }

        // Set up auto complete search bar
        initPlaces()
        setUpPlacesAutoComplete()

        btnBack.setOnClickListener {
            finish()
        }

        btnDismiss.setOnClickListener {
            finish()
        }

        btnChoose.setOnClickListener {
            if (isNewChosen) {
                AppController.userProfile!!.latHomeLocation = chosenPlace!!.latLng!!.latitude
                AppController.userProfile!!.longHomeLocation = chosenPlace!!.latLng!!.longitude

                intent.putExtra("home_location_new", tvAddress.text.toString())
                setResult(Activity.RESULT_OK, intent)
                finish()
            } else {
                TastyToast.makeText(this, "Vui lòng chọn địa chỉ mới", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }
        }
    }

    private fun initPlaces() {
        Places.initialize(this, getString(R.string.places_api))
        placesClient = Places.createClient(this)
    }

    private fun setUpPlacesAutoComplete() {
        val autoCompleteFragment = supportFragmentManager.findFragmentById(R.id.place_autocomplete_home_setting) as AutocompleteSupportFragment
        autoCompleteFragment.setPlaceFields(placeFields)

        autoCompleteFragment.setOnPlaceSelectedListener(object:com.google.android.libraries.places.widget.listener.PlaceSelectionListener{
            override fun onPlaceSelected(p0: Place) {
                Toast.makeText(this@HomeSettingActivity, ""+p0.address, Toast.LENGTH_SHORT).show()

                tvAddress.text = p0.address
                chosenPlace = p0
                isNewChosen = true
            }

            override fun onError(p0: Status) {
                Toast.makeText(this@HomeSettingActivity, ""+p0.statusMessage, Toast.LENGTH_SHORT).show()
            }
        })
    }
}
