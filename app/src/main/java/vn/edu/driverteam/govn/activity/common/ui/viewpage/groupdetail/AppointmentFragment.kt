package vn.edu.driverteam.govn.activity.common.ui.viewpage.groupdetail

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.activity.common.AppointmentDetailActivity
import vn.edu.driverteam.govn.activity.common.CreateAppointmentActivity
import vn.edu.driverteam.govn.models.Appointment
import vn.edu.driverteam.govn.models.appointment.ListAppointmentAdapter
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.AppointmentService
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.models.AppointmentResponse
import com.sdsmdg.tastytoast.TastyToast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val GROUP_ID = "groupId"
private const val GROUP_NAME = "groupName"
private const val LIST_APPOINTMENT = "listAppointment"

/**
 * A simple [Fragment] subclass.
 * Use the [AppointmentFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AppointmentFragment : Fragment() {
    private var groupId: String? = null
    private var groupName: String? = null
    private var listAppointment: List<Appointment>? = null
    private lateinit var recyclerViewListAppointment: RecyclerView
    private lateinit var btnCreateAppointment: ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            groupId = it.getString(GROUP_ID)
            groupName = it.getString(GROUP_NAME)
            listAppointment = it.getParcelableArrayList(LIST_APPOINTMENT)
        }
    }

    override fun onResume() {
        super.onResume()
        arguments?.let {
            groupId = it.getString(GROUP_ID)
            groupName = it.getString(GROUP_NAME)
            listAppointment = it.getParcelableArrayList(LIST_APPOINTMENT)
        }

        loadAppointment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_appointment, container, false)
        initComponent(view)
        return view
    }

    private fun initComponent (view: View) {
        recyclerViewListAppointment = view.findViewById(R.id.list_group_appointment)
        btnCreateAppointment = view.findViewById(R.id.btn_add_appointment)

        btnCreateAppointment.setOnClickListener {
            val intent = Intent(activity, CreateAppointmentActivity::class.java)
            intent.putExtra("group_id", groupId)
            intent.putExtra("group_name", groupName)
            startActivity(intent)
        }
    }

    private fun initSwipeToDeleteAppointment(listAppointment: ArrayList<Appointment>) {
        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                    when(which){
                        DialogInterface.BUTTON_POSITIVE -> {
                            onDeleteAppointment(listAppointment[viewHolder.adapterPosition]._id!!)
                        }
                        DialogInterface.BUTTON_NEGATIVE -> {
                            loadAppointment()
                        }
                    }
                }
                val builder = AlertDialog.Builder(context!!)
                builder.setMessage("Bạn có chắc muốn xóa buổi hẹn này không?")
                        .setPositiveButton("Có", dialogClickListener)
                        .setNegativeButton("Không", dialogClickListener).show()

            }

            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                val view = viewHolder.itemView
                val background = ColorDrawable()
                val deleteIcon = ContextCompat.getDrawable(context!!, R.drawable.ic_delete_white_24dp)
                background.color = Color.RED
                deleteIcon!!.setBounds(view.right-deleteIcon.minimumWidth - 50, view.top + 120, view.right - 50, view.top + 120 + deleteIcon.minimumHeight)
                background.setBounds(view.right+dX.toInt(), view.top, view.right, view.bottom)
                background.draw(c)
                deleteIcon.draw(c)
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
        }

        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(recyclerViewListAppointment)
    }

    private fun onDeleteAppointment(appointmentId: String) {
        val service = APIServiceGenerator.createService(AppointmentService::class.java)
        val call = service.deleteAppointment(appointmentId)

        call.enqueue(object : Callback<AppointmentResponse> {
            override fun onResponse(call: Call<AppointmentResponse>, response: Response<AppointmentResponse>) {
                if (response.isSuccessful) {
                    listAppointment = response.body()!!.appointments
                    loadAppointment()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(activity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                    loadAppointment()
                }
            }

            override fun onFailure(call: Call<AppointmentResponse>, t: Throwable) {
                TastyToast.makeText(activity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
                loadAppointment()
            }

        })
    }

    private fun loadAppointment() {
        recyclerViewListAppointment.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = ListAppointmentAdapter(listAppointment as MutableList<Appointment>) { appointment: Appointment ->
                run {
                    val intent = Intent(activity, AppointmentDetailActivity::class.java)
                    intent.putExtra("appointment_id", appointment._id)
                    startActivity(intent)
                }
            }
        }
        initSwipeToDeleteAppointment(listAppointment as ArrayList<Appointment>)
    }

    companion object {
        @JvmStatic
        fun newInstance(groupId: String?, groupName: String?, listAppointment: ArrayList<Appointment>) =
                AppointmentFragment().apply {
                    arguments = Bundle().apply {
                        putString(GROUP_ID, groupId)
                        putString(GROUP_NAME, groupName)
                        putParcelableArrayList(LIST_APPOINTMENT, listAppointment)
                    }
                }
    }
}
