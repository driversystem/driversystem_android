package vn.edu.driverteam.govn.services

import android.app.ActivityManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import vn.edu.driverteam.govn.activity.MainActivity

class GroupTravelService : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val appointmentId = intent?.getStringExtra("appointmentId")
        val appointmentLng = intent?.getDoubleExtra("appointment_location_lng", -1.0)
        val appointmentLat = intent?.getDoubleExtra("appointment_location_lat", -1.0)
        val name = intent?.getStringExtra("appointment_location_name")
        val address = intent?.getStringExtra("appointment_location_address")
        val mainIntent = Intent(context, MainActivity::class.java)
        mainIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        mainIntent.putExtra("appointmentId", appointmentId)
        mainIntent.putExtra("appointment_location_lng", appointmentLng)
        mainIntent.putExtra("appointment_location_lat", appointmentLat)
        mainIntent.putExtra("appointment_location_name", name)
        mainIntent.putExtra("appointment_location_address", address)
        context?.startActivity(mainIntent)
    }

    private fun isAppRunning(context: Context?, packageName: String) : Boolean {
        val activityManager = context?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val processInfos = activityManager.runningAppProcesses

        processInfos?.forEach { processInfo->
            if(processInfo.processName == packageName) {
                return true
            }
        }
        return false
    }
}