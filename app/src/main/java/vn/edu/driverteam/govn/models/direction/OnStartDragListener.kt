package vn.edu.driverteam.govn.models.direction

interface OnStartDragListener {
    fun onStartDrag(viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder)
}