package vn.edu.driverteam.govn.services.models

import vn.edu.driverteam.govn.models.User

class AuthenticationResponse {
    var success: Boolean? = null
    var token: String? = null
    var user: User? = null
    var newUser: Boolean? = null
}