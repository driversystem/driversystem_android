package vn.edu.driverteam.govn.models.user

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.models.User
import vn.edu.driverteam.govn.models.group.ListGroupAdapter
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

class ListMemberAdapter(private val myListMember: MutableList<User>, private val roles: List<String>, val clickListener: (User) -> Unit) : RecyclerView.Adapter<ListGroupAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListGroupAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycler_view_list_member_item, parent, false)

        return ListGroupAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return myListMember.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ListGroupAdapter.ViewHolder, position: Int) {
        val memberName = holder.view.findViewById<TextView>(R.id.tv_memberName)
        val role = holder.view.findViewById<TextView>(R.id.tv_role)
        val avatar = holder.view.findViewById<CircleImageView>(R.id.circleImageView_user_avatar_member_item)
        val phoneNumber = holder.view.findViewById<TextView>(R.id.tv_memberNumber)

        if((myListMember[position].avatar != "") and (myListMember[position].avatar != null)) {
            Picasso.get().load(myListMember[position].avatar).into(avatar)
        }
        memberName.text = myListMember[position].name
        if(myListMember[position].phoneNumber != null) {
            phoneNumber.text = "SĐT: " + myListMember[position].phoneNumber
        }

        role.text = roles[position]
        holder.view.setOnClickListener {clickListener(myListMember[position])}
    }
}