package vn.edu.driverteam.govn.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class NotificationPublisher: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val time = intent?.getStringExtra("time")
        val groupName = intent?.getStringExtra("groupName")
        val notificationHelper = NotificationHelper(context!!)
        val nb = notificationHelper.getChannelNotification(time.toString(), groupName.toString())
        notificationHelper.getManager().notify(1, nb.build())
    }
}