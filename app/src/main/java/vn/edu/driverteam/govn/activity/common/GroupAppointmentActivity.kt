package vn.edu.driverteam.govn.activity.common

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import butterknife.BindView
import butterknife.ButterKnife
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.activity.common.ui.viewpage.groupappointment.ListAppointmentFragment
import vn.edu.driverteam.govn.activity.common.ui.viewpage.groupappointment.ListGroupFragment
import vn.edu.driverteam.govn.activity.common.ui.viewpage.ViewPagerAdapter
import com.google.android.material.tabs.TabLayout


class GroupAppointmentActivity : AppCompatActivity() {
    @BindView(R.id.tabLayout)
    lateinit var tabLayout: TabLayout
    @BindView(R.id.viewPager)
    lateinit var viewPager: ViewPager
    @BindView(R.id.iv_invite_notification)
    lateinit var inviteNotification: ImageView
    @BindView(R.id.btnBack_group_appointment)
    lateinit var btnBack: LinearLayout
    @BindView(R.id.tv_title_group_appointment)
    lateinit var tvTitleGroupAppointment: TextView

    private lateinit var groupFragment: ListGroupFragment
    private lateinit var appointmentFragment: ListAppointmentFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_appointment)
        ButterKnife.bind(this)

        inviteNotification.setOnClickListener {
            val intent = Intent(this, InviteNotificationActivity::class.java)
            startActivity(intent)
        }
        groupFragment = ListGroupFragment()
        appointmentFragment = ListAppointmentFragment()

        tabLayout.setupWithViewPager(viewPager)

        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager, 0)
        viewPagerAdapter.addFragment(groupFragment, "NHÓM")
        viewPagerAdapter.addFragment(appointmentFragment, "LỊCH HẸN")
        viewPager.adapter = viewPagerAdapter

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            @SuppressLint("SetTextI18n")
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab != null) {
                    when(tab.position) {
                        0 -> {
                            tvTitleGroupAppointment.text = "DANH SÁCH NHÓM"
                        }
                        1 -> {
                            tvTitleGroupAppointment.text = "DANH SÁCH LỊCH HẸN"
                        }
                    }
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }
        })

        btnBack.setOnClickListener {
            finish()
        }
    }
}
