package vn.edu.driverteam.govn.activity.common.ui.viewpage.groupdetail

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.GridView
import android.widget.ImageView

import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.activity.common.GroupDetailActivity
import vn.edu.driverteam.govn.models.group.ImageAdapter
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import java.util.ArrayList

private const val PHOTOS = "photos"

class PhotosFragment : Fragment() {
    var photos: ArrayList<String>? = null
    lateinit var gridViewPhotos: GridView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            photos = it.getStringArrayList(PHOTOS)
        }
        photos!!.add(0, "")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.fragment_photos, container, false)
        initComponents(view)
        return view
    }

    private fun initComponents (view: View) {
        gridViewPhotos = view.findViewById(R.id.grid_view_groupPhotos)

        gridViewPhotos.adapter = context?.let { ImageAdapter(photos as MutableList<String>, it) }
        gridViewPhotos.setOnItemClickListener { _, _, position, _ ->
            if(position != 0) {
                val imageDialog = Dialog(context!!)
                imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                imageDialog.setContentView(R.layout.layout_image_dialog)

                val image = imageDialog.findViewById<ImageView>(R.id.fullViewImage)
                Picasso.get().load(photos!![position]).into(image)
                image.setOnClickListener {
                    imageDialog.hide()
                }
                image.scaleType = ImageView.ScaleType.FIT_CENTER

                imageDialog.show()
            } else {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .setActivityTitle("Đăng ảnh")
                        .start(context as Activity)
                (activity as GroupDetailActivity).featureFlag = 2
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(photos: List<String>) =
                PhotosFragment().apply {
                    arguments = Bundle().apply {
                        putStringArrayList(PHOTOS, photos as ArrayList<String>)
                    }
                }
    }
}
