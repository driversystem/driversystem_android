package vn.edu.driverteam.govn.activity.common

import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.ViewPager
import butterknife.BindView
import butterknife.ButterKnife
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.activity.common.ui.viewpage.ViewPagerAdapter
import vn.edu.driverteam.govn.activity.common.ui.viewpage.groupdetail.AppointmentFragment
import vn.edu.driverteam.govn.activity.common.ui.viewpage.groupdetail.MembersFragment
import vn.edu.driverteam.govn.activity.common.ui.viewpage.groupdetail.PhotosFragment
import vn.edu.driverteam.govn.controllers.AppController
import vn.edu.driverteam.govn.models.Appointment
import vn.edu.driverteam.govn.models.Group
import vn.edu.driverteam.govn.models.User
import vn.edu.driverteam.govn.models.group.ImageAdapter
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.GroupService
import vn.edu.driverteam.govn.services.models.GroupResponse
import vn.edu.driverteam.govn.services.models.SuccessResponse
import com.google.android.material.tabs.TabLayout
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.sdsmdg.tastytoast.TastyToast
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileInputStream
import java.util.*
import kotlin.collections.ArrayList

class GroupDetailActivity : AppCompatActivity() {
    @BindView(R.id.viewPager_groupDetail)
    lateinit var viewPager: ViewPager
    @BindView(R.id.tabLayout_groupDetail)
    lateinit var tabLayout: TabLayout
    @BindView(R.id.imBack_groupDetail)
    lateinit var btnBack: LinearLayout
    @BindView(R.id.description_groupDetail)
    lateinit var tvDescription: TextView
    @BindView(R.id.tv_groupName)
    lateinit var tvGroupName: TextView
    @BindView(R.id.swipeRefresh_groupDetail)
    lateinit var swipeContainer: SwipeRefreshLayout
    @BindView(R.id.btn_leaveGroup)
    lateinit var btnLeaveGroup: Button
    @BindView(R.id.avatar_groupDetail)
    lateinit var avatar: CircleImageView
    @BindView(R.id.btn_changeAvatarGroup)
    lateinit var btnChangeAvatarGroup: Button

    private lateinit var listMemberFragment: MembersFragment
    private lateinit var photoCollection: PhotosFragment
    private lateinit var appointmentFragment: AppointmentFragment
    private lateinit var viewPagerAdapter: ViewPagerAdapter

    private lateinit var groupId: String
    private lateinit var mGroup: Group
    private lateinit var loadingAlert: AlertDialog
    private lateinit var storageReference: StorageReference

    var featureFlag: Int = 0

    companion object {
        private const val GALLERY_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_detail)
        ButterKnife.bind(this)

        groupId = intent.getSerializableExtra("group_id") as String
        tabLayout.setupWithViewPager(viewPager)
        initComponent()
    }

    override fun onResume() {
        super.onResume()
        loadGroupInfo()
    }

    @SuppressLint("SetTextI18n")
    private fun initComponent() {

        swipeContainer.setOnRefreshListener {
            val intent = Intent(this, GroupDetailActivity::class.java)
            intent.putExtra("group_id", groupId)
            finish()
            overridePendingTransition(0, 0)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btnBack.setOnClickListener {
            finish()
        }

        btnLeaveGroup.setOnClickListener {
            val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                when(which){
                    DialogInterface.BUTTON_POSITIVE -> {
                        onLeaveGroup()
                    }
                    DialogInterface.BUTTON_NEGATIVE -> {}
                }
            }
            val builder = AlertDialog.Builder(this)
            builder.setMessage("Bạn có chắc muốn rời nhóm này không? Nếu bạn là chủ nhóm, nhóm sẽ bị xóa")
                    .setPositiveButton("Có", dialogClickListener)
                    .setNegativeButton("Không", dialogClickListener).show()
        }

        btnChangeAvatarGroup.setOnClickListener {
            featureFlag = 1
            openGallery()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            GALLERY_REQUEST_CODE -> {
                if(resultCode == Activity.RESULT_OK) {
                    data?.data?.let {
                        launchImageCrop(it)
                    }
                }
            }

            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                if(resultCode == Activity.RESULT_OK) {
                    when(featureFlag) {
                        1 -> {
                            updateGroupAvatar(data)
                        }
                        2 -> {
                            addPhoto(data)
                        }
                    }
                } else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    TastyToast.makeText(this, "Lỗi: Đã có sự cố khi cắt ảnh", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }
        }
    }

    private fun addPhoto(data: Intent?) {
        val result = CropImage.getActivityResult(data)
        storageReference = FirebaseStorage.getInstance().reference
                .child("images")
                .child("Group")
                .child("GroupPhotos")
                .child(groupId)
                .child(File(result.uri.path!!).name + Date().toString())
        val stream = FileInputStream(File(result.uri.path!!))
        val uploadTask = storageReference.putStream(stream)
        uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                TastyToast.makeText(this, "Failed", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
            }
            storageReference.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                onAddPhoto(task.result.toString().substring(0, task.result.toString().indexOf("&token")))
            }
        }
    }

    private fun onAddPhoto(photo: String) {
        val groupPhoto = Group("", emptyList(), emptyList(), "", "", listOf(photo))

        val service = APIServiceGenerator.createService(GroupService::class.java)
        val call = service.addPhoto(groupId, groupPhoto)

        call.enqueue(object : Callback<SuccessResponse> {
            override fun onResponse(call: Call<SuccessResponse>, response: Response<SuccessResponse>) {
                if(response.isSuccessful) {
                    photoCollection.photos!!.add(photo)
                    photoCollection.gridViewPhotos.adapter = ImageAdapter(photoCollection.photos!!, this@GroupDetailActivity)
                    TastyToast.makeText(this@GroupDetailActivity, "Đăng ảnh thành công", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@GroupDetailActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<SuccessResponse>, t: Throwable) {
                TastyToast.makeText(this@GroupDetailActivity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }
        })
    }

    private fun updateGroupAvatar(data: Intent?) {
        if(isGroupModerator(AppController.userProfile!!) or (mGroup.groupOwner!!._id == AppController.userProfile!!._id)) {
            storageReference = FirebaseStorage.getInstance().reference.child("images").child("Group").child("GroupAvatar").child(groupId)
            val result = CropImage.getActivityResult(data)
            val stream = FileInputStream(File(result.uri.path!!))
            val uploadTask = storageReference.putStream(stream)
            uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    TastyToast.makeText(this, "Failed", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
                storageReference.downloadUrl
            }.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    onUpdateAvatar(task.result.toString().substring(0, task.result.toString().indexOf("&token")))
                }
            }
        } else {
            TastyToast.makeText(this, "Người dùng không đủ quyền để thực hiện chức năng này", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
        }
    }

    private fun onUpdateAvatar(imageUri: String) {
        val avatarGroup = Group("", emptyList(), emptyList(), "", imageUri, emptyList())
        val service = APIServiceGenerator.createService(GroupService::class.java)
        val call = service.updateAvatar(groupId, avatarGroup)

        call.enqueue(object : Callback<SuccessResponse> {
            override fun onResponse(call: Call<SuccessResponse>, response: Response<SuccessResponse>) {
                if(response.isSuccessful) {
                    Picasso.get().load(imageUri).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(avatar)
                    TastyToast.makeText(this@GroupDetailActivity, "Cập nhật ảnh nhóm thành công", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@GroupDetailActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<SuccessResponse>, t: Throwable) {
                TastyToast.makeText(this@GroupDetailActivity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }
        })
    }

    private fun isGroupModerator(input: User) : Boolean {
        mGroup.groupModerators!!.forEach { user ->
            if (user._id == input._id) {
                return true
            }
        }
        return false
    }

    private fun loadGroupInfo(){
        showLoadingDialog()
        val service = APIServiceGenerator.createService(GroupService::class.java)
        val call = service.getGroupById(groupId)
        call.enqueue(object : Callback<GroupResponse> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<GroupResponse>, response: Response<GroupResponse>) {
                if(response.isSuccessful){
                    mGroup = response.body()!!.group!!
                    initTabViewPager()
                    tvGroupName.text = mGroup.groupName
                    tvDescription.text = mGroup.description
                    if((mGroup.avatar != "") and (mGroup.avatar != null)) {
                        Picasso.get().load(mGroup.avatar).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(avatar)
                    }
                    if(AppController.userProfile!!._id == mGroup.groupOwner!!._id) {
                        btnLeaveGroup.text = "Xóa nhóm"
                    }
                    hideLoadingDialog()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@GroupDetailActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                    hideLoadingDialog()
                }
            }

            override fun onFailure(call: Call<GroupResponse>, t: Throwable) {
                hideLoadingDialog()
            }

        })
    }

    private fun initTabViewPager(){
        if(!(this::listMemberFragment.isInitialized && this::photoCollection.isInitialized && this::appointmentFragment.isInitialized)){
            listMemberFragment = MembersFragment.newInstance(
                    groupId,
                    mGroup.groupModerators as ArrayList<User>,
                    mGroup.members as ArrayList<User>,
                    mGroup.groupOwner!!)
            photoCollection = PhotosFragment.newInstance(mGroup.photos!!)

            appointmentFragment = AppointmentFragment.newInstance(groupId, mGroup.groupName,mGroup.appointments as ArrayList<Appointment>)
        } else {
            appointmentFragment.arguments?.putStringArrayList("photos", mGroup.photos as ArrayList<String>)
            appointmentFragment.arguments?.putParcelableArrayList("listAppointment", mGroup.appointments as ArrayList<Appointment>)
        }

        viewPagerAdapter = ViewPagerAdapter(supportFragmentManager, 0)
        viewPagerAdapter.addFragment(listMemberFragment, "Thành viên")
        viewPagerAdapter.addFragment(photoCollection, "Ảnh")
        viewPagerAdapter.addFragment(appointmentFragment, "Lịch hẹn")
        viewPager.adapter = viewPagerAdapter
    }

    private fun showLoadingDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setCancelable(false)
        builder.setView(R.layout.layout_loading_dialog)
        loadingAlert = builder.create()
        loadingAlert.show()
    }

    private fun hideLoadingDialog() {
        loadingAlert.dismiss()
    }

    private fun onLeaveGroup() {
        val service = APIServiceGenerator.createService(GroupService::class.java)
        val call = service.leaveGroup(groupId)

        call.enqueue(object : Callback<SuccessResponse>{
            override fun onResponse(call: Call<SuccessResponse>, response: Response<SuccessResponse>) {
                if(response.isSuccessful) {
                    finish()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@GroupDetailActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<SuccessResponse>, t: Throwable) {
                TastyToast.makeText(this@GroupDetailActivity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }
        })
    }

    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        val mimeTypes = arrayOf("image/jpeg", "image/png", "image/jpg")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }

    private fun launchImageCrop(uri: Uri) {
        CropImage.activity(uri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1080, 1080)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .start(this)
    }
}