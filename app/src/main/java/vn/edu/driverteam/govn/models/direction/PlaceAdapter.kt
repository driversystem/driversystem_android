package vn.edu.driverteam.govn.models.direction

import android.annotation.SuppressLint
import androidx.core.view.MotionEventCompat
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import vn.edu.driverteam.govn.R
import java.util.*

class PlaceAdapter(private val myPlaceSet: ArrayList<SimplePlace>,private var mDragStartListener:OnStartDragListener):
        androidx.recyclerview.widget.RecyclerView.Adapter<PlaceAdapter.ViewHolder>(), ItemTouchHelperAdapter {

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        Collections.swap(myPlaceSet, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
        return true
    }

    override fun onItemDismiss(position: Int) {
        if (position<myPlaceSet.size){
            myPlaceSet.removeAt(position)
        }
        notifyDataSetChanged()
    }

    class ViewHolder(val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceAdapter.ViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.place_adapter_layout, parent, false)

        // set the view's size, margins, paddings and layout parameters
//        ...
        return PlaceAdapter.ViewHolder(view)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val tvPlace=holder.view.findViewById<TextView>(R.id.tvPlace_place_adapter_layout)
        val btnRemove=holder.view.findViewById<ImageView>(R.id.imRemove_place_adapter_layout)
        val btnMove=holder.view.findViewById<ImageView>(R.id.imDrag_place_adapter_layout)

        tvPlace.text = myPlaceSet[position].name
        btnRemove.setOnClickListener{
            onItemDismiss(position)
        }
        btnMove.setOnTouchListener { _, event ->
            if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                mDragStartListener.onStartDrag(holder)
            }
            false
        }
    }

    override fun getItemCount()=myPlaceSet.size

//    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), ItemTouchHelperViewHolder {
//
//        val textView: TextView = itemView.findViewById<View>(R.id.text) as TextView
//        val handleView: ImageView = itemView.findViewById<View>(R.id.imDrag_place_adapter_layout) as ImageView
//
//        override fun onItemSelected() {
//            itemView.setBackgroundColor(Color.LTGRAY)
//        }
//
//        override fun onItemClear() {
//            itemView.setBackgroundColor(0)
//        }
//    }
}