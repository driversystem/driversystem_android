package vn.edu.driverteam.govn.services.models

class SignupResponse {
    var success: Boolean? = null
    var message: String? = null
}