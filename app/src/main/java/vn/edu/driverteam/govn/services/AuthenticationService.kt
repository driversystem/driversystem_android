package vn.edu.driverteam.govn.services

import vn.edu.driverteam.govn.services.models.AuthenticationResponse
import vn.edu.driverteam.govn.services.models.RefreshTokenResponse
import vn.edu.driverteam.govn.services.models.SignupResponse
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthenticationService {
    @FormUrlEncoded
    @POST("auth/provider_auth")
    fun authWithThirdParty(@Field("authProvider") authProvider: String, @Field("providerUID") providerUID: String, @Field("name") name: String): Call<AuthenticationResponse>

    @FormUrlEncoded
    @POST("auth/email")
    fun authWithEmail(@Field("email") email: String, @Field("password") password: String): Call<AuthenticationResponse>

    @FormUrlEncoded
    @POST("auth/refresh_token")
    fun refreshToken(@Field("accessToken") accessToken: String): Call<RefreshTokenResponse>

    @FormUrlEncoded
    @POST("register/email/validate")
    fun registerWithEmail(@Field("email") email: String, @Field("password") password: String, @Field("name") name: String, @Field("birthDate") birthDate: String, @Field("phoneNumber") phoneNumber: String): Call<SignupResponse>
}