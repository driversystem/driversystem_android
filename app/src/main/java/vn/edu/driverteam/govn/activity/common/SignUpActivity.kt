package vn.edu.driverteam.govn.activity.common

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.HapticFeedbackConstants
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import vn.edu.driverteam.govn.activity.MainActivity
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.controllers.AppController
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.models.AuthenticationResponse
import vn.edu.driverteam.govn.services.AuthenticationService
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.models.SignupResponse
import com.sdsmdg.tastytoast.TastyToast
import kotlinx.android.synthetic.main.activity_signup.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SignUpActivity : AppCompatActivity() {

    // Kotlin support this
    @BindView(R.id.txtEmail_signup)
    lateinit var txtEmail: EditText
    @BindView(R.id.txtPassword_signup)
    lateinit var txtPassword: EditText
    @BindView(R.id.txtFullName_signup)
    lateinit var txtFullName: EditText
    @BindView(R.id.txtPhoneNumber_signup)
    lateinit var txtPhoneNumber: EditText
    @BindView(R.id.btnSignUp_signup)
    lateinit var btnSignUp: View
    @BindView(R.id.btnSignIn_signup)
    lateinit var btnSignIn: View
    @BindView(R.id.btnHideShowPassword)
    lateinit var btnHideShowPassword: ImageView
    private var isHiding = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        ButterKnife.bind(this)

        initComponents()
    }

    private fun initComponents() {
        btnSignUp.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            onSignUpWithEmail()
        }
        btnSignIn.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            onSignIn()
        }
        btnHideShowPassword.setOnClickListener {
            if(!isHiding){
                txtPassword_signup.transformationMethod = HideReturnsTransformationMethod.getInstance()
                isHiding = true
            } else {
                txtPassword_signup.transformationMethod = PasswordTransformationMethod.getInstance()
                isHiding = false
            }
        }
    }


    private fun isEmailValid(email: String): Boolean {
        //TODO: Replace this with your own logic
        return email.contains("@")
    }

    private fun onSignIn() {
        val intent = Intent(this, SignInActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun onSignUpWithEmail() {
        // Reset errors.
        txtEmail.error = null
        txtPassword.error = null
        txtFullName.error = null
        txtPhoneNumber.error = null

        // Store values at the time of the login attempt.
        val email = txtEmail.text.toString()
        val password = txtPassword.text.toString()
        val fullName = txtFullName.text.toString()
        val phoneNumber = txtPhoneNumber.text.toString()
        var isValidateOk = true

        // Validate email
        if (TextUtils.isEmpty(email)) {
            txtEmail.error = "Email không thể bỏ trống!"
            isValidateOk = false
        } else if (!isEmailValid(email)) {
            txtEmail.error = "Email không hợp lệ!"
            isValidateOk = false
        }

        // Validate password
        if (TextUtils.isEmpty(password)) {
            txtPassword.error = "Password không thể bỏ trống!"
            isValidateOk = false
        }

        // Validate full name
        if (TextUtils.isEmpty(fullName)) {
            txtFullName.error = "Họ và tên không thể bỏ trống!"
            isValidateOk = false
        }

        // Validate phone number
        if (TextUtils.isEmpty(phoneNumber)) {
            txtPhoneNumber.error = "Số điện thoại không thể bỏ trống!"
            isValidateOk = false
        }

        if (!isValidateOk)
            return

        val service = APIServiceGenerator.createService(AuthenticationService::class.java)

        val call = service.registerWithEmail(email, password, fullName, "1996-01-01", phoneNumber)
        call.enqueue(object : Callback<SignupResponse> {
            override fun onResponse(call: Call<SignupResponse>, response: Response<SignupResponse>) {
                if (response.isSuccessful) {
                    TastyToast.makeText(this@SignUpActivity, "Đăng ký thành công!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                    signInWithEmail(email, password)
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@SignUpActivity, "Lỗi" + apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<SignupResponse>, t: Throwable) {
                TastyToast.makeText(this@SignUpActivity, "Không có kết nối Internet", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }
        })
    }

    internal fun signInWithEmail(email: String, password: String) {
        val service = APIServiceGenerator.createService(AuthenticationService::class.java)
        val call = service.authWithEmail(email, password)
        call.enqueue(object : Callback<AuthenticationResponse> {
            override fun onResponse(call: Call<AuthenticationResponse>, response: Response<AuthenticationResponse>) {
                if (response.isSuccessful) {
                    onAuthenticationSuccess(response.body()!!)
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@SignUpActivity, "Lỗi: " + apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<AuthenticationResponse>, t: Throwable) {
                TastyToast.makeText(this@SignUpActivity, "Không có kết nối Internet", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }
        })
    }

    private fun onAuthenticationSuccess(response: AuthenticationResponse) {
        AppController.userProfile = response.user
        AppController.accessToken = response.token //Save access token

        // Tạo mới dữ liệu cài đặt
        AppController.settingFilterCar = "true"
        AppController.settingFilterReport = "true"
        AppController.settingTrafficLayer = "false"
        AppController.soundMode = 1
        AppController.voiceType = 1
        AppController.settingInvisible = "false"
        AppController.settingSocket = "true"
        AppController.settingUserRadius = 5000
        AppController.settingReportRadius = 5000

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }
}
