package vn.edu.driverteam.govn.services

import vn.edu.driverteam.govn.models.User
import vn.edu.driverteam.govn.services.models.*
import retrofit2.Call
import retrofit2.http.*

interface UserService {
    @get:GET("user/profile")
    val userProfile: Call<UserProfileResponse>

    // @FormUrlEncoded
    // Bỏ FormUrlEncoded vì gây lỗi với @Body
    @PUT("user/location/current")
    fun updateCurrentLocation(@Body user: User): Call<UserProfileResponse>

    @PUT("user/updateSocketID")
    fun updateSocketID(@Body user: User): Call<UserProfileResponse>

    @GET("user/nearby")
    fun getNearbyUsers(@Query("userID") userID: String?, @Query("lat") lat: Double?, @Query("lng") lng: Double?, @Query("radius") radius: Float): Call<NearbyUserResponse>

    @PUT("user/status")
    fun updateStatus(@Body user: User): Call<UserProfileResponse>

    @get:GET("user/groups")
    var allUserGroup: Call<GroupResponse>

    @GET("user/search")
    fun searchUser(@Query("q") query: String?, @Query("excludeGroup") groupId: String?): Call<UserProfileResponse>

    @get:GET("user/invitation")
    var allInvitation: Call<GroupResponse>

    @PUT("user/invitation/accept")
    fun acceptInvite(@Query("id") groupId: String?): Call<SuccessResponse>

    @PUT("user/invitation/deny")
    fun denyInvite(@Query("id") groupId: String?): Call<SuccessResponse>
    
    @get:GET("user/appointments")
    val allUserAppointment: Call<AppointmentResponse>

    @POST("user/profile/picture")
    fun updateAvatar(@Body avatar: User): Call<SuccessResponse>
}