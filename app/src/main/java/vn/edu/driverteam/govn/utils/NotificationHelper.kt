package vn.edu.driverteam.govn.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.ContextWrapper
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import vn.edu.driverteam.govn.R

class NotificationHelper @RequiresApi(Build.VERSION_CODES.O) constructor(base: Context) : ContextWrapper(base) {
    private val channelID = "channelID"
    private val channelName = "Channel Name"
    private var mManager: NotificationManager? = null

    init {
        createChannel()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        val channel = NotificationChannel(channelID, channelName, NotificationManager.IMPORTANCE_HIGH)
        getManager().createNotificationChannel(channel)
    }

    fun getManager() : NotificationManager {
        if(mManager == null) {
            mManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }

        return mManager as NotificationManager
    }

    fun getChannelNotification(time: String, groupName: String) : NotificationCompat.Builder {
        return NotificationCompat.Builder(applicationContext, channelID)
                .setContentTitle("Lời nhắc lịch hẹn")
                .setStyle(NotificationCompat.BigTextStyle().bigText("Bạn có lịch hẹn ở nhóm $groupName vào lúc $time "))
                .setContentText("Bạn có lịch hẹn ở nhóm $groupName vào lúc $time ")
                .setSmallIcon(R.drawable.logo)
    }
}