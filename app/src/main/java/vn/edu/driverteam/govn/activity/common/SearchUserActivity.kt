package vn.edu.driverteam.govn.activity.common

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.*
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.models.User
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.GroupService
import vn.edu.driverteam.govn.services.UserService
import vn.edu.driverteam.govn.services.models.SuccessResponse
import vn.edu.driverteam.govn.services.models.UserProfileResponse
import com.sdsmdg.tastytoast.TastyToast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchUserActivity : AppCompatActivity() {
    private lateinit var svSearchUser : SearchView
    private lateinit var lvListResultUser : ListView
    private lateinit var mListUser: List<User>
    private lateinit var groupId: String
    private lateinit var btnBack: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_user)
        groupId = intent.getSerializableExtra("group_id") as String

        initComponent()
    }

    private fun initComponent() {
        svSearchUser  = findViewById(R.id.searchView_searchUser)
        lvListResultUser = findViewById(R.id.listView_listUserQuery)
        btnBack = findViewById(R.id.tvBack_searchUser)

        btnBack.setOnClickListener{
            finish()
        }

        svSearchUser.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if(newText != ""){
                    searchUser(newText)
                }
                return false
            }
        })
    }

    private fun searchUser(string: String?) {
        val service = APIServiceGenerator.createService(UserService::class.java)
        val call = service.searchUser(string, groupId)
        call.enqueue(object : Callback<UserProfileResponse>{
            override fun onResponse(call: Call<UserProfileResponse>, response: Response<UserProfileResponse>) {
                if(response.isSuccessful){
                    mListUser = response.body()?.users!!
                    initListUserResult()
                    
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@SearchUserActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                TastyToast.makeText(this@SearchUserActivity, "Kết nối mạng yếu !!!", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
            }
        })
    }
    
    private fun initListUserResult() {
        val arrayAdapter = object: ArrayAdapter<User>(this@SearchUserActivity, android.R.layout.simple_list_item_2, android.R.id.text1, mListUser){
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val view = super.getView(position, convertView, parent)
                val text1 = view.findViewById<TextView>(android.R.id.text1)
                val text2 = view.findViewById<TextView>(android.R.id.text2)

                text1.text = mListUser[position].name
                text2.text = mListUser[position].email
                return view
            }
        }

        lvListResultUser.adapter = arrayAdapter

        lvListResultUser.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->  inviteMember(position)}
    }

    private fun inviteMember (position : Int) {
        val service = APIServiceGenerator.createService(GroupService::class.java)
        val call = service.inviteMember(groupId, mListUser[position]._id!!)
        call.enqueue(object : Callback<SuccessResponse>{
            override fun onResponse(call: Call<SuccessResponse>, response: Response<SuccessResponse>) {
                if(response.isSuccessful) {
                    TastyToast.makeText(this@SearchUserActivity, response.body()!!.message, TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@SearchUserActivity, apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<SuccessResponse>, t: Throwable) {
                TastyToast.makeText(this@SearchUserActivity, t.message, TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
            }

        })
    }
}
