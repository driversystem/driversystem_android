package vn.edu.driverteam.govn.activity.common

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.HapticFeedbackConstants
import android.view.View
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import vn.edu.driverteam.govn.activity.MainActivity
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.controllers.AppController
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.models.AuthenticationResponse
import vn.edu.driverteam.govn.services.AuthenticationService
import vn.edu.driverteam.govn.services.ErrorUtils
import com.sdsmdg.tastytoast.TastyToast
import kotlinx.android.synthetic.main.activity_signin.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignInActivity : AppCompatActivity() {

    @BindView(R.id.txtEmail_signin)
    lateinit var txtEmail: EditText
    @BindView(R.id.txtPassword_signin)
    lateinit var txtPassword: EditText
    @BindView(R.id.btnHideShowPassword_signin)
    lateinit var btnHideShowPassword: ImageView
    var isHiding = false

    @BindView(R.id.btnSignInWithEmail_signin)
    lateinit var btnSignInWithEmail: View

    @BindView(R.id.btnSignUp_signin)
    lateinit var btnSignUp: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        ButterKnife.bind(this)

        initComponents()
    }

    private fun onAuthenticationSuccess(response: AuthenticationResponse) {
        AppController.userProfile = response.user
                AppController.accessToken = response.token //Save access token

        // Tạo mới dữ liệu cài đặt
        AppController.settingFilterCar = "true"
        AppController.settingFilterReport = "true"
        AppController.settingTrafficLayer = "false"
        AppController.soundMode = 1
        AppController.voiceType = 1
        AppController.settingInvisible = "false"
        AppController.settingSocket = "true"
        AppController.settingUserRadius = 5000
        AppController.settingReportRadius = 5000

        // Notify sign in successfully
        TastyToast.makeText(this, "Đăng nhập thành công!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()

        // Start Main Activity
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    private fun initComponents() {
        // Sign in buttons
        btnSignInWithEmail.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            onSignInWithEmail()
        }
        btnHideShowPassword.setOnClickListener {
            if(!isHiding){
                txtPassword_signin.transformationMethod = HideReturnsTransformationMethod.getInstance()
                isHiding = true
            } else {
                txtPassword_signin.transformationMethod = PasswordTransformationMethod.getInstance()
                isHiding = false
            }
        }

        btnSignUp.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            onSignUp()
        }
    }

    private fun isEmailValid(email: String): Boolean {
        //TODO: Replace this with your own logic
        return email.contains("@")
    }

    private fun onSignUp() {
        val intent = Intent(this, SignUpActivity::class.java)
        startActivity(intent)
    }

    private fun onSignInWithEmail() {
        // Reset errors.
        txtEmail.error = null
        txtPassword.error = null

        // Store values at the time of the login attempt.
        val email = txtEmail.text.toString()
        val password = txtPassword.text.toString()
        var isValidateOk = true

        // Validate email
        if (TextUtils.isEmpty(email)) {
            txtEmail.error = "Email không thể bỏ trống!"
            isValidateOk = false
        } else if (!isEmailValid(email)) {
            txtEmail.error = "Email không hợp lệ!"
            isValidateOk = false
        }

        // Validate password
        if (TextUtils.isEmpty(password)) {
            txtPassword.error = "Mật khẩu không thể bỏ trống!"
            isValidateOk = false
        }

        if (!isValidateOk)
            return

        // Create and call service API
        val service = APIServiceGenerator.createService(AuthenticationService::class.java)
        val call = service.authWithEmail(email, password)
        call.enqueue(object : Callback<AuthenticationResponse> {
            override fun onResponse(call: Call<AuthenticationResponse>, response: Response<AuthenticationResponse>) {
                if (response.isSuccessful) {
                    onAuthenticationSuccess(response.body()!!)
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@SignInActivity, "Lỗi: " + apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<AuthenticationResponse>, t: Throwable) {
                TastyToast.makeText(this@SignInActivity, "Không có kết nối Internet", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }
        })
    }
}
