package vn.edu.driverteam.govn.models.group

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.models.Group

class ListInviteNotificationAdapter(private val myListInvitation: MutableList<Group>, private val context: Context, val clickListener: (Group) -> Unit): RecyclerView.Adapter<ListGroupAdapter.ViewHolder>()  {
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListGroupAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycler_view_list_invite_notification_item, parent, false)

        return ListGroupAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return myListInvitation.size
    }

    override fun onBindViewHolder(holder: ListGroupAdapter.ViewHolder, position: Int) {
        val tvGroupName = holder.view.findViewById<TextView>(R.id.tv_groupName_invite)

        tvGroupName.text = myListInvitation[position].groupName
        holder.view.setOnClickListener{ clickListener(myListInvitation[position])}
    }
}