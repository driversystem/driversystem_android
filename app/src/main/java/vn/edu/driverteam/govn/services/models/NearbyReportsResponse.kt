package vn.edu.driverteam.govn.services.models

import vn.edu.driverteam.govn.models.Report

class NearbyReportsResponse {
    var success: Boolean? = null
    var reports: List<Report>? = null
}