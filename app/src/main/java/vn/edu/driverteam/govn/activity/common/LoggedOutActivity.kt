package vn.edu.driverteam.govn.activity.common

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.activity.MainActivity
import vn.edu.driverteam.govn.controllers.AppController
import vn.edu.driverteam.govn.services.APIServiceGenerator
import vn.edu.driverteam.govn.services.AuthenticationService
import vn.edu.driverteam.govn.services.ErrorUtils
import vn.edu.driverteam.govn.services.models.AuthenticationResponse
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.sdsmdg.tastytoast.TastyToast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoggedOutActivity : AppCompatActivity() {
    @BindView(R.id.btnSignIn_loggedOut)
    lateinit var btnSignin: Button
    @BindView(R.id.btnSignUp_loggedOut)
    lateinit var btnSignup: TextView

    @BindView(R.id.btnSignInWithFacebook_signin)
    lateinit var btnSignInWithFacebook: LoginButton
    @BindView(R.id.btnSignInWithGoogle_signin)
    lateinit var btnSignInWithGoogle: View
    @BindView(R.id.fb)
    lateinit var btnCustomFacebookSignIn: Button

    companion object {
        private const val RC_SIGN_IN: Int = 1
    }

    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var mGoogleSignInOptions: GoogleSignInOptions

    private var firebaseAuth: FirebaseAuth? = null
    private var callbackManager: CallbackManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logged_out)
        ButterKnife.bind(this)

        btnCustomFacebookSignIn.setOnClickListener{
            btnSignInWithFacebook.performClick()
        }

        firebaseAuth = FirebaseAuth.getInstance()
        configureGoogleSignIn()
        callbackManager = CallbackManager.Factory.create()

        btnSignInWithFacebook.setPermissions("email")
        btnSignInWithFacebook.setOnClickListener {
            logInWithFacebook()
        }
        btnSignInWithGoogle.setOnClickListener {
            logInWithGoogle()
        }

        initComponent()
    }

    private fun initComponent() {
        btnSignin.setOnClickListener{
            val intent = Intent(this, SignInActivity::class.java)
            startActivity(intent)
        }

        btnSignup.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }
    }

    private fun logInWithFacebook() {
        btnSignInWithFacebook.registerCallback(callbackManager, object: FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                handleFacebookAccessToken(result!!.accessToken)
            }

            override fun onCancel() {
                Toast.makeText(this@LoggedOutActivity,"cancel",Toast.LENGTH_LONG).show()
            }

            override fun onError(error: FacebookException?) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun handleFacebookAccessToken(accessToken: AccessToken?) {
        val credential: AuthCredential = FacebookAuthProvider.getCredential(accessToken!!.token)
        firebaseAuth!!.signInWithCredential(credential)
                .addOnFailureListener {e->
                    Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
                }
                .addOnSuccessListener {result ->
                    val email: String? = result.user?.email
                    Log.d("user", result.user?.displayName.toString())
                    Toast.makeText(this, "Successfully logged in with user $email", Toast.LENGTH_LONG).show()
                }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        val credential= GoogleAuthProvider.getCredential(acct.idToken, null)
        firebaseAuth!!.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                signInWithThirdPartyProvider(acct, "google")
            } else {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun signInWithThirdPartyProvider(account: GoogleSignInAccount, provider: String) {
        val service = APIServiceGenerator.createService(AuthenticationService::class.java)
        val call = service.authWithThirdParty(provider, account.id.toString(), account.displayName.toString())

        call.enqueue(object : Callback<AuthenticationResponse> {
            override fun onResponse(call: Call<AuthenticationResponse>, response: Response<AuthenticationResponse>) {
                if(response.isSuccessful) {
                    onAuthenticationSuccess(response.body()!!)
                } else {
                    val apiError = ErrorUtils.parseError(response)
                    TastyToast.makeText(this@LoggedOutActivity, "Lỗi: " + apiError.message(), TastyToast.LENGTH_SHORT, TastyToast.ERROR).show()
                }
            }

            override fun onFailure(call: Call<AuthenticationResponse>, t: Throwable) {
                TastyToast.makeText(this@LoggedOutActivity, "Không có kết nối Internet", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show()
            }
        })
    }

    private fun onAuthenticationSuccess(response: AuthenticationResponse) {
        AppController.userProfile = response.user
        AppController.accessToken = response.token //Save access token

        // Tạo mới dữ liệu cài đặt
        AppController.settingFilterCar = "true"
        AppController.settingFilterReport = "true"
        AppController.settingTrafficLayer = "false"
        AppController.soundMode = 1
        AppController.voiceType = 1
        AppController.settingInvisible = "false"
        AppController.settingSocket = "true"
        AppController.settingUserRadius = 5000
        AppController.settingReportRadius = 5000

        // Notify sign in successfully
        TastyToast.makeText(this, "Đăng nhập thành công!", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show()

        // Start Main Activity
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                if (account != null) {
                    firebaseAuthWithGoogle(account)
                }
            } catch (e: ApiException) {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        } else {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun configureGoogleSignIn() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, mGoogleSignInOptions)
    }

    private fun logInWithGoogle() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }
}
