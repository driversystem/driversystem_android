package vn.edu.driverteam.govn.services.models

import vn.edu.driverteam.govn.models.User

class NearbyUserResponse {
    var success: Boolean? = null
    var users: List<User>? = null
}