package vn.edu.driverteam.govn.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AppointmentProperties(groupTravel: Boolean) {
    @SerializedName("groupTravel")
    @Expose
    var groupTravel: Boolean? = groupTravel

}