package vn.edu.driverteam.govn.activity.common

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import vn.edu.driverteam.govn.R
import vn.edu.driverteam.govn.utils.FileUtils

class CustomCameraActivity : AppCompatActivity() {
    companion object {
    }

    @BindView(R.id.imCamera_custom_camera)
    lateinit var imCamera: ImageView
    @BindView(R.id.imBack_custom_camera)
    lateinit var btnBack: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_camera)

        ButterKnife.bind(this)
        initComponents()
    }

    private fun initComponents() {
        btnBack.setOnClickListener {
            finish()
        }
        if (intent.getStringExtra("base64Image") != null) {
            val base64Image = intent.getStringExtra("base64Image")
            var newBitmap = FileUtils.decodeImageFile(base64Image!!)

            val matrix = Matrix()
            matrix.postRotate(-90f)
            newBitmap = Bitmap.createBitmap(newBitmap, 0, 0, newBitmap.width, newBitmap.height, matrix, true)

            imCamera.setImageBitmap(newBitmap)
        }
        if (intent.getParcelableExtra<Uri>("imageUri") != null){
            val imageStream = contentResolver.openInputStream(intent.getParcelableExtra<Uri>("imageUri")!!)
            val bitmap = BitmapFactory.decodeStream(imageStream)
            val matrix = Matrix()
            matrix.postRotate(90f)
            val newBitmap: Bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            imCamera.setImageBitmap(newBitmap)
        }
    }
}
