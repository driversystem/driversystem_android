package vn.edu.driverteam.govn.services.models

class APIError {

    private val success: Boolean? = null
    private val message: String? = null

    fun success(): Boolean? {
        return success
    }

    fun message(): String? {
        return message
    }
}