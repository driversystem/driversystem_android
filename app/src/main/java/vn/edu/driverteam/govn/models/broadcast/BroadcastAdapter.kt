package vn.edu.driverteam.govn.models.broadcast

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.squareup.picasso.Picasso
import vn.edu.driverteam.govn.R
import de.hdodenhof.circleimageview.CircleImageView
import vn.edu.driverteam.govn.models.TypeBroadcastChat

class BroadcastAdapter(private val broadcastSet: MutableList<TypeBroadcastChat>, val clickListener: (TypeBroadcastChat) -> Unit) :
        RecyclerView.Adapter<BroadcastAdapter.ViewHolder>() {
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
//        create a new view
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.broadcast_chat_adapter_layout_new, parent, false)
        return ViewHolder(view)
    }

//    Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tvName = holder.view.findViewById<TextView>(R.id.tvName_broadcast_chat_adapter_layout)
        val avatar = holder.view.findViewById<CircleImageView>(R.id.imTypeBroadcast_chat_adapter_layout)

        tvName.text = broadcastSet[position].name
        Picasso.get().load(broadcastSet[position].imageBroadcastChat).into(avatar)
        holder.view.setOnClickListener { clickListener(broadcastSet[position]) }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = broadcastSet.size
}